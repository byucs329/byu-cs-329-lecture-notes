# Overview

The final covers black-box testing, white-box testing, control flow graphs, dominance trees, mutation testing, formal specification, and formal verification with weakest-precondition calculus.   The target is 3 hours of student time to complete. The format is short answer/free response. One 8.5x11 page of notes with data on any side is allowed.

# Resources

The [lecture notes](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/) repository has entries for each topic on the test with suggested readings and learning activities.

# Comprehensive List of Topics

The below is a comprehensive list of topics on the exam. If a topic is not on the below list, then that is not being tested on the final. Also included is some indication to what each question asks to do in regards to the topic.

  * Write test inputs for black-box test input partitioning and boundary value analysis for a given specification.
  * Write test inputs for white-box test branch coverage for a given Java method.
  * Draw a control flow graph for a given Java method. Please do add an entry and exit node to the graph.
  * Compute the dominance tree from the control flow graph in the previous problem.
  * Write test inputs for white-box statement coverage for a given Java method.
  * Add additional test inputs to, or modify the method in, the previous problem to cover a given set of mutations. 
  * Write a formal specification for a program suitable for Dafny to use in a proof of total correctness. The specification needs to include a loop invariant and decreases clause for termination. See [dafny-intro](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/dafny/dafny-intro.md) and [8-Formal-Specification](https://bitbucket.org/byucs329/homework-support/src/master/homework-writeups/8-Formal-Specification.md).
  * Prove that a given Dafny program with if-statements satisfies its specification using the weakest-precondition calculus. See [weakest-precondition](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/weakest-preconditions.md), [wp-for-loops](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/wp-for-loops.md), and [9-Formal-Verification](https://bitbucket.org/byucs329/homework-support/src/master/homework-writeups/9-Formal-Verification.md).
  * Prove that a given Dafny program with a while-loop satisfies its specification using the weakest-precondition calculus.
 