# Objectives

* Explain how mutation analysis assesses the fitness of a test suite
* Define different mutation operators
* Use the [PIT](http://pitest.org/) to analysis test suites in Java

# Reading

* [What the heck is mutation testing?](https://www.codeaffine.com/2015/10/05/what-the-heck-is-mutation-testing/)
* [Mutation Testing](https://en.wikipedia.org/wiki/Mutation_testing)
* [Getting Mutants to Test your Tests](http://media.ogn.s3.amazonaws.com/ogn27/microslot-ChrisRimmer.pdf)
* [Mutation Testing in Software Testing: Mutant Score & Analysis Example](https://www.guru99.com/mutation-testing.html)
* [Mutants](https://www.inf.ed.ac.uk/teaching/courses/st/2011-12/Resource-folder/09_mutation.pdf)
* [PIT](http://pitest.org/) 

# Outline

  * Evaluates the quality of existing tests by introducing random mutations into the code base and then running the existing tests to see if any fail
  * If an existing test *fails* then the mutation is killed
  * Add tests to kill any surviving mutants and repeat
  * General terms

     * *Killed* and *Lived* are self explanatory.
     * *No coverage* is the same as *Lived* except there were no tests that exercised the line of code where the mutation was created.
     * A mutation may *time out* if it causes an infinite loop, such as removing the increment from a counter in a for loop.
     * A *non viable* mutation is one that could not be loaded by the JVM as the bytecode was in some way invalid. 
     * A *memory error* might occur as a result of a mutation that increases the amount of memory used by the system, or may be the result of the additional memory overhead required to repeatedly run your tests in the presence of mutations. 
     * A *run error* means something went wrong when trying to test the mutation. Certain types of non viable mutation can currently result in an run error. If you see a large number of run errors this is probably be an indication that something went wrong.

  * Many mutations exist and results vary depending on mutations used: [PIT Mutations](http://pitest.org/quickstart/mutators/)

# PIT

The Maven dependency for PIT (see [Quickstart](http://pitest.org/quickstart/)) is direct. Be sure to set the **targetClass** and **targetTests** correctly (and be sure they are on the class path). See the [FAQ](http://pitest.org/faq/) if it is not working. **NOTE**: must run `mvn test` first or the JUnit5 plugin will not find any tests (TODO: figure out why).

```xml
<plugin>
	<groupId>org.pitest</groupId>
	<artifactId>pitest-maven</artifactId>
	<version>1.4.10</version>
	<dependencies>
		<dependency>
			<groupId>org.pitest</groupId>
			<artifactId>pitest-junit5-plugin</artifactId>
			<version>0.10</version>
		</dependency>
	</dependencies>
	<configuration>
		<targetClasses>
			<param>edu.byu.cs329.pit.Coverage</param>
		</targetClasses>
		<targetTests>
			<param>edu.byu.cs329.pit.tests.CoverageTests</param>
		</targetTests>
		<mutators>
			<mutator>NEW_DEFAULTS</mutator>
			<mutator>RETURN_VALS</mutator>
			<mutator>ROR</mutator>
		</mutators>
	</configuration>
</plugin>
```

There are [several mutators](https://pitest.org/quickstart/mutators/). These can be specified as groups or individually. Set the mutator to turn them on or off. **ALL** is probably too many.

Run PIT with **mvn test org.pitest:pitest-maven:mutationCoverage** after the project tests are run (`mvn test`). The other goal in the quick-start  hooks to source control and only tests classes that have changed or been added (super nice feature for sure). For JUnit 5, the *pitest-junit5-plugin* in needed otherwise PIT will not find the tests. Reports are in *target/pit-reports*---open the *index.html* file

# Report

The report is in the target directory in `pit-reports`. The results of each run are in a time stamped named directory. Open the `index.html` file in the run of interest. Follow the links to the source. Green is good. Red/pink is bad. The numbers just right of the line numbers with the link show the number of mutants that survived. Hover-over show which mutants they are and the link gives details on all the mutants.
