package edu.byu.cs329.pit.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import edu.byu.cs329.pit.Coverage;

@DisplayName("Tests for Coverage class")
public class CoverageTests {

  @Test
  void testOneTimesZeroIsZero() {
    int result = Coverage.russianMultiplication(1, 0);
    Assertions.assertEquals(0, result);
    result = Coverage.russianMultiplication(2, 2);
    Assertions.assertEquals(4, result);
  }

  @Test
  void testTwoTimesZeroIsZero() {
    int result = Coverage.russianMultiplication(2, 0);
    Assertions.assertEquals(0,result);
  }

  @Test
  void testComplexityCoverage() {
    // MC/DC coverage
    Assertions.assertEquals(0, Coverage.russianMultiplication(2, 0));
  }

  @Test
  void testConditionCoverageConditionDecision() {
    Assertions.assertEquals(1, Coverage.conditionDecision(2, 0));
    Assertions.assertEquals(1, Coverage.conditionDecision(4, 1));

  }

  @Test
  void testDecisionCoverageConditionDecision() {
    Coverage.conditionDecision(1, 0);
    Assertions.assertEquals(0, Coverage.conditionDecision(4, 0));
    Assertions.assertEquals(0, Coverage.conditionDecision(3, 0));
    Coverage.conditionDecision(1, 1);
    Coverage.conditionDecision(4, 1);
  }

  @Test
  void testDecisionCoverageConditionDecisionMore() {
    Coverage.conditionDecisionMore(1, 0);
    Coverage.conditionDecisionMore(4, 0);
    Coverage.conditionDecisionMore(4, 1);
  }
}
