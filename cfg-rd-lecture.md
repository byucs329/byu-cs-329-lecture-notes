# Objectives

Create control flow graphs from source code. Implement the algorithm that builds the control flow graph from the AST representation using the visitor pattern.

Perform reaching definitions data-flow analysis on a control flow graph.

# Reading

  * [11-static-analysis-cfg.ppt](compilers/11-static-analysis-cfg.ppt)
  * [12-static-analysis-reaching-definition.pptx](compilers/12-static-analysis-reaching-definition.pptx)
  * [13-static-analysis-classic-problems.ppt](compilers/13-static-analysis-classic-problems.ppt)

# Intro

Factorial example: the goal. (Included here in case it's useful.)

```java
    static int factorial(int n) {
      [ int result; ]1
      [ int i; ]2
      [ assertTrue(n >= 1); ]3
      [ result = 1; ]4
      [ i = 2; ]5
      [ while (i <= n) {
        [ result = result * i; ]7
        [ i = i + 1; ]8
      } ]6
      [ return result; ]9
    }
```

Show on slide 3 of [deck 11](compilers/11-static-analysis-cfg.ppt).

# Basic Blocks

First, we have to figure out how to split up the code into basic blocks. We use
basic blocks because multiple statements can go on a line and statements may be
complex rather than atomic. It is always possible to break a program down into
basic blocks. Also, some basic blocks may be combined with others. We'll see an
example of this later.

Label the basic blocks (top to bottom, left to right; `assertTrue()` is 3,
`while` is 6, `return` is 9, and statements in `while` are 7 and 8).

# CFGs

The boolean-only example in Robby's slide [deck 11](compilers/11-static-analysis-cfg.ppt) has moderately interesting control flow. Skip the implementation details. Draw the CFG on the board as it is discovered.

## Rules

Apply the following rules to a function's statements to create the control flow graph. These rules suffice for a simple language without complex expressions. Additional language features may necessitate additional rules or reasoning.

Observe that the order in which statements are processed corresponds to the order in which they are visited by the visitor pattern. Except where noted otherwise, `visit()` should return `true` so that its descendants are also processed.

### Function body

1. Begin by identifying the first statement (b-init in slides). Make a last statement b-last. If there are no statements, b-init is simply b-last.
2. If there are statements, create an edge from the last statement to b-last.

After completing these steps, visit each statement in order (return `true` from `visit()` and this will happen automatically). This step is given as an example of the rule given above and will not be repeated.

### Sequences of statements

Add an edge from each non-`return` statement to its successor.

### Assignments, method calls, etc.

Do nothing.

### `while` or `for` statements 

For convenience, we call the loop's basic block `w`.

Observe that a `for` loop is a `while` loop with additional functionality built in. It can be transformed to a `while` loop. Also observe that there is already an edge from `w` to the next statement in its parent.

  1. If the `while` loop body contains statements, add an edge from `w` to its first statement.
  2. If the `while` loop body contains statements and the last is not a `return` statement, add an edge from the last node to `w`.
  3. If the loop has no statements, add an edge from `w` to `w`.

### `if` statements

For convenience, we call the `if` statement's basic block `i`. There is already an edge from `i` to some successor block, which we call `n`.

Apply the following to the sequences of statements in both the true (then) and false (else) blocks:

  1. If there are statements in the sequence, add an edge from `i` to the first statement.
  2. If there are statements and the last is not a `return` statement, add an edge from the last statement to `n`.
  3. If there are statements, remove the edge from `i` to `n`.

### `return` statements

Add an edge from the statement to b-last.

## Practice

Have students practice on the example programs in slide 30 of [deck 11](compilers/11-static-analysis-cfg.ppt).

# Reaching Definitions

Slide [deck 12](compilers/12-static-analysis-reaching-definition.pptx) has good motivating examples and [deck 13](compilers/13-static-analysis-classic-problems.ppt) break it down clearly.

`S` is the set of statements in the method. `V` is the set of variable names in the method.  
The set `D = {*, ?} union S` in the set of *definitions* for the analysis where `*` means the definition is from the parameter parameter, and `?` means that it does not yet have a definition. 

The analysis relies `entry` and `exit` functions that map a statement in `S` to a subset of `V x D`---(variable, definition) pairs. The functions are computed using `kill` and `gen` set for each statement.

For reaching definitions, `kill(s)` where `s in S` depends on the statement:

```
kill([x = e;]^l) = {(x, d) | d in D}
kill([...]^l) = {}
```

Any assignment kills all definitions of the assigned variable. Any other statement kills no definitions of `x`.

For reaching definitions `gen(s) where `s in S` depends on the statement:

```
gen([x = e;]^l) = {(x, l)}
gen([...]) = {}
```

Any assignment creates a definition of the variable where `x` is the variable and `l` is the assignment statement. Any other statement does not generate anything.

The `entry` and `exit` functions are computed for a forward data-flow analysis on the CFG. The `entry` set flows into the statement and the `exit` set flows out of the statement. The flow is calculated as follows for a statement `s`:

```
entry(s) = 
   if s is the start node: {(x, *) | for all x, isParameter(x)} union 
                           {(x, ?) | for all x, isLocal(x)}
   otherwise: Union_{s' in preds(s)} exit(s')

exit(s) = (entry(s) setminus kill(s)) union gen(s)
```

The entry set for the start statement has the parameter and `?` for all locals. The other entry sets are the union over the exit sets of the predecessor. The exit set kills definitions and adds in generated definitions.

The flow is repeated from start to end until none of the entry and exit sets change. Use a table to track where each row is a statement and each column is an iteration of the flow through the graph.

```java
void f(int x, int y) {
  [x = a + b;]1
  [y = a * b;]2
  [while (y > a + b) {
    [a = a + 1;]4
    [x = a + b;]5
  }]3
}
```

```java
static int factorial(int n) {
  [int result;]0
  [int i;]1
  [StaticJavaLib.assertTrue(n >= 1);]2
  [result = 1;]3
  [i = 2;]4
  [while (i <= n) {
    [result = result * i;]6
    [i = i + 1;]7
  }]5
  [return result;]8
}
```