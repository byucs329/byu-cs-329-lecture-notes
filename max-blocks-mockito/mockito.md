# Objective

Use [mockito](https://site.mockito.org/) to create *dummy* objects, *fake* objects, *stub* objects, or *mock* objects for unit testing. In particular, create a *mock* to test a simple algorithm for computing leaders for maximal basic blocks. In this case, the *mock* will be the control flow graph.

# Reading

* [Mockito](https://site.mockito.org/)
* [Unit tests with Mockito - Tutorial](https://www.vogella.com/tutorials/Mockito/article.html)
* [Mockito User Docs](http://static.javadoc.io/org.mockito/mockito-core/2.25.0/org/mockito/Mockito.html)
* [Verify Interface](https://static.javadoc.io/org.mockito/mockito-core/3.0.0/org/mockito/Mockito.html#4)

# Mocks and Spys

The term *mock* is used interchangeably to mean

   * *dummy* objects: passed around but never used
   * *fake* objects: simplified working implementations
   * *stub* classes: partial implementation with only responses for the test at hand

In the end it is some type of a dummy implementation for an interface or a class in which you define the output of certain method calls. Mock objects are configured to perform a certain behavior during a test. They typically record the interaction with the system and tests can validate that.

There are two types of testing that can happen with *mocks*: interaction testing and stateful testing. Interaction testing makes sure that the system under test (SUT) calls interfaces on the mocks in the right order and the right number of times. For example, a client calls a mocked server with specific arguments a fixed number of times. That is interface testing, and mocking is well suited to such testing as it tracks all the calls on mocked objects and provides interfaces to validate interface interaction.

Stateful testing checks the actual input to output response of the SUT to be sure it is doing correct computation. Mocks are able to do stateful testing, but some care must be taken as the mocks can become very complicated since they have to be smarter about how they respond to the SUT. If the mocks are getting too complex, then it may be time to reconsider how the object is being tested.

A spy is a little different than a mock. Spys are used to test interfaces and stateful interactions. Where a mock fakes an object, a spy runs the actual code for the object (though it is possible to mock specific API calls if desired so some aspects run the actual code while other aspects are mocked). A spy adds an additional feature to the object though in that it tracts all the interactions. That means with a spy it is possible to validate interactions in the same way interactions are validated with a mock. In most implementations, a spy is a wrapper around an object that tracks the interactions and then passes through the calls to the actual object.

# Mockito

Mockito is one of many frameworks. It is used here to illustrate in general mocking.

  * Mockito simplifies the process of creating mock objects and is super helpful for classes with external dependencies
  * Typical flow:

    1. Mock dependencies
    2. Execute code in class under test
    3. Validate if the code worked

  * Switch to use the annotations: ```@Mock```
  * Show example of ```@InjectMock``` to initialize the fields of a class with mock objects (TODO)
  * Add example of validate API and the argument matching API
  * Do it for a graph (like the CFG) if possible

Watch the dependencies. The first provides the Mockito interface while the second provides the integration with the JUnit Jupiter platform runner. Be sure to have the latest greatest versions or JUnit will give unexpected exceptions from Mockito.

```xml
<dependency>
  <groupId>org.mockito</groupId>
	<artifactId>mockito-core</artifactId>
	<version>3.12.4</version>
	<scope>test</scope>
</dependency>
<dependency>
	<groupId>org.mockito</groupId>
	<artifactId>mockito-junit-jupiter</artifactId>
	<version>3.12.4</version>
	<scope>test</scope>
</dependency>
```

## @Mock and the when-method

Teach the basic principle behind maximal basic blocks and what a *leader* is for a block. Use ```src/test/resources/MaxBlocks.java```. Walk through (quickly) the code to compute leaders in ```ComputeLeaders.java```.
See [Basic Block](https://en.wikipedia.org/wiki/Basic_block) for a simple definition:

  * The first instruction is a leader.
  * The target of a conditional or an unconditional goto/jump instruction is a leader.
  * The instruction that immediately follows a conditional or an unconditional goto/jump instruction is a leader.

This means that these instructions end a basic block:

  * unconditional and conditional branches, both direct and indirect
  * returns to a calling procedure
  * instructions which may throw an exception
  * function calls can be at the end of a basic block if they cannot return, such as functions which throw exceptions or special calls like C's longjmp and exit
  * the return instruction itself.

Create a mock for the CFG Interface. Also create mocks for all the statements in the ```src/test/resources/MaxBlocks.java``` file. Here we are going to mock all the AST node types.

```java
@DisplayName("Tests for ComputeLeader")
public class ComputeLeaderTests {

  @Test
  @DisplayName("Should return start when givin one node graph")
  void should_ReturnStart_when_GivenOneNodeGraph() {
    ControlFlowGraph cfg = mock(ControlFlowGraph.class, withSettings().name("OneNodeGraph"));
    Statement stmnt = mock(Statement.class);

    when(cfg.getStart()).thenReturn(stmnt);

    Collection<Statement> leaders = ComputeLeaders.getLeaders(cfg);

    assertEquals(1, leaders.size());
    assertTrue(leaders.contains(stmnt));
    then(cfg).should(times(2)).getStart();
    then(cfg).should().getSuccs(any());
    then(cfg).should().getPreds(any());
    then(cfg).shouldHaveNoMoreInteractions();
  }

  @Test
  @DisplayName("Should return five leaders when given MabBlock graph")
  void should_ReturnFiveLeaders_when_GivenMaxBlockGraph() {
    ControlFlowGraph cfg = mock(ControlFlowGraph.class, withSettings().name("MaxBlockGraph"));
    Statement s1 = mock(Statement.class);
    Statement s2 = mock(Statement.class);
    Statement s3 = mock(Statement.class);
    Statement s4 = mock(Statement.class);
    Statement s5 = mock(Statement.class);
    Statement s6 = mock(Statement.class);
    Statement s7 = mock(Statement.class);
    Statement end = mock(Statement.class);

    Set<Statement> succS1 = Set.of(s2);
    Set<Statement> succS2 = Set.of(s3);
    Set<Statement> succS3 = Set.of(end, s4);
    Set<Statement> succS4 = Set.of(s5);
    Set<Statement> succS5 = Set.of(s6);
    Set<Statement> succS6 = Set.of(s7,s3);
    Set<Statement> succS7 = Set.of(s3);

    Set<Statement> predsS3 = Set.of(s2, s6, s7);

    given(cfg.getStart()).willReturn(s1);
    given(cfg.getPreds(s3)).willReturn(predsS3);
    given(cfg.getSuccs(any())).will(
      (InvocationOnMock invocation) ->  {
        Statement s = invocation.getArgument(0, Statement.class);
        if (s.equals(s1)) {
          return succS1;
        } else if (s.equals(s2)) {
          return succS2;
        } else if (s.equals(s3)) {
          return succS3;
        } else if (s.equals(s4)) {
          return succS4;
        } else if (s.equals(s5)) {
          return succS5;
        } else if (s.equals(s6)) {
          return succS6;
        } else if (s.equals(s7)) {
          return succS7;
        }
        return new HashSet<Statement>();
      }
    );

    Collection<Statement> leaders = ComputeLeaders.getLeaders(cfg);

    assertEquals(5, leaders.size());
    assertTrue(leaders.contains(s1));
    assertTrue(leaders.contains(s3));
    assertTrue(leaders.contains(s4));
    assertTrue(leaders.contains(end));
    assertTrue(leaders.contains(s7));
  }
}
```

It is super important to call the `@ExtendWith(MockitoExtension.class)` is what initializes the mocks and is integrated into JUnit. It is also possible to call ```MockitoAnnotations.initMocks(this)``` before using any of the mocks. Both do the same thing. They creates the mocks. Here is a stronger test that actually checks all the leaders.

A word on *strictness*. Mockito is in general *lenient* letting users mock in any way they so desire, but increasingly, Mockito is making the default behavior *more strict* disallowing some aspects of mocking considered *bad form.*

For example, mocking behavior that is never exercised is *bad form.* So Mockito throws an exception. Also bad form is Mocking many different return types from a given method call as is the case for the graph example. It the above graph example, about the third or fourth definition for `getSuccs()` Mockito complains. The call to `lenient` turns off the strictness for the mock.

A better test approach maybe to write simpler tests with simpler structures. For example, write a test for type of leader from the specification.

```java

```

## Verifying Interactions

The [verify interface](https://static.javadoc.io/org.mockito/mockito-core/3.0.0/org/mockito/Mockito.html#4) on Mockito is very capable. There is a whole lot that it can do including a validation of parameters in calls and the order of calls.

Add code to our test to check that ```cfg.getStart``` is called before ```cfg.getSuccs(es1)```.

```java
@Test
void mockTest() {
  Set<Statement> leaders = ComputeLeaders.getLeaders(cfg);
  Assertions.assertEquals(5, leaders.size());
  Mockito.verify(cfg, Mockito.times(8)).getSuccs(Mockito.any());

  InOrder order = Mockito.inOrder(cfg);
  order.verify(cfg).getStart();
  order.verify(cfg).getSuccs(es1);
}
```

Why did the test fail? Fix the test or the code.

```java
InOrder order = Mockito.inOrder(cfg);
order.verify(cfg, Mockito.times(2)).getStart();
order.verify(cfg).getSuccs(es1);
```

Make sure ```cfg.getPreds()``` is never called as is ```cfg.getMethodDeclaration```.

```java
Mockito.verify(cfg, Mockito.never()).getPreds(Mockito.any());
Mockito.verify(cfg, Mockito.never()).getMethodDeclaration();
Mockito.verifyNoMoreInteractions(es1);
```

This demo is just a slice of what can be done with Mockito. Note that the ```@Spy``` annotation is just like ```@Mock``` only now the actual implementation of the object is used only Mockito is able to verify interactions. It is also possible now to *mock* specific API calls on the *mock* as needed.

Add example of pulling out things from the parameters. See [Using Answers for Complex Mock](https://www.vogella.com/tutorials/Mockito/article.html#mockitousage).

```java
@Test
    void mockTest() {
      Set<Statement> leaders = ComputeLeaders.getLeaders(cfg);
      Assertions.assertEquals(5, leaders.size());
      Mockito.verify(cfg, Mockito.times(8)).getSuccs(Mockito.any());

      InOrder order = Mockito.inOrder(cfg);
      order.verify(cfg, Mockito.times(2)).getStart();
      order.verify(cfg).getSuccs(es1);

      Mockito.verify(cfg, Mockito.never()).getPreds(Mockito.any());
      Mockito.verify(cfg, Mockito.never()).getMethodDeclaration();
      Mockito.verifyNoMoreInteractions(es1);
    }
```