package edu.byu.cs329.maxblocks;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.Statement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;

import edu.byu.cs329.cfg.ComputeLeaders;
import edu.byu.cs329.cfg.ControlFlowGraph;

@DisplayName("Tests for ComputeLeader")
public class ComputeLeaderTests {

  private static Set<Statement> makeSet(Statement[] stmnts) {
    return new HashSet<Statement>(Arrays.asList(stmnts));
  }

  @Test
  @DisplayName("Given one node when compute leaders then start is a leader")
  void given_oneNode_when_getLeaders_then_startIsALeader() {
    ControlFlowGraph cfg = Mockito.mock(ControlFlowGraph.class);
    Statement n0 = Mockito.mock(Statement.class);

    // Given
    BDDMockito.given(cfg.getStart()).willReturn(n0);

    // When
    Set<Statement> leaders = ComputeLeaders.getLeaders(cfg);

    // Then
    Assertions.assertAll(
      () -> Assertions.assertEquals(1, leaders.size()),
      () -> Assertions.assertTrue(leaders.contains(n0))
    );

    InOrder order = Mockito.inOrder(cfg);
    BDDMockito.then(cfg).should(order, atLeastOnce()).getStart();
     BDDMockito.then(cfg).should(order, times(1)).getSuccs(any(Statement.class));

  }

  @Test
  @DisplayName("Given merged path when compute leaders then start and merge are leaders")
  void given_mergedPath_when_getLeaders_then_startAndMergeAreLeader() {
    ControlFlowGraph cfg = Mockito.mock(ControlFlowGraph.class);
    Statement n0 = Mockito.mock(Statement.class);
    Statement n1 = Mockito.mock(Statement.class);
    Statement n2 = Mockito.mock(Statement.class);

    Statement[] n0Succs = {n1};
    Statement[] n1Preds = {n0, n2};
    Statement[] empty = {};

    // Given
    BDDMockito.given(cfg.getStart()).willReturn(n0);
    BDDMockito.given(cfg.getPreds(n1)).willReturn(makeSet(n1Preds));
    BDDMockito.given(cfg.getSuccs(any(Statement.class))).will(
      (InvocationOnMock args) -> {
        Statement n = args.getArgument(0, Statement.class);
        if (n == n0) {
          return makeSet((n0Succs));
        }
        return makeSet(empty);
      }
    );
    // When
    Set<Statement> leaders = ComputeLeaders.getLeaders(cfg);

    // Then
    Assertions.assertAll(
      () -> Assertions.assertEquals(2, leaders.size()),
      () -> Assertions.assertTrue(leaders.contains(n0)),
      () -> Assertions.assertTrue(leaders.contains(n1))
    );

    InOrder order = Mockito.inOrder(cfg);
    BDDMockito.then(cfg).should(order, atLeastOnce()).getStart();
     BDDMockito.then(cfg).should(order, times(1)).getSuccs(any(Statement.class));
  }

}
