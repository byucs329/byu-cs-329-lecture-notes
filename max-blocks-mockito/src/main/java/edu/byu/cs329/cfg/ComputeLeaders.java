package edu.byu.cs329.cfg;

import java.util.HashSet;
import java.util.Set;
import org.eclipse.jdt.core.dom.Statement;

/**
 * Compute the leaders in a control flow graph.
 */
public class ComputeLeaders {
  
  /** 
   * Compute leaders of maximal basic blocks in a control flow graph.
   * 
   * Leaders are the first statements of maximal basic blocks. 
   * A maximal basic block covers as many statements as possible so that
   * there is a single entry point, a single exit point, and no internal
   * branching.  Leaders are:
   *   - The entry point for the method
   *   - Any target of a branch (conditional or otherwise)
   *   - Anything immediately following a branch or return
   * For each leader, its basic block is the leader and all statements
   * upto, but not including, the next leader.
   * 
   * @ensures s in leaders if and only if
   *             s == start
   *          \/ size(pred(s)) > 1                              
   *          \/ exists s', s in succ(s') /\ size(succ(s')) > 1
   *          \/ exists s', s in succ(s') /\ isReturn(s')
   *
   * @param cfg control flow graph
   * @return leaders in the graph
   */
  public static Set<Statement> getLeaders(ControlFlowGraph cfg) {
    Set<Statement> leaders = new HashSet<Statement>();
    Set<Statement> visited = new HashSet<Statement>();
    leaders.add(cfg.getStart());
    search(cfg.getStart(), cfg, leaders, visited);
    return leaders;
  }

  private static void search(Statement s, ControlFlowGraph cfg, Set<Statement> leaders,
      Set<Statement> visited) {
    visited.add(s);
    Set<Statement> succs = cfg.getSuccs(s);
    if (succs.size() > 1) {
      leaders.addAll(succs);
    }
    
    Set<Statement> preds = cfg.getPreds(s);
    if (preds.size() > 1) {
      leaders.add(s);
    }

    for (Statement i : succs) {
      if (visited.contains(i)) {
        leaders.add(i);
      } else {
        search(i, cfg, leaders, visited);
      }
    }
  }
}
