# Objectives

  * Write tests for the equivalence of stateful objects
  * Write tests for the equivalence of stateful statements

# Reading

  * [Stateful Property Based Testing](https://docs.google.com/presentation/d/1gKC6CYCXQjIGwXd2-I6lcKgG6bi5q3nOmCqKoqX5LGI/edit#slide=id.ge46cba2e6_0_7)
  * [Randoop](https://randoop.github.io/randoop/)

# Outline

Stateless generates **random input** while stateful generates **random programs**. As before, there are two difficult problems: how do you generate random programs and what do you use for an oracle to test the state of the object after the program is run?

## Generating Random Programs: IntegerSet

A random program is a random sequence of calls on its interface that should drive it to some random, but interesting state. To generate a random program, attach a number to each method, and generate a sequence of numbers to exercise the methods in a random order. As before, generate random inputs to the method calls.

Write `private static Stream<Arguments> generateRandomSets()`. Define any needed constants. But it should generate an `ops` array for the sequence of method calls (each method having a unique number), and it should generate an `args` array to be the arguments for each method call. These should be bundled by a `Stream<Arguments>`. As before, the number for tests and length of tests should be knobs.

```java
  private static final int ADD_ELEMENT = 0;
  private static final int REMOVE_ELEMENT = 1;
  private static final int NUM_OPERATIONS = 2;
  private static final int NUM_TESTS = 10;
  private static final int MAX_VALUE = 50;
  private static final int MAX_OPS = 10;

  private static Stream<Arguments> generateRandomSets() {
    ArrayList<Arguments> args = new ArrayList<>();
    Random r = new Random();
    for (int i = 0; i < NUM_TESTS; ++i) {
      int ops[] = new int[r.nextInt(MAX_OPS)];
      int elems[] = new int[ops.length];
      for (int j = 0; j < ops.length; j++) {
        ops[j] = r.nextInt(NUM_OPERATIONS);
        elems[j] = r.nextInt(MAX_VALUE);
      }
      args.add(Arguments.of(ops, elems));
    }
    return args.stream();
  }
```

Also write `private static void generateRandomSet(IntegerSet set, int ops[], int elems[])` to interpret the `ops` and `elems` arrays into actual operations on the passed in set with the indicated elements as objects.

```java
  private static void generateRandomSet(IntegerSet set, int ops[], int elems[]) {
    for (int i = 0; i < ops.length; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        set.addElement(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        set.removeElement(elems[i]);
      }
    }
  }
```

## Testing the sets

From here, the easiest test to make sure there are no runtime exceptions when interpreting the `ops` and `elems` arrays. This test is simple and low hanging. 

```java
  @ParameterizedTest(name = "Test no exceptions {arguments}")
  @DisplayName("No exception in building set")
  @MethodSource("generateRandomSets")
  public void testNoExceptionsBuildingRandomSets(int ops[], int elems[]) {
    IntegerSet s = new IntegerSet();
    Assertions.assertDoesNotThrow(() -> {
      generateRandomSet(s, ops, elems);
    });
  }
```

If you have a *gold standard* implementation of an object, then use it as an oracle for randomly generated operations on the object (e.g., a random program). Write `IntegerSetTests.testEquivalence(int ops[], int elems[])` and `IntegerSetTests.testEquivalenceOnRandomSets(int ops[], int elems[])` with class. In this example, the standard Java `HashSet` is used as the oracle with a point-wise comparison to the class under test. 

```java
  private void testEquivalence(int ops[], int elems[]) {
    IntegerSet setUnderTest = new IntegerSet();
    Set<Integer> goldStandard = new HashSet<>();

    generateRandomSet(setUnderTest, ops, elems);
    generateRandomSet(goldStandard, ops, elems);

    assertEquals(setUnderTest.size(), goldStandard.size());
    for (Integer i : goldStandard) {
      assertTrue(setUnderTest.contains(i));
    }

  }

  @ParameterizedTest(name = "Test equivalence {arguments}")
  @DisplayName("Add/remove in random inputs should be equivalent")
  @MethodSource("generateRandomSets")
  public void testEquivalenceOnRandomSets(int ops[], int elems[]) {
    testEquivalence(ops, elems);
  }
```

Run and debug these tests before moving on.

If there is no *gold standard* implementation, then embed equivalence preserving operations in random programs

   * Generate a random program prefix and create two instances that arrive at the same state on the prefix
   * Add in equivalent, but different statements, in one object or both objects (e.g., do and then undo something on one object or do something to both objects in two different, but equivalent ways)
   * Generate a random post-fix and apply it to both objects
   * Check equivalence

Write several equivalent but different statements that can be done to two sets. Write a method for each of those.

```java
  private static void addElement(IntegerSet a, IntegerSet b, Integer elem) {
    a.addElement(elem);
    a.addElement(elem);

    // Equivalent Code
    b.addElement(elem);
  }

  private static void addElementRemoveAddElement(IntegerSet a, IntegerSet b, Integer elem) {
    a.addElement(elem);
    a.removeElement(elem);
    a.addElement(elem);

    // Equivalent Code
    b.addElement(elem);
  }

  private static void addElementRemoveElementIfNotThere(IntegerSet a, Integer elem) {
    if (!a.contains(elem)) {
      a.addElement(elem);
      a.removeElement(elem);
    }
  }
```

Write a new `generateRandomSet(IntegerSet setA, IntegerSet setB, int ops[], int elems[])` that works on two sets but also includes somewhere the addition of the equivalent but different operations on the sets. So both sets see the same operations in a prefix and postfix but get equivalent but different operations somewhere random in the middle.

```java
private static final int NUM_EQUIV_STATEMENTS = 3;

  private static void generateRandomSet(IntegerSet setA, IntegerSet setB, int ops[], int elems[]) {
    Random r = new Random();

    int prefix = r.nextInt(ops.length);

    for (int i = 0; i < prefix; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        setA.addElement(elems[i]);
        setB.addElement(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        setA.removeElement(elems[i]);
        setB.removeElement(elems[i]);
      }
    }

    int equiv = r.nextInt(NUM_EQUIV_STATEMENTS);
    int elem = r.nextInt(MAX_VALUE);
    if (equiv == 0) {
      logger.info("addElement({})", elem);
      addElement(setA, setB, elem);
    } else if (equiv == 1) {
      logger.info("addElementRemoveAddElement({})", elem);
      addElementRemoveAddElement(setA, setB, elem);
    } else if (equiv == 2) {
      logger.info("addElementRemoveElementIfNotThere({})", elem);
      addElementRemoveElementIfNotThere(setA, elem);
    }

    for (int i = prefix; i < ops.length; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        setA.addElement(elems[i]);
        setB.addElement(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        setA.removeElement(elems[i]);
        setB.removeElement(elems[i]);
      }
    }
  }
```

Write the test to make it all go.

```java
private void testEquivalenceSameClass(int ops[], int elems[]) {
    IntegerSet setA = new IntegerSet();
    IntegerSet setB = new IntegerSet();

    generateRandomSet(setA, setB, ops, elems);

    assertEquals(setA.size(), setB.size());
    assertEquals(setA.elements, setB.elements);
  }

  @ParameterizedTest(name = "Test equivalent statements {arguments}")
  @DisplayName("Add/remove in random inputs with equivalent statements inserted should be equivalent")
  @MethodSource("generateRandomSets")
  public void testEquivalenceOnRandomSetsWithEquivalentStatements(int ops[], int elems[]) {
    testEquivalenceSameClass(ops, elems);
  }
```

  * Discuss challenge of generating random programs and tracing the fault when the equivalent code is not known---what are solutions?
  * Talk about observing intermediate results or outputs---can add assertions for intermediate values if they matter and track both the state and output of the object

# All the code

```java
package edu.byu.cs329.pbt.stateful;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.byu.cs329.pbt.stateful.IntegerSet;

public class IntegerSetTests {
  private static Logger logger = LoggerFactory.getLogger(IntegerSetTests.class);
  private static final int ADD_ELEMENT = 0;
  private static final int REMOVE_ELEMENT = 1;
  private static final int NUM_OPERATIONS = 2;
  private static final int NUM_TESTS = 10;
  private static final int MAX_VALUE = 50;
  private static final int MAX_OPS = 10;

  private static void generateRandomSet(IntegerSet set, int ops[], int elems[]) {
    for (int i = 0; i < ops.length; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        set.addElement(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        set.removeElement(elems[i]);
      }
    }
  }

  private static void generateRandomSet(Set<Integer> set, int ops[], int elems[]) {
    for (int i = 0; i < ops.length; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        set.add(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        set.remove(elems[i]);
      }
    }
  }

  private static Stream<Arguments> generateRandomSets() {
    ArrayList<Arguments> args = new ArrayList<>();
    Random r = new Random();
    for (int i = 0; i < NUM_TESTS; ++i) {
      int ops[] = new int[r.nextInt(MAX_OPS)];
      int elems[] = new int[ops.length];
      for (int j = 0; j < ops.length; j++) {
        ops[j] = r.nextInt(NUM_OPERATIONS);
        elems[j] = r.nextInt(MAX_VALUE);
      }
      args.add(Arguments.of(ops, elems));
    }
    return args.stream();
  }

  @ParameterizedTest(name = "Test no exceptions {arguments}")
  @DisplayName("No exception in building set")
  @MethodSource("generateRandomSets")
  public void testNoAssertionsBuildingRandomSets(int ops[], int elems[]) {
    IntegerSet s = new IntegerSet();
    Assertions.assertDoesNotThrow(() -> {
      generateRandomSet(s, ops, elems);
    });
  }

  private void testEquivalence(int ops[], int elems[]) {
    IntegerSet setUnderTest = new IntegerSet();
    Set<Integer> goldStandard = new HashSet<>();

    generateRandomSet(setUnderTest, ops, elems);
    generateRandomSet(goldStandard, ops, elems);

    assertEquals(setUnderTest.size(), goldStandard.size());
    for (Integer i : goldStandard) {
      assertTrue(setUnderTest.contains(i));
    }

  }

  @ParameterizedTest(name = "Test equivalence {arguments}")
  @DisplayName("Add/remove in random inputs should be equivalent")
  @MethodSource("generateRandomSets")
  public void testEquivalenceOnRandomSets(int ops[], int elems[]) {
    testEquivalence(ops, elems);
  }

  private static final int NUM_EQUIV_STATEMENTS = 3;

  private static void generateRandomSet(IntegerSet setA, IntegerSet setB, int ops[], int elems[]) {
    Random r = new Random();

    int prefix = r.nextInt(ops.length);

    for (int i = 0; i < prefix; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        setA.addElement(elems[i]);
        setB.addElement(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        setA.removeElement(elems[i]);
        setB.removeElement(elems[i]);
      }
    }

    int equiv = r.nextInt(NUM_EQUIV_STATEMENTS);
    int elem = r.nextInt(MAX_VALUE);
    if (equiv == 0) {
      logger.info("addElement({})", elem);
      addElement(setA, setB, elem);
    } else if (equiv == 1) {
      logger.info("addElementRemoveAddElement({})", elem);
      addElementRemoveAddElement(setA, setB, elem);
    } else if (equiv == 2) {
      logger.info("addElementRemoveElementIfNotThere({})", elem);
      addElementRemoveElementIfNotThere(setA, elem);
    }

    for (int i = prefix; i < ops.length; ++i) {
      if (ops[i] == ADD_ELEMENT) {
        setA.addElement(elems[i]);
        setB.addElement(elems[i]);
      } else if (ops[i] == REMOVE_ELEMENT) {
        setA.removeElement(elems[i]);
        setB.removeElement(elems[i]);
      }
    }
  }

  private static void addElement(IntegerSet a, IntegerSet b, Integer elem) {
    a.addElement(elem);
    a.addElement(elem);

    // Equivalent Code
    b.addElement(elem);
  }

  private static void addElementRemoveAddElement(IntegerSet a, IntegerSet b, Integer elem) {
    a.addElement(elem);
    a.removeElement(elem);
    a.addElement(elem);

    // Equivalent Code
    b.addElement(elem);
  }

  private static void addElementRemoveElementIfNotThere(IntegerSet a, Integer elem) {
    if (!a.contains(elem)) {
      a.addElement(elem);
      a.removeElement(elem);
    }
  }

  private void testEquivalenceSameClass(int ops[], int elems[]) {
    IntegerSet setA = new IntegerSet();
    IntegerSet setB = new IntegerSet();

    generateRandomSet(setA, setB, ops, elems);

    assertEquals(setA.size(), setB.size());
    assertEquals(setA.elements, setB.elements);
  }

  @ParameterizedTest(name = "Test equivalent statements {arguments}")
  @DisplayName("Add/remove in random inputs with equivalent statements inserted should be equivalent")
  @MethodSource("generateRandomSets")
  public void testEquivalenceOnRandomSetsWithEquivalentStatements(int ops[], int elems[]) {
    testEquivalenceSameClass(ops, elems);
  }
}
```