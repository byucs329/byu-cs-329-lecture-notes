# Objectives

  * Explain the role of stateless Property Based Testing (SPBT)
  * Create useful oracles for stateless PBT 
  * Generate useful random input for stateless PBT including constrained input
  * Explain the difference between parameterized and dynamic tests
  * Use JUnit for stateless property based testing

# Reading

* [Dynamic Tests](https://blog.codefx.org/libraries/junit-5-dynamic-tests/)
* [Parametric Tests](https://blog.codefx.org/libraries/junit-5-parameterized-tests/)
* [Stateless Property based testing](https://docs.google.com/presentation/d/1iGbxZoL_cqr5vgYjyBFdE_kjKDfW7xfDzgRrJt_vXSw/edit)

# Stateless Property Based Testing

The underlying assumption is that manually writing tests is expensive and that more tests means more certainty about code (not necessarily true if the tests are not interesting). Stateless property based testing (stateless PBT) trades manually writing tests for writing a program to randomly generate tests, and then generate a whole bunch of random tests hoping that those tests are interesting enough to find code defects.

Stateless PBT generates random input and then checks that some property of the output holds. It presents two challenges: how to generate useful non-redundant random input and how to check properties of the output strong enough to matter.

We are punting on generating useful non-redundant random input. Generating the input is all that matters here. There are several ways to check properties of the output:

  * Assert any ensures from the specification on the output (post-conditions holds)
  * Check for runtime exceptions (it did something unexpected)
  * Assert a point-wise equivalence to some *gold-standard* implementation (compare with a perfect oracle)
  * Check for algebraic properties (e.g. inverse, etc.)

In general person hours writing tests is more expensive that compute hours running test.  

# SortedArrayMethods.merge

Not covered this year, but is a good exercise for the class.

# Assert Post-condition: SortedArrayMethods.insert

Implement the post-condition check.

```java
  static boolean isSorted(int[] arr) {
    int prev = Integer.MIN_VALUE;
    for (int i : arr) {
      if (prev > i) {
        return false;
      }
      prev = i;
    }
    return true;
  }

  static int count(int x, int[] arr) {
    int c = 0;
    for (int i : arr) {
      if (x == i) {
        ++c;
      }
    }
    return c;
  }

  static void assertCorrectInsert(int[] arr, int elem, int[] res) {
    assertEquals(res.length, arr.length + 1);
    assertTrue(isSorted(res));
    for (int i : res) {
      if (i == elem) {
        assertEquals(count(i, res), count(i, arr) + 1);
      } else {
        assertEquals(count(i, res), count(i, arr));
      }
    }
  }
```

Generate random input for ```SortedArrayMethods.insert```. This input is a little bit tricky because it is constrained input: it must be sorted. The method should take as input an integer for the maximum size array and an integer for the maximum element size in the array. Both of these become knobs to turn for generating random tests. 

**A word about Random**: always set a random seed for any random number generator so that the sequence is deterministic. In this example, it would be better to use a single class wide state random number generator rather than create one in each method. As such, `r` is a static member that is initialized with a seed.

```java
  static int[] randomSortedArray(int max, int maxsize) {
    int[] res = new int[r.nextInt(maxsize)];
    int lowerBound = 0;
    for (int i = 0; i < res.length; i++) {
      lowerBound = lowerBound + r.nextInt(max - lowerBound);
      res[i] = lowerBound;
    }
    return res;
  }
```

Write a method to generate random test inputs. The method should return a stream of ```Arguments``` (JUnit class). Note that the method relies on a few class defined constants.

```java
  private static int NUM_TESTS = 10000;
  private static int MAX_ARRAY_SIZE = 20;

  static Stream<Arguments> createRandomTestInputForInsert() {
    ArrayList<Arguments> tests = new ArrayList<>(NUM_TESTS);

    for (int i = 0; i < NUM_TESTS; ++i) {
      tests
          .add(Arguments.of(randomSortedArray(Math.abs(r.nextInt()), MAX_ARRAY_SIZE), r.nextInt()));
    }

    return tests.stream();

  }
```

Implement the test method for both parameterized and dynamic test approaches. Dynamic tests do not include the **@BeforeEach** and **@AfterEach** callbacks. Add code to see effect.

```java
  @ParameterizedTest(name = "Insert on {arguments}")
  @DisplayName("Insert on random test input (ParameterizedTest)")
  @MethodSource("createRandomTestInputForInsert")
  void insertOnRandomTestInut(int[] arr, int x) {
    int[] rv = SortedArrayMethods.insert(arr, 4);
    assertCorrectInsert(arr, 4, rv);
  }

  void testInsert(int[] arr, int x) {
    int[] rv = SortedArrayMethods.insert(arr, 4);
    assertCorrectInsert(arr, 4, rv);
  }

  static String prettyPrint(Arguments args) {
    Object argArray[] = args.get();
    int arr[] = (int[]) argArray[0];
    String s = Arrays.toString(arr) + ", " + (int) argArray[1];
    return s;
  }

  @TestFactory
  @DisplayName("Insert on random test input (DynamicTest)")
  Stream<DynamicTest> testInsert4() {
    Stream<Arguments> argsStream = createRandomTestInputForInsert();
    return argsStream.map(args -> DynamicTest.dynamicTest("Testing " + prettyPrint(args), () -> {
      Object argsArray[] = args.get();
      testInsert((int[]) argsArray[0], (int) argsArray[1]);
    }));
  }
```

# Exceptions, Equivalence, and Inverse: Zune

The Microsoft Zune bug crippled the already flagging Zune. The bug effectively bricked the Zune because of a bad leap year calculation. Microsoft did publish a patch, but Zune never recovered.

The random input is already being generated in the test code. ```generateYearFromDayInputs``` creates some number of random days while ```generateDayFromYearInputs``` generates some year that is constrained to be after the ```Zune.ORIGINYEAR```. 

Write a test that simply calls ```Zune.yearFromDay``` to see if any exceptions are thrown.

```java
  @ParameterizedTest(name = "Zune.yearFromDay({arguments})")
  @DisplayName("Test Zune.yearFromDay on random input")
  @MethodSource("generateYearFromDayInputs")
  public void testNonStandard(int days) {
    assertDoesNotThrow(() -> Zune.yearFromDay(days));
  }
```

Write test that compares the faulty implementation to an oracle for equivalence.

```java
  @ParameterizedTest(name = "Zune.yearFormDay({arguments}) == Zune.yearFromDayFaulty({arguments})")
  @DisplayName("Test Zune.dayFromYear equals Zune.yearFromDayFaulty on random input")
  @MethodSource("generateYearFromDayInputs")
  public void testEquivalence(int days) {
    assertEquals(Zune.yearFromDayNice(days), Zune.yearFromDayFaulty(days));
  }
```

Write a final property that gets a day count from a given year, adds some extra days, and then checks that it gets the same year back out.

```java
  @ParameterizedTest(name = "Zune.yearFromDayNice(Zune.dayFromYear({arguments})) == {arguments}")
	@DisplayName("Test inverse on Zune.dayFromYear and Zune.yearFromDayNice on random input")
	@MethodSource("generateDayFromYearInputs")
	public void testInverse(int year, int days) {
		assertEquals(year, Zune.yearFromDayFaulty(days));
	}
```