package edu.byu.cs329.pbt.stateful;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@DisplayName("Stateful property based tests for IntegerSet")
public class IntegerSetTests {
  private static final int ADD_ELEMENT = 0;
  private static final int REMOVE_ELEMENT = 1;
  private static final int NUM_OPERATIONS = 2;
  private static final int NUM_TESTS = 10;
  private static final int MAX_VALUE = 50;
  private static final int MAX_OPS = 10;

  private static final Random random = new Random(42);

  static Stream<Arguments> generator() {
    Supplier<Arguments> supplier = new Supplier<Arguments>() {
      @Override
      public Arguments get() {
        int size = random.nextInt(MAX_OPS);
        int[] commands = new int[size];
        int[] args = new int[size];
        fill(commands, args);
        return Arguments.of(commands, args);
      }

      void fill(int[] commands, int[] args) {
        for (int i = 0 ; i < commands.length ; ++i) {
          commands[i] = random.nextInt(NUM_OPERATIONS);
          args[i] = random.nextInt(MAX_VALUE);
        }
      }
    };
    return  Stream.generate(supplier).limit(NUM_TESTS);
  }

  static void interpretProgram(IntegerSet s, int[] commands, int[] elements, int start, int stop) {
    for (int i = start ; i < stop ; ++i) {
      int command = commands[i];
      int element = elements[i];
      if (command == ADD_ELEMENT) {
        s.addElement(element);
      } else {
        s.removeElement(element);
      }
    }
  }

  @ParameterizedTest(name = "Should not throw exception when given {0} and {1}")
  @DisplayName("Tests for the addElement and removeElement methods")
  @MethodSource("generator")
  void testsForAddElementAndRemoveElement(int[] commands, int[] elements) {
    IntegerSet a = new IntegerSet();
    IntegerSet b = new IntegerSet();

    if (commands.length == 0) {
      return;
    }

    int stop = random.nextInt(commands.length);
    assertDoesNotThrow(() -> interpretProgram(a, commands, elements, 0, stop));
    assertDoesNotThrow(() -> interpretProgram(b, commands, elements, 0, stop));

    int element = random.nextInt(MAX_VALUE);
    a.removeElement(element);
    a.addElement(element);
    a.addElement(element);

    b.addElement(element);

    assertDoesNotThrow(() -> interpretProgram(a, commands, elements, stop, commands.length));
    assertDoesNotThrow(() -> interpretProgram(b, commands, elements, stop, commands.length));

    assertEquals(a, b);
  }
}
