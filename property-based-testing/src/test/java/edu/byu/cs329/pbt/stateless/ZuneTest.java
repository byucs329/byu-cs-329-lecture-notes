package edu.byu.cs329.pbt.stateless;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import edu.byu.cs329.pbt.stateless.Zune;

public class ZuneTest {

	private static int NUM_TESTS = 100;

	public static Stream<Arguments> generateYearFromDayInputs() {
		ArrayList<Arguments> args = new ArrayList<>();
		Random r = new Random();

		for (int i = 0; i < NUM_TESTS; ++i) {
			args.add(Arguments.of(1 + r.nextInt(10000)));
		}

		return args.stream();
	}

	public static Stream<Arguments> generateDayFromYearInputs() {
		ArrayList<Arguments> args = new ArrayList<>();
		Random r = new Random();

		for (int i = 0; i < NUM_TESTS; ++i) {
			int year = Zune.ORIGINYEAR + r.nextInt(10000);
			int days = Zune.dayFromYear(year) + r.nextInt(Zune.numberDaysInYear(year));
			args.add(Arguments.of(year, days));
		}

		return args.stream();
	}
}
