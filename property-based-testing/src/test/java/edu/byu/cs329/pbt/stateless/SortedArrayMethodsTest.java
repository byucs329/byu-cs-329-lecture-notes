package edu.byu.cs329.pbt.stateless;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@DisplayName("Tests for SortedArrayMethods")
public class SortedArrayMethodsTest {

  private static final int NUM_TESTS = 10;
	private static final int MAX_ARRAY_SIZE = 20;

  static Random random = new Random(42);

  static Stream<Arguments> generator() {
    Supplier<Arguments> supplier = new Supplier<Arguments>() {
      @Override
      public Arguments get() {
        int size = random.nextInt(MAX_ARRAY_SIZE);
        int[] rv = new int[size];
        int base = random.nextInt(Integer.MAX_VALUE);
        for (int i = 0 ; i < size ; ++i) {
          rv[i] = base;
          base += random.nextInt(Integer.MAX_VALUE - base);
        }
        return Arguments.of(rv, random.nextInt(Integer.MAX_VALUE));
      }
    };

    return Stream.generate(supplier).limit(NUM_TESTS);
  }

  @DisplayName("Tests for SortedArrays.insert: no exceptions")
  @ParameterizedTest(name="Should not throw exception when given {0} and {1}")
  @MethodSource("generator")
  void should_notThrowException_when_givenRandomInput(int[] array, int value) {
    Assertions.assertDoesNotThrow(() -> SortedArrayMethods.insert(array, value));
  }

  @DisplayName("Tests for SortedArrays.insert matches Java Utils")
  @ParameterizedTest(name="Should match Java Utils when given  {0} and {1}")
  @MethodSource("generator")
  void should_matchJavaUtils_when_givenRandomInput(int[] array, int value) {
    List<Integer> goldStandard = Arrays.stream(array).boxed().collect(Collectors.toList());
    goldStandard.add(value);
    Collections.sort(goldStandard);
    List<Integer> result = Arrays.stream(SortedArrayMethods.insert(array, value)).boxed().collect(Collectors.toList());
    Assertions.assertEquals(goldStandard, result);
  }

  /*
  * @ensures res.length == arr.lenth + 1
  * @ensures forall i :: 0 <= i < rv.length ==>
                      i > 0 ==> res[i-1] <= res[i]
  *                /\ res[i] == n ==> count(n, res) == count(n, arr) + 1
  *                /\ res[i] != n ==> count(res[i], res) == count(res[i], arr)
  */
  @DisplayName("Tests for SortedArrays.insert matches specification")
  @ParameterizedTest(name="Should match specification when given  {1} and {0}")
  @MethodSource("generator")
  void should_matchSpecification_when_givenRandomInput(int[] array, int value) {
    int[] result = SortedArrayMethods.insert(array, value);
    Assertions.assertEquals(array.length + 1, result.length);
    Assertions.assertTrue(isSorted(result));
    for (int i : array) {
      if (i == value) {
        Assertions.assertEquals(getCount(array,i) + 1, getCount(result, i));
      } else {
        Assertions.assertEquals(getCount(array,i), getCount(result, i));
      }
    }
  }

  Long getCount(int[] array, int value) {
    return Arrays.stream(array).filter((int v) -> v == value).count();
  }

  boolean isSorted(int[] array) {
    if (array.length == 0) {
      return true;
    }
    for (int i = 1 ; i < array.length ; ++i) {
      if (array[i-1] > array[i]) {
        return false;
      }
    }
    return true;
  }
}
