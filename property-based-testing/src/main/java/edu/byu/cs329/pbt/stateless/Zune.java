package edu.byu.cs329.pbt.stateless;

public class Zune {

  public static int ORIGINYEAR = 1980;

  /**
   * Determines if leap year.
   * 
   * @param year year
   * @return
   */
  // Requires: nothing
  // Ensures: result is true if the input year is a leap year

  // a leap year is:
  // a year divisible by 4, but every fourth year, except the 100th year

  public static boolean isLeapYear(int year) {
    return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
  }

  public static boolean isLeapYearFaulty(int year) {
    return year % 4 == 0 && (year % 100 != 0);
  }

  /**
   * Converts days to year.
   * 
   * @requires days >= 1
   * @ensures year == yearsSince1980Given(days)
   * 
   * @param days  total days
   * @return
   */
  public static int yearFromDay(int days) {
    int year = ORIGINYEAR; /* = 1980 */
    while (days > 365) {
      if (isLeapYear(year)) {
        if (days > 366) {
          days -= 366;
          year += 1;
        }
      } else {
        days -= 365;
        year += 1;
      }
    }
    return year;
  }

  /**
   * Bad implementation of yearFromDay.
   * 
   * @param days  total days
   * @return
   */
  public static int yearFromDayFaulty(int days) {
    int year = ORIGINYEAR; /* = 1980 */
    while (days > 365) {
      if (isLeapYearFaulty(year)) {
        if (days > 366) {
          days -= 366;
          year += 1;
        } else {
          return year;
        }
      } else {
        days -= 365;
        year += 1;
      }
    }
    return year;
  }

  /**
   * Correct yearFromDay.
   * 
   * @param days  total days
   * @return
   */
  public static int yearFromDayFixed(int days) {
    int year = ORIGINYEAR; /* = 1980 */
    while (days > 365) {
      if (isLeapYear(year)) {
        if (days > 366) {
          days -= 366;
          year += 1;
        } else {
          return year;
        }
      } else {
        days -= 365;
        year += 1;
      }
    }
    return year;
  }

  /**
   * Convert days to years.
   * 
   * @param days  total days
   * @return
   */
  public static int yearFromDayNice(int days) {
    int year = ORIGINYEAR;
    while (days > numberDaysInYear(year)) {
      days -= numberDaysInYear(year);
      year += 1;
    }
    return year;
  }

  /** 
   * Determine the days in a given year.
   * 
   * @param year  a year
   * @return
   */
  public static int numberDaysInYear(int year) {
    if (isLeapYear(year)) {
      return 366;
    } else {
      return 365;
    }
  }

  /**
   * Convert year to days.
   * 
   * @cs329.requires  year >= 1980
   * @cs329.ensures result is number of days since 31 dec 1979
   * 
   * @param year  a given year
   * @return
   */
  public static int dayFromYear(int year) {
    int days = 0;
    int indexYear = ORIGINYEAR;
    while (indexYear < year) {
      if (isLeapYear(indexYear)) {
        days += 366;
      } else {
        days += 365;
      }
      indexYear += 1;
    }
    return days + 1;
  }

}
