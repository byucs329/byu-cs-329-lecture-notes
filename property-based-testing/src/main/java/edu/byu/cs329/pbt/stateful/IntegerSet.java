package edu.byu.cs329.pbt.stateful;

import java.util.ArrayList;
import java.util.List;

public class IntegerSet {
  public List<Integer> elements;

  private IntegerSet(List<Integer> elems) {
    elements = new ArrayList<>(elems);
  }

  /**
   * Creates an empty set.
   * 
   * @cs329.requires
   * @cs329.ensures set is empty
   */
  public IntegerSet() {
    elements = new ArrayList<>();
  }

  /**
   * Returns the size of this set (e.g., its rank).
   * 
   * @cs329.requires
   * @cs329.ensures the return represents only the number of unique values in the
   *                set
   * 
   * @return the number of unique elements in the set
   */
  public int size() {
    return elements.size();
  }

  /**
   * Adds an element to the set.
   * 
   * @cs329.requires x is non-null
   * @cs329.ensures {x} \cup set
   * 
   * @param x is the element to add
   */
  public void addElement(Integer x) {
    for (int i = 0; i < elements.size(); ++i) {
      if (x >= elements.get(i)) {
        elements.add(i, x);
        return;
      }
    }
    elements.add(x);
  }

  /**
   * Removes an element from the set.
   * 
   * @cs329.requires x is non-null
   * @cs329.ensures set \ {x}
   * 
   * @param x i removed from the set
   */
  public void removeElement(Integer x) {
    elements.remove(x);
  }

  /**
   * Determines if an element is in the set.
   * 
   * @cs329.requires x is non-null
   * @cs329.ensures true iff x \in set and false otherwise
   * 
   * @param x an integer
   * @return true if x \in set
   */
  public boolean contains(Integer x) {
    return elements.contains(x);
  }

  /**
   * Returns the intersection of two sets.
   * 
   * @cs329.requires s is non-null
   * @cs329.ensures \forall x, x \in the return set iff x \in set and x \in s
   * 
   * @param s a set
   * @return a new IntegerSet that is the intersection with the input set
   */
  public IntegerSet intersect(IntegerSet s) {
    List<Integer> elems = new ArrayList<>(s.elements);
    elems.removeIf((Integer i) -> !s.contains(i));
    return new IntegerSet(elems);
  }

  /**
   * * Returns the union of two sets.
   * 
   * @cs329.requires s is non-null
   * @cs329.ensures \forall x, x \in the return set iff x \in set or x \in s
   * 
   * @param s a set
   * @return a new IntegerSet that is the union with the input set
   */
  public IntegerSet union(IntegerSet s) {
    List<Integer> elems = new ArrayList<>(s.elements);
    elems.addAll(s.elements);
    return new IntegerSet(elems);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof IntegerSet)) {
      return false;
    }

    IntegerSet i = (IntegerSet)obj;
    return elements.equals(i.elements);
  }
}
