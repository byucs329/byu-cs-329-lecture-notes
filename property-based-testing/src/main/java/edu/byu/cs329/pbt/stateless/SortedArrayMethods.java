package edu.byu.cs329.pbt.stateless;

public class SortedArrayMethods {

  /**
   * Merges two sorted arrays.
   * 
   * @requires left != null /\ right != null
   * @requires forall i :: 0 < i < left.length ==> left[i-1] <= left[i]       
   * @reequires forall i :: 0 < i < right.length ==> right[i-1] <= right[i]
   * 
   * @ensures res.length == left.length + right.length
   * @ensures forall i :: 0 <= i < res.length ==> 
  *                    i > 0 ==> res[i-1] <= res[i]
                    /\ count(res[i], res) == count(res[i], left) + count(res[i], right) (is a permutation)
   * 
   * @param left  is a sorted array
   * @param right is a sorted array
   * @return res
   */
  public static int[] merge(int[] left, int[] right) {
    int[] res = new int[left.length + right.length];
    int li = 0;
    int ri = 0;
    for (int i = 0; i < res.length; i++) {
      int leftVal = li < left.length ? left[li] : Integer.MAX_VALUE;
      int rightVal = ri < right.length ? right[ri] : Integer.MAX_VALUE;
      if (leftVal <= rightVal) {
        res[i] = leftVal;
        li += 1;
      } else {
        res[i] = rightVal;
        ri += 1;
      }
    }
    return res;
  }

  /**
   * Adds a new element.
   * 
   * @requires arr != null
   * @requires forall i :: 0 < i < arr.length ==> arr[i-1] <= arr[i]
   *
   * @ensures res.length == arr.lenth + 1
   * @ensures forall i :: 0 <= i < rv.length ==> 
                       i > 0 ==> res[i-1] <= res[i]
   *                /\ res[i] == n ==> count(n, res) == count(n, arr) + 1
   *                /\ res[i] != n ==> count(res[i], res) == count(res[i], arr)
   * @param arr is a sorted array
   * @param n   is is an item
   * @return res
   */
  public static int[] insert(int[] arr, int n) {
    int[] res = new int[arr.length + 1];
    int i;
    for (i = 0; i < arr.length && arr[i] < n; i++) {
      res[i] = arr[i];
    }
    res[i] = n;
    for (; i < arr.length; i++) {
      res[i + 1] = arr[i];
    }
    return res;
  }

}
