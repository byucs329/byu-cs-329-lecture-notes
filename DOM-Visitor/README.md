# Objectives

The goal is for the student to gain a basic understanding of the Eclipse JDT DOM model. At the end of the lecture, and learning activities, a student should be able to

  * Traverse the AST with ASTView or DOMViewer
  * Analyze the AST using the visitor-pattern
  * Modify the AST to accomplish code transformations

# Reading

  * IBM Knowledge Center for [org.eclipse.jdt.core.dom](https://www.ibm.com/docs/en/wdfrhcw/1.3.0?topic=reference-orgeclipsejdtcoredom)
  * Eclipse Documentation [org.eclipse.jdt.core.dom](https://help.eclipse.org/2020-09/index.jsp?topic=%2Forg.eclipse.jdt.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fjdt%2Fcore%2Fdom%2Fpackage-summary.html&resultof%3D%2522%256f%2572%2567%252e%2565%2563%256c%2569%2570%2573%2565%252e%256a%2564%2574%252e%2563%256f%2572%2565%252e%2564%256f%256d%2522%2520)
  * AST-Visitor-DOM.ppt

# Outline

Work directly with [lab0-constant-folding](https://github.com/byu-cs329/lab0-constant-folding). First remove the implementation that is given for `ParenthesizedExpression` types, but leave the specification and all the support APIs for manipulating the AST. Demonstrate on the board what is meant by folding parenthesized expressions. Work a few examples.

Review the specification for folding parenthesized expressions. It's a little complicated, so be sure to take enough time for students to really understand it. Once the specification is internalized, review the tests with the test framework including the `ASTNode.subtreeMatch`. Be sure to give time to gain a general *intuitive* understanding of what is provided to support testing. The heavy lifting is all in the `ASTNode.subtreeMatch`.

Return to the `ParethensizeExpressionFolding` class. Implement the `checkRequires` method. Be sure to use the `Utils.requireNonNull` and `Utils.throwRuntimeException` to make the code easy, Run the tests. Create and implement the visitor. Take time to find in the documentation the different types of literals that are supported. All the code should be in the `endVisit` method. Use the `Utils.replaceChildInParent` to modify the AST. Take time to explain the `ASTNode.copySubtree` and why it is important (the AST is linked up and down).

Repeat the whole process, minus the specification, for binary infix expressions for `==` on `NumberLiterals` only. Assume all `NumberLiterals` have type `int`. Only use the `endVisit` method as before. Write the tests first!

# Deprecated Outline

This presentation is deprecated only because the [lab0-constant-folding](https://github.com/byu-cs329/lab0-constant-folding) framework is simpler and more complete. The simplicity comes from it only using the `endVisit` method to accomplish the folding. That is **much** more direct that how `InfixExpressions` are approached in the below presentation. 

Introduce the DOM model via the ASTView plugin on Eclipse. Also demonstrate it with [DOMViewer](https://github.com/byu-cs329/DOMViewer). Connect the tree with the source file. Load the `BinaryAdd.java` file and review the tree. 

[Constant folding](https://en.wikipedia.org/wiki/Constant_folding) is the process where constant expressions are reduced by the compiler before generating code. The goal for this lecture is to implement a visitor that will do constant folding on `InfixExpressions` for the `InfixExpression.Operator.PLUS` when there are only two operands that are of type `NumberLiteral` with tokens that are integers. 
 
Examples:

  * `x = 3 + 7` becomes `x = 10`; (see `src/test/resources/BinaryAdd.java`)
  * `x = 3 + (7 + 4)` becomes `x = 14` (see `src/test/resources/OneNestedBinaryAdd.java`)
  * `x = y + (7 + 4)` becomes `x = y + 11` (see `src/test/resources/VariableWithOneNestedBinaryAdd.java`)

Not that constant folding does not include replacing variables that reduce to constants. 

```java
x = 3 + 7;
y = x;
```

Constant folding for the above gives:

```java
x = 10;
y = x;
```

## Review Basic Framework

There are two important pieces to review: 1) the `ConstantFolding` class with its main method; and 2) the `ConstantFoldingTest` class with its test rigging. Briefly review the use fo the class loader to get the URI (rather clever really).

## How to test?

For the simple tests, it may be easiest to count the number of literals in the folded file and see if that count, and the associated token values, match an expected value. Write a visitor to gather all the tokens associated with `NumberLiterals`.

```java
package edu.byu.cs329.constantfolding;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.NumberLiteral;

public class LiteralTrackerVisitor extends ASTVisitor {
  List<String> literalList = null;
  
  public LiteralTrackerVisitor() {
    literalList = new ArrayList<String>();
  }
  
  @Override
  public boolean visit(NumberLiteral node) {
    literalList.add(node.getToken());
    return true;
  }
}
```

Add in the code to check that the expected value matches the `NumberLiteral` list contents.

```java
private List<String> getLiteralList(ASTNode node) {
    LiteralTrackerVisitor literalTrackerVisitor = new LiteralTrackerVisitor();
    node.accept(literalTrackerVisitor);
    return literalTrackerVisitor.literalList;
  }
  
  @ParameterizedTest(name = "Should return only literal {0} when given file {1}")
  @CsvSource({"7, BinaryAdd.java", "14, OneNestedBinaryAdd.java", "11, VariableWithOneNestedBinaryAdd.java"})
  void Should_ReturnOneLiteral_When_GivenFileWithOneLiteralExpression(String expected, String fileName) {
    URI uri = getURI(fileName);
    Objects.requireNonNull(uri, "Failed to get URI for " + fileName);

    // TODO: add folding test code here
    
    List<String> literals = getLiteralList(node);
    Assertions.assertEquals(1, literals.size());
    String token = literals.get(0);
    Assertions.assertEquals(expected, token);
  }
```

Explain to students that the `assertEquals` uses the `Object.equals` method so it safely compares objects. This behavior is different from the `==` operator. The `assertSame` method uses the `==` operator instead if that is what is desired.

## Constant Folding Algorithm (Binary Only)

Work the examples on the board recursively. Illustrate how the algorithm returns something once it arrives at the leaf nodes, and those value propagate back up the tree. Also note that a `NumberLiteral` has a token, but things that are not of type `NumberLiteral` do not and need to stay the same. And when a new `NumberLiteral` is created, it needs to be passed up. 

The visitor will need to keep track of these return values with a field in the visitor class. Let's call that field `operand`. Let's also create an inner class for the type of the operand that can track the base class `Expression` as well as the value of the expression in the cases where that expression is a `NumberLiteral`.

```java
public class ConstantFoldingVisitor extends ASTVisitor {

  static final Logger log = LoggerFactory.getLogger(ConstantFoldingVisitor.class);

  class Operand {
    Expression expr = null;
    Integer value = null;

    public Operand(final Expression expr) {
      this.expr = expr;
    }

    public Operand(final Expression expr, final Integer value) {
      this.expr = expr;
      this.value = value;
    }
  }

  Operand operand = null;
}
```

Define what happens when the `NumberLiteral` is visited: it sets the `operand` field! Not that all new AST objects are created with a factory pattern. Every node has a reference to the AST that owns that node. That AST provides methods to create new nodes for the AST. In this example, we are creating a new `NumberLiteral`. The reason to create such a *new AST node* is that at some point we may modify the tree, and that **only** works with new AST objects. For example, it is possible to set the expression in the assignment to a new AST expression, even if it is a copy of the old expression. If it is newly created or copied, then all works well. If it is not, then an exception is thrown. In this example, we'll build an entirely new expression from the leaf nodes up (see the copy methods in AST to do it differently).

```java
  @Override
  public boolean visit(NumberLiteral node) {
    String token = node.getToken();
    NumberLiteral numLit = node.getAST().newNumberLiteral(token);
    Integer value = null;

    try {
      value = Integer.decode(token);
    } catch (NumberFormatException e) {
      log.warn("\'" + node.getToken() + "\'" + " is not an integer literal -- skipping");
    }

    operand = new Operand(numLit, value);
    return true;
  }
```

Time to override the `InfixExpression` visitor method. First reject anything not supported: so is not `InfixExpression.Operator.PLUS` or `node.hasExtendedOperands()`. These are unsupported in the lecture so throw an `UnsupportedOperationException`. After that, the implementation is direct. Get the left operand from the accept. Get the right operand from the accept. Then combine them if possible. Either way, the end result should be a new value for the `operand` field.

```java
  private static boolean isIntLiteralExpression(final Operand left, final Operand right) {
    if (left != null && left.value != null && right != null && right.value != null) {
      return true;
    }
    return false;
  }
  @Override
  public boolean visit(InfixExpression node) {
    if (node.getOperator() != InfixExpression.Operator.PLUS) {
      throw new UnsupportedOperationException(
          "Operator \'" + node.getOperator().toString() + "\' is not supported");
    }
    
    if (node.hasExtendedOperands()) {
      throw new UnsupportedOperationException(
          "Extended operands are not supported");
    }

    operand = null;
    node.getLeftOperand().accept(this);
    Operand left = operand;

    operand = null;
    node.getRightOperand().accept(this);
    Operand right = operand;

    AST ast = node.getAST();
    Expression newExpr = null;
    if (!isIntLiteralExpression(left, right)) {
      newExpr = (Expression) (ASTNode.copySubtree(ast, node));
      operand = new Operand(newExpr);
    } else {
      Integer newValue = left.value + right.value;
      newExpr = ast.newNumberLiteral(newValue.toString());
      operand = new Operand(newExpr, newValue);
    }

    return false;
  }
```

At this point, there is a new operand defined for the infix-expression, but the AST tree itself is not using that operand yet. Use the end-visit method to stitch the new operand from the infix-expression into the tree.

```java
  @Override
  public void endVisit(InfixExpression node) {
    Objects.requireNonNull(operand);
    Objects.requireNonNull(operand.expr);
    StructuralPropertyDescriptor location = node.getLocationInParent();
    Objects.requireNonNull(location);
    if (location.isChildProperty()) {
      node.getParent().setStructuralProperty(location, operand.expr);
    } else {
      throw new UnsupportedOperationException(
          "Location \'" + location.toString() + "\' is not supported");
    }
  }
```

Note the use of the `Qbjects.requireNonNull` methods. There should never be a null operand from the infix-expression. This requirement is encoded in the method implementation. 

Here is where the copy matters. Every child has a link back up to its parent node, and care must be taken to maintain the links. The way that happens is by having the parent point to the new node. In this case, whatever node was holding the infix-expression, now points to the new expression. This rewiring as all handled by the `location` in the parent. Once the location is known, then the parent is able to point to the new expression. See the documentation on the DOM for the either locations it might be (`ChildListPropertyDescriptor` or `SimplePropertyDescriptor`). Basically, the expression may be in a list, in which case, it needs to be set in the list (see `node.getParent().structuralPropertiesForType()`). In this case, find the index for the entry for the node in the parent's structural properties, and replace that index with the new node. It should be something like the following:

```java
  List l = (List) old.getParent().getStructuralProperty(p);
  l.set(l.indexOf(old), neo);
```

Here is the entire visitor and test code for reference.

```java
package edu.byu.cs329.constantfolding;

import java.util.Objects;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConstantFoldingVisitor extends ASTVisitor {

  static final Logger log = LoggerFactory.getLogger(ConstantFoldingVisitor.class);

  class Operand {
    Expression expr = null;
    Integer value = null;

    public Operand(final Expression expr) {
      this.expr = expr;
    }

    public Operand(final Expression expr, final Integer value) {
      this.expr = expr;
      this.value = value;
    }
  }

  Operand operand = null;

  private static boolean isIntLiteralExpression(final Operand left, final Operand right) {
    if (left != null && left.value != null && right != null && right.value != null) {
      return true;
    }
    return false;
  }

  @Override
  public boolean visit(NumberLiteral node) {
    String token = node.getToken();
    NumberLiteral numLit = node.getAST().newNumberLiteral(token);
    Integer value = null;

    try {
      value = Integer.decode(token);
    } catch (NumberFormatException e) {
      log.warn("\'" + node.getToken() + "\'" + " is not an integer literal -- skipping");
    }

    operand = new Operand(numLit, value);
    return true;
  }

  @Override
  public boolean visit(InfixExpression node) {
    if (node.getOperator() != InfixExpression.Operator.PLUS) {
      throw new UnsupportedOperationException(
          "Operator \'" + node.getOperator().toString() + "\' is not supported");
    }
    
    if (node.hasExtendedOperands()) {
      throw new UnsupportedOperationException(
          "Extended operands are not supported");
    }

    node.getLeftOperand().accept(this);
    Operand left = operand;

    node.getRightOperand().accept(this);
    Operand right = operand;

    AST ast = node.getAST();
    Expression newExpr = null;
    if (!isIntLiteralExpression(left, right)) {
      newExpr = (Expression) (ASTNode.copySubtree(ast, node));
      operand = new Operand(newExpr);
    } else {
      Integer newValue = left.value + right.value;
      newExpr = ast.newNumberLiteral(newValue.toString());
      operand = new Operand(newExpr, newValue);
    }

    return false;
  }

  @Override
  public void endVisit(InfixExpression node) {
    Objects.requireNonNull(operand);
    Objects.requireNonNull(operand.expr);
    StructuralPropertyDescriptor location = node.getLocationInParent();
    Objects.requireNonNull(location);
    if (location.isChildProperty()) {
      node.getParent().setStructuralProperty(location, operand.expr);
    } else {
      throw new UnsupportedOperationException(
          "Location \'" + location.toString() + "\' is not supported");
    }
  }

}
```

```java
package edu.byu.cs329.constantfolding;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.core.dom.ASTNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.byu.cs329.constantfolding.ConstantFolding;

public class ConstantFoldingTest {

  static final Logger log = LoggerFactory.getLogger(ConstantFoldingTest.class);
  
  private URI getURI(final String fileName) {
    URL url = ClassLoader.getSystemResource(fileName);
    Objects.requireNonNull(url, "\'" + fileName + "\'" + " not found in classpath");
    URI uri = null;
    try {
      uri = url.toURI();
    } catch (URISyntaxException e) {
      log.error("Failed to get URI for" + fileName);
      e.printStackTrace();
    }
    return uri;    
  }
  
  private List<String> getLiteralList(ASTNode node) {
    LiteralTrackerVisitor literalTrackerVisitor = new LiteralTrackerVisitor();
    node.accept(literalTrackerVisitor);
    return literalTrackerVisitor.literalList;
  }
  
  @ParameterizedTest(name = "Should return only literal {0} when given file {1}")
  @CsvSource({"7, BinaryAdd.java", "14, OneNestedBinaryAdd.java", "11, VariableWithOneNestedBinaryAdd.java"})
  void Should_ReturnOneLiteral_When_GivenFileWithOneLiteralExpression(String expected, String fileName) {
    URI uri = getURI(fileName);
    Objects.requireNonNull(uri, "Failed to get URI for " + fileName);

    // TODO: add folding test code here
    ASTNode node = ConstantFolding.fold(uri);    
    
    List<String> literals = getLiteralList(node);
    Assertions.assertEquals(1, literals.size());
    String token = literals.get(0);
    Assertions.assertEquals(expected, token);
  }
  
}
```

# How might you remove if-statements?

If an if-statement has a boolean literal for the expression, then the statement may go away entirely, or it might reduce to an ```EmptyStatement```. Here is code to got started with either way. The visitor will promote the ```ThenStatement``` when the expression is the ```BooleanLiteral``` true.

```java
public class BooleanLiteralTFoldingVisitor extends ASTVisitor {

  private void setNewPropertyInParent(ASTNode oldNode, ASTNode newNode) {
    ASTNode parent = oldNode.getParent();
    StructuralPropertyDescriptor location = oldNode.getLocationInParent();
    
    if (location.isChildProperty()) {
      parent.setStructuralProperty(location, newNode);
      return;
    }
    
    if (location.isChildListProperty()) {
      Object obj = parent.getStructuralProperty(location);
      @SuppressWarnings("unchecked")
      List<ASTNode> propertyList = (List<ASTNode>)obj;
      int index = propertyList.indexOf(oldNode);
      if (index < 0) {
        throw new NoSuchElementException();
      }
      
      propertyList.set(index, newNode);
      return;
    }
    
    throw new UnsupportedOperationException();
  }
  
  @Override
  public void endVisit(IfStatement node) {
    Expression exp = node.getExpression();
    if (!(exp instanceof BooleanLiteral)) {
      return;
    }

    boolean val = ((BooleanLiteral) exp).booleanValue();
    if (!val) {
      throw new UnsupportedOperationException("false not supported");
    }

    ASTNode thenStatement = ASTNode.copySubtree(node.getAST(), node.getThenStatement());
    setNewPropertyInParent(node, thenStatement);
  }

}
```

Here is the test code.

```java
  @Test
  void Should_PromoteThenBody_When_IfConditionIsLiteralTrue() {
    URI uri = getURI("PromoteIfBody.java");
    Objects.requireNonNull(uri, "Failed to get URI for PromoteIfBody.java");

    ASTNode node = ConstantFolding.fold(uri);
    IfStatementGatherVisitor ifg = new IfStatementGatherVisitor();
    node.accept(ifg);
    
    List<Statement> ifs = ifg.litList;
    Assertions.assertEquals(0, ifs.size());
    System.out.print(node);
  }
```

The fold class needs to change to call each of the visitors.

```java
  /**
   * Performs constant folding.
   * 
   * @param file URI to the input Java file
   * @return the root ASTNode for the constant folded version of the input
   */
  public static ASTNode fold(URI file) {

    String inputFileAsString = readFile(file);
    ASTNode node = parse(inputFileAsString);

    NumberLiteralFoldingVisitor cf = new NumberLiteralFoldingVisitor();
    node.accept(cf);
    
    BooleanLiteralTFoldingVisitor bf = new BooleanLiteralTFoldingVisitor();
    node.accept(bf);
    
    return node;
  }
```
