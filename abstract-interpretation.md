# Intro

- Static analyses so far have been classical
- Classical analyses are intraprocedural and sometimes target sub-Turing properties
    - For example, LVA and RD are intraprocedural and can't be used to solve the halting problem
- FV is usually dynamic and usually has limitations in what can be expressed
    - Dafny, for example, uses ILE

# Choose two: soundness, completeness, termination

For these definitions, we need to distinguish between the analyzer and the program being analyzed. More generally, there is a system creating a proof about some other system.

- [Soundness](https://en.wikipedia.org/wiki/Soundness): everything asserted by the analyzer (the system creating the proof) is true of the program being analyzed (the other system).
- [Completeness](https://en.wikipedia.org/wiki/Completeness_(logic)): everything that is true about the program being analyzed can be proven by the analyzer.
- Termination: given infinite memory, the analyzer will always terminate in a finite period of time.

Assuming that both systems are Turing-complete (in software, this is usually true), the analyzer may have any two of these properties - but it can never have all three.

Is testing sound? Complete? Does it terminate? What implications do these properties have on what we do and don't learn from testing?

# Abstract interpretation

An abstract interpreter typically overapproximates possible program behaviors (so it is complete but unsound with respect to which behaviors can happen). However, if this overapproximation is used to prove that some behavior cannot happen, then the abstract interpreter is sound but incomplete with respect to that behavior. Generally speaking, we use abstract interpreters to prove what programs _don't_ do, so they are sound but incomplete with respect to these behaviors.

As its name suggests, an abstract interpreter runs the program. However, it does so in a way that guarantees termination and soundness. First, we'll talk about semantics so we have a way of understanding what it means to run a program (in a mathematical context). Then, we'll modify the system so that the state space is finite (we'll define state space). Next, we'll discuss why we don't learn anything from repeating states. Lastly, we'll discuss the fact that the system we've devised makes it possible for us to simultaneously explore all possible program behaviors and not just behaviors from a particular execution.

## Operational semantics - CE (drop S and K for now)

```java
int summation(int n) {
    int total = 0;
    int i=1;
    while (i<=n) {
        total += i;
        ++i;
    }
    return total;
}
```

In order to execute this program, we need to define semantics. Our semantics will be very simple because of limited class time. In practice, however, we usually desugar (simplify) our programs so that the semantics can be more straightforward. Consider the following transition rules:

```
e' = e[x -> v]  v = eval(exp, e)
--------------------------------
   (x = exp; s, e) -> (s, e')

              eval(c, e) = true
---------------------------------------------
(while (c) b; s, e) -> (b; while (c) b; s, e)

      eval(c, e) = false
-----------------------------
(while (c) b; s, e) -> (s, e)
```

Using these transition rules, we can transition from one state to another until execution is complete. However, this system may run forever (because a program may do so).

## Abstraction to prove termination

Consider the following program:

```java
while (true) {
}
```

In this program, we quickly repeat states. Since we're looking for the absence of behaviors and since we know we can never produce a new state (we've already calculated our next state's successor and it's something we've seen), we can safely conclude that we've captured all program behaviors.

However, the following program generates new states and so our system will never terminate:

```java
x = 0;
while (true) {
    ++x;
}
```

What we need is a system whose states are from a finite set of possibilities. That, coupled with the knowledge that we never need to re-explore a state, allows our interpretation to be finite.

In order to guarantee finiteness, we throw away some precision (in a way that _overapproximates_ the possibilities). For example, we might keep only a number's sign (and not its cardinality). This condenses the infinite set of integers into a set of size 3. However, we sometimes have to merge possibilities together:

```java
int y = random();
int z = random();
int difference = abs(y) - abs(z);
if (difference > 0) {
    x = 6;
} else {
    x = 0;
}
return x;
```

Should `difference` be `-`, `0`, or `+`? It could be any of the three. We need a value set that is capable of reasoning about these combined possibilities. The natural choice is the power set of `{-, 0, +}`.

As a result, which branch should we take? Since we're overapproximating, we need to consider both possibilities. This is why `->` is a transition _relation_ and not a transition function. Instead of producing a linear trace (that may contain a cycle), abstract interpretation produces a graph (that may contain cycles).

As we add more complexity, like adding functions, we lose the ability to be sure that a variable is unique. Everything in our system has to be finite. As a result, we can never replace a value when it is assigned. Instead, we have to _merge_ values together. Our choice of a power set is convenient, as we can easily construct a lattice from the power set. When two values merge, we calculate their least upper bound in the lattice. Any abstraction we choose for which we can construct a join-semilattice will allow us to perform abstract interpretation.

## Simultaneously exploring all program behaviors

This abstraction has an unintended consequence, which we have already seen in some form: it allows us to reason soundly about things we don't know. This means that we can not only reason about nondeterminism from within the program semantics (like `random()`), but also from without it (like from input). This means that we can reason about all possible executions of a program at once, not just about a single execution.

# AAM

If there's time, show the transition rules for Dalvik bytecode.
