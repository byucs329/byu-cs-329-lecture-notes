# Objectives

At the end of the lecture students should have a basic understanding of the visitor pattern and how it is used for computation. A student should be able to

  * Explain the role of the `accept` method
  * Explain the role of the `visit` method
  * Show the ways in which the `visit` method is able to change the traversal either through return value or calling `accept` directly on children.
  * Create visitors and `visit` methods that are able to do useful computation
  * Explain the process and philosophy of Test-Driven Development (TDD)

# Reading

Wikipedia is a good starting point for learning the [visitor pattern](https://en.wikipedia.org/wiki/Visitor_pattern). This lecture ignores complex tree structures with different types for nodes. The key is understanding `accept` and `visit`. Polymorphism uses the same principles only with dynamic dispatch to resolve the type to the correct `visit` method.

Wikipedia is also a good starting point for learning [test-driven development](https://en.wikipedia.org/wiki/Test-driven_development).

# Outline

The visitor pattern is an object-oriented design pattern that is a way of separating an algorithm from an object structure on which it operates.

The visitor pattern is commonly used with linked structures, such as lists, trees, and graphs. The structure's Node class defines a generic traversal in the `accept` method. Each `NodeVisitor` uses this traversal and then performs some operation in the `visit` method and optionally in the `endVisit` method.

## Visitor Pattern

The `accept` method defines the traversal order. The `visit` method defines the computation. Typically the node defines the traversal order (the `accept` method) and the visitor is defined in a separate visitor class. As such the `accept` method takes as input the visitor class that will operate on each node as it is visited.

Pattern the visitor on the DOM visitor so the DOM is familiar to the students. Define the NodeVisitor default implementation:

  * `visit(Node)` called when entering the node and before any children are visited. If it returns false, then the traversal does not visit children. The default implementation returns `true`.
  * `endVisit(Node)` called after the children are visited, and it is called immediately if `visit` returns false. The default implementation does nothing.

```java
package edu.byu.cs329.visitorpattern;

public class NodeVisitor {
  public boolean visit(Node n) {
    return true;
  }
  
  public void endVisit(Node n) {
  }
}
```

Create the `accept` method (in BinaryTreeNode) to call the visit method as appropriate.

```java
 public void accept(NodeVisitor v) {
    boolean doTraverseChildren = v.visit(this);
    
    if (doTraverseChildren) {
      if (left != null) {
        left.accept(v);
      }
      if (right != null) {
        right.accept(v);
      }
    }
    
    v.endVisit(this);
  }
```

Create the `accept` method (in ListNode) to call the visit method as appropriate.

```java
 public void accept(NodeVisitor v) {
    boolean doTraverseNext = v.visit(this);
    
    if (doTraverseNext) {
      if (next != null) {
        next.accept(v);
      }
    }
    
    v.endVisit(this);
  }
```

## Test-Driven Development

Test-driven development (TDD) is a software development process where tests cases are created before code is developed. The claim is that code developed to pass tests is simpler, cleaner, and easily tested than code developed first without any tests.

There are five steps for TDD:

  1. Add a test
  2. Run all tests and see if the new test fails
  3. Write the code to pass the new test
  4. Run tests to make sure new test passes
  5. Refactor code (while re-running tests)
  Repeat
  
## Using TDD to implement Visitors

Use TDD and the visitor pattern to create visitors to compute

  1. `contains`: find a node in the tree or list (return true if present and false otherwise)
  2. `height`: compute the height of the tree or length of the list
  3. `add`: add a node (return true if not there already otherwise false) in order

For `contains` let's start with the `LinkedList`.

Show how to use `@BeforeAll` to setup static objects that persist for all test methods.

```java
@BeforeAll
static void createList() {
  ListNode next = new ListNode(19);
  start = new ListNode(13, next);
  llForTest = new LinkedList(start);
}
```

Create first test in `LinkedListTest` for `contains` visitor where target is the start node.

```java
@Test
@Tag("contains")
@DisplayName("contains returns true when target is start")
void shouldReturnTrue_whenContainsStart() {
  Assertions.assertTrue(llForTest.contains(start.value));
}
```

Implement `ContainsNodeVisitor` to return false to verify that the test fails. Then return true to pass test (note that this is not correct algorithmically, but for TDD it is). We need a test that will lead us to have to traverse the linked list and actual implement the `visit` method.

Create second test in `LinkedListTest` for `contains` visitor where target is the start's next node.

```java
@Test
@Tag("contains")
@DisplayName("contains returns true when target is start's next node")
void shouldReturnTrue_whenContainsStartsNext() {
  Assertions.assertTrue(llForTest.contains(start.next.value));
}
```

This test will fail -- this is the next desired step in TDD. Now implement the code to pass the test. A simple check in the `visit` method to see if the node's value is equal to the target is sufficient. If they are equal, return false to short-circuit the rest of the traversal (quit early).

Now write tests in `BinarySearchTreeTest` starting from the simplest case, i.e. the root, to more complex trees. Use the same `ContainsNodeVisitor`; no code changes should be needed.

```java
  @BeforeAll
  static void createBST() {
    BinaryTreeNode left = new BinaryTreeNode(3);
    BinaryTreeNode right = new BinaryTreeNode(42);
    root = new BinaryTreeNode(13, left, right);
    bstForTest = new BinarySearchTree(root);
  }

  @Test
  @Tag("contains")
  @DisplayName("contains returns true when target is root's left node")
  void shouldReturnTrue_whenContainsRootsLeft() {
    Assertions.assertTrue(bstForTest.contains(root.left.value));
  }

  @Test
  @Tag("contains")
  @DisplayName("contains returns true when target is root's right node")
  void shouldReturnTrue_whenContainsRootsRight() {
    Assertions.assertTrue(bstForTest.contains(root.right.value));
  }
```

Next, create tests in `BinarySearchTreeTest` to drive the implemention of `BinarySearchTree.height()` and the `HeightNodeVisitor`. Add tests in `LinkedListTest` using the same visitor for the `LinkedList.length()` method. The visitor code should not need to be changed.

Finally, create tests in `BinarySearchTreeTest` to drive the implementation of `BinarySearchTree.add()` and the `AddNodeVisitor` -- the visitor should just return the node that will be the immediate root or previous of the node that needs to be added. Add tests in `LinkedListTest` using the same visitor for the `LinkedList.add()` method. The visitor code should not need to be changed.

Note the powerful reuse of the same visitor classes to perform similar operations with different object structures.

### Visitor Implementations

#### Contains

Note that this implementation does *not* take advantage of the property that the structure is a binary tree as it checks every node.

```java 
public class ContainsNodeVisitor extends NodeVisitor {
  boolean isFound = false;
  int target;
  
  public ContainsNodeVisitor(int target) {
    this.target = target;
  }

  @Override
  public boolean visit(Node n) {
    if (n.value == target) {
      isFound = true;
    }
    return !isFound;
  }
}
```

#### Height

```java
public class HeightNodeVisitor extends NodeVisitor {

  int height = 0;
  int currentLevel = 0;
  
  @Override
  public boolean visit(Node n) {
    currentLevel += 1;
    return true;
  }
  
  @Override
  public void endVisit(Node n) {
    if (currentLevel > height) {
      height = currentLevel;
    }
    currentLevel -= 1;
  }
   
}
```

#### Add

The idea of the visitor is to find the node to which the added value should happen, and then add it correctly in the `add` method when the visitor finishes.

```java
public class AddNodeVisitor extends NodeVisitor {
  Node immediateRoot = null;
  
  protected int target = 0;
  
  public AddNodeVisitor(int target) {
    this.target = target;
  }

  @Override
  public boolean visit(Node n) {
    if (immediateRoot != null) {
      return false;
    }
 
    if (n.value == target) {
      immediateRoot = n;
    } else if (target < n.value && n.left != null) {
      n.left.accept(this);
    } else if (target > n.value && n.right != null) {
      n.right.accept(this);
    }
    
    return false;
  }

  @Override
  public void endVisit(Node n) {
    if (immediateRoot != null) {
      return;
    }
    
    immediateRoot = n;
  }
}
```