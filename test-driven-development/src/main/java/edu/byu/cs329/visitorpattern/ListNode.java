package edu.byu.cs329.visitorpattern;

public class ListNode extends Node {

  Node next;

  public ListNode(int value) {
    this.value = value;
    this.next = null;
  }

  public ListNode(int value, Node next) {
    this.value = value;
    this.next = next;
  }
    
  public void accept(NodeVisitor v) {
    // TODO
  }
    
}
