package edu.byu.cs329.visitorpattern;

abstract class Node {

  protected int value;

  public abstract void accept(NodeVisitor nv);
}