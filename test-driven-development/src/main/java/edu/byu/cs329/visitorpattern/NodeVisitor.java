package edu.byu.cs329.visitorpattern;

public class NodeVisitor {
  
  public boolean visit(Node n) {
    return true;
  }

  public boolean visit(ListNode n) {
    return true;
  }

  public boolean visit(BinaryTreeNode n) {
    return true;
  }

  public void endVisit(Node n) {
    // No default implementation
  }
}
