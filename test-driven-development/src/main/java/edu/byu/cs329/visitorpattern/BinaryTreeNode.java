package edu.byu.cs329.visitorpattern;

public class BinaryTreeNode extends Node {

  Node left;
  Node right;

  public BinaryTreeNode(int value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }

  public BinaryTreeNode(int value, Node left, Node right) {
    this.value = value;
    this.left = left;
    this.right = right;
  }
    
  public void accept(NodeVisitor v) {
    // TODO
  }

}
