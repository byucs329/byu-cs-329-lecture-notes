package edu.byu.cs329.visitorpattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Tests for LinkedList Visitors")
public class LinkedListTest {

  public static LinkedList llForTest = null;
  public static ListNode start = null;

  LinkedList testSubject = null;

  @BeforeEach
  void initLinkedList() {
    ListNode node3 = new ListNode(3, null);
    ListNode node2 = new ListNode(2, node3);
    ListNode node1 = new ListNode(1, node2);
    ListNode node0 = new ListNode(0, node1);
    testSubject = new LinkedList(node0);
  }

  @Test
  @DisplayName("Test for PrintVisitor")
  void Test_ForPrintVisitor() {
    ListNode node3 = new ListNode(3, null);
    ListNode node2 = new ListNode(2, node3);
    ListNode node1 = new ListNode(1, node2);
    ListNode node0 = new ListNode(0, node1);

    NodeVisitor printVisitor = new PrintVisitor();

    node0.accept(printVisitor);
  }

  @Nested
  @DisplayName("Tests for LinkedList.contains")
  class LinkedListContainsTests {
    @Test
    @DisplayName("Should return false when given four")
    void should_returnFalse_when_givenFour() {
      boolean rv = testSubject.contains(4);
      Assertions.assertFalse(rv);
    }

    @Test
    @DisplayName("Should return true when given 3")
    void should_returnFalse_when_givenThree() {
      boolean rv = testSubject.contains(3);
      Assertions.assertTrue(rv);
    }
  }

}
