package edu.byu.cs329.visitorpattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Tests for BinarySearchTree Visitors")
public class BinarySearchTreeTest {

  public static BinarySearchTree bstForTest = null;
  public static BinaryTreeNode root = null;
  BinarySearchTree testSubject = null;

  @BeforeEach
  void initTestSubject() {
    BinaryTreeNode left = new BinaryTreeNode(3, null, null);
    BinaryTreeNode rightLeft = new BinaryTreeNode(10, null, null);
    BinaryTreeNode rightRight = new BinaryTreeNode(12, null, null);
    BinaryTreeNode right = new BinaryTreeNode(11, rightLeft, rightRight);
    BinaryTreeNode root = new BinaryTreeNode(5, left, right);
    testSubject = new BinarySearchTree(root);
  }

  @Test
  @DisplayName("Test for PrintVisitor")
  void Test_ForPrintVisitor() {
    BinaryTreeNode left = new BinaryTreeNode(3, null, null);
    BinaryTreeNode rightLeft = new BinaryTreeNode(10, null, null);
    BinaryTreeNode rightRight = new BinaryTreeNode(12, null, null);
    BinaryTreeNode right = new BinaryTreeNode(11, rightLeft, rightRight);
    BinaryTreeNode root = new BinaryTreeNode(5, left, right);
    NodeVisitor printVisitor = new PrintVisitor();

    root.accept(printVisitor);
  }

  @Nested
  @DisplayName("Tests for BinarySearchTree.contains")
  class BinarySearchTreeContainsTests {
    @Test
    @DisplayName("Should return false when given four")
    void should_returnFalse_when_givenFour() {
      boolean rv = testSubject.contains(4);
      Assertions.assertFalse(rv);
    }

    @Test
    @DisplayName("Should return true when given 3")
    void should_returnFalse_when_givenThree() {
      boolean rv = testSubject.contains(3);
      Assertions.assertTrue(rv);
    }
  }

  @Nested
  @DisplayName("Tests for BinarySearchTree.add")
  class BinarySearchTreeAddTests {
    @Test
    @DisplayName("Should return false when given twelve")
    void should_returnFalse_when_givenTwelve() {
      boolean rv = testSubject.add(12);
      Assertions.assertFalse(rv);
    }

    @Test
    @DisplayName("Should return true when given 6")
    void should_returnTrue_when_givenSix() {
      boolean rv = testSubject.add(6);
      Assertions.assertTrue(rv);
    }
  }

}
