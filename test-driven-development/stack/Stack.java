package tdd;

/**
 * This class implements a simple Integer Stack with only push and pop operations.
 * Coded for CS329 using Test-Driven Development (Clean Code style)
 * Includes constraint specifications: requires and ensures clauses
 * @author Brett Decker
 * @version winter-2020
 */
public class Stack {

    private static final int DEFAULT_STORAGE = 100;    // This value is arbitrary
    private int size = 0;
    private Integer[] elements;

    /**
     * Default constructor creates stack with the default storage size.
     */
    public Stack() {
        initalize(DEFAULT_STORAGE);
    }

    /**
     * Default constructor creates stack with the specified storage size.
     */
    public Stack(int storageSize) {
        initalize(storageSize);
    }

    /**
     * Set up the Stack member variables.
     * @param storageSize The desired initial storage size
     */
    private void initalize(int storageSize) {
        elements = new Integer[storageSize];
    }

    /**
     * Get the number of elements in the stack.
     * @ensures size >= 0 && size <= storageSize
     * @ensures size == |stack|
     * @return (size : int) - the number of elements in the stack
     */
    public int size() {
        return size;
    }

    /**
     * Expand the storage size to twice its original and copy over all elements.
     */
    private void growInternalArray() {
        Integer[] copy = new Integer[elements.length * 2];
        for (int i = 0; i < elements.length; i++) {
            copy[i] = elements[i];
        }
        elements = copy;
    }

    /**
     * Push a new top value to the stack.
     * @param value The value to push to the top of the stack
     */
    public void push(Integer value) {
        if (size == elements.length) {
            growInternalArray();
        }
        elements[size++] = value;
    }

    /**
     * Pop and return the top value off the stack.
     * @return (top : Integer) - the top element of the stack
     */
    public Integer pop() {
        Integer result = null;
        if (size > 0) {
            result = elements[--size];
        }
        return result;
    }
}