package tdd;

/**
 * This class implements a simple Integer Stack with only push and pop operations.
 * Coded for CS329 using Test-Driven Development (Clean Code style)
 * Includes constraint specifications: requires and ensures clauses
 * @author Brett Decker
 * @version winter-2020
 * The following are class invariants -- these must be ensured after every method
 * @invariant elements != null
 * @invariant size >= 0
 * @invariant elements.length > 0
 */
public class Stack {

    private static final int DEFAULT_STORAGE = 100;    // This value is arbitrary
    private static final int GROWTH_FACTOR = 2;
    private int size = 0;
    private Integer[] elements;

    /**
     * Default constructor creates stack with the default storage size.
     * @requires initialSize > 0
     * @ensures elements != null
     * @ensures size >= 0
     * @ensures elements.length > 0
     */
    public Stack() {
        initalize(DEFAULT_STORAGE);
    }

    /**
     * Default constructor creates stack with the specified storage size.
     * @requires initialSize > 0
     * @ensures elements != null
     * @ensures size >= 0
     * @ensures elements.length > 0
     * @param initialSize - the desired storageSize
     */
    public Stack(int initialSize) {
        initalize(initialSize);
    }

    /**
     * Set up the Stack member variables.
     * @param storageSize The desired initial storage size
     */
    private void initalize(int storageSize) {
        if (storageSize <= 0) {
            throw new IllegalArgumentException("storageSize");
        }
        elements = new Integer[storageSize];
    }

    /**
     * Get the number of elements in the stack.
     * @ensures size >= 0 AND size <= elements.length
     * @ensures size == |stack|
     * @return (size : int) - the number of elements in the stack
     */
    public int size() {
        return size;
    }

    /**
     * Expand the storage size to twice its original and copy over all elements.
     * @ensures elements[0 ... n] = old(elements[0 .. n])
     * @ensures elements.length = old(elements.length) * GROWTH_FACTOR
     */
    private void growInternalArray() {
        Integer[] copy = new Integer[elements.length * GROWTH_FACTOR];
        for (int i = 0; i < elements.length; i++) {
            copy[i] = elements[i];
        }
        elements = copy;
    }

    /**
     * Push a new top value to the stack.
     * @ensures elements[0 ... n] = old(elements[0 .. n])
     * @enusres elements[n+1] = value
     * @ensures size = old(size) + 1
     * @ensures elements.length >= size
     * @param value The value to push to the top of the stack
     */
    public void push(Integer value) {
        if (size == elements.length) {
            growInternalArray();
        }
        elements[size++] = value;
    }

    /**
     * Pop and return the top value off the stack.
     * @ensures if size > 0 then size = old(size) - 1 AND top = old(elements[n])
     * @ensures if size == 0 then size = 0 AND top = null
     * @ensures elements[0 .. old(n)-1] = old(elements[0 .. n-1])
     * @return (top : Integer) - the top element of the stack
     */
    public Integer pop() {
        Integer top = null;
        if (size > 0) {
            top = elements[--size];
        }
        return top;
    }
}