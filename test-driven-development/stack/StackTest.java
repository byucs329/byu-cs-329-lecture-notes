package tdd;

import java.security.SecureRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Tests for the Stack class.
 * Coded for CS329 using Test-Driven Development (Clean Code style)
 * Equivalence Partions: empty stack | 1 - MAX elements | more than MAX elements
 * @author Brett Decker
 * @version winter-2020
 */
public class StackTest {

    static final int INITIAL_STACK_SIZE = 20;
    static Stack stack;
    static SecureRandom rand;

    /**
     * Create a SecureRandom instance to use for creating random values.
     */
    @BeforeAll
    static void InitialzeRandom() {
        rand = new SecureRandom();
    }

    @BeforeEach
    void CreateEmptyStack() {
        stack = new Stack(INITIAL_STACK_SIZE);
    }

    static int GetRandomInteger() {
        return rand.nextInt() % 1000;
    }

    /**
     * Tests for the 'Empty Stack' Equivalence Partition
     */
    @Nested
    @DisplayName("Empty Stack Tests")
    class EmptyStackChecks {
        
        @Test
        @DisplayName("After creating a stack the reference is not null")
        public void TestCreateStackThenReferenceIsNotNull() {
            assertNotNull(stack);
        }

        @Test
        @DisplayName("After creating a new empty stack the size should be 0")
        public void TestCreateEmptyStackThenSizeIsZero() {
            assertTrue(stack.size() == 0);
        }

        @Test
        @DisplayName("After creating a new empty stack and popping then size should be zero")
        public void CreateEmptyStackPopThenSizeIsZero() {
            stack.pop();
            assertTrue(stack.size() == 0);
        }

        @Test
        @DisplayName("After creating a new empty stack and popping then element should be null")
        public void CreateEmptyStackPopThenElemIsNull() {
            assertNull(stack.pop());
        }
    }

    /**
     * Tests for the '1 - MAX elements' Equivalence Partition
     */
    @Nested
    @DisplayName("Stack Tests (without exceeding maximum inital storage size")
    class WithinSizeStackTests {

        @Test
        @DisplayName("After creating a new empty stack and adding one element size should be 1")
        public void TestCreateEmtpyStackAddOneElemThenSizeIsOne() {
            stack.push(513);
            assertTrue(stack.size() == 1);
        }

        @Test
        @DisplayName("After creating a new empty stack and adding one element and popping size should be 0")
        public void TestCreateEmtpyStackAddOneElemAndPopThenSizeIsZero() {
            stack.push(42);
            stack.pop();
            assertTrue(stack.size() == 0);
        }

        @Test
        @DisplayName("After creating a new empty stack and adding one element and popping we return that first element")
        public void CreateEmtpyStackAddOneElemAndPopThenReturnFirstElem() {
            int value = GetRandomInteger();
            stack.push(value);
            assertEquals(value, stack.pop());
        }

        @Test
        @DisplayName("After creating a new empty stack and adding two elements then size should be two")
        public void CreateEmtpyStackAddTwoElemsThenSizeIsTwo() {
            stack.push(GetRandomInteger());
            stack.push(GetRandomInteger());
            assertTrue(stack.size() == 2);
        }

        @Test
        @DisplayName("After creating a new empty stack and pushing twice and popping twice then we get the first element")
        public void CreateEmptyStackPushTwicePopTwiceThenElemIsFirst() {
            int first = GetRandomInteger();
            stack.push(first);
            int second = GetRandomInteger();
            stack.push(second);
            stack.pop();
            int result = stack.pop();
            assertEquals(first, result);
        }
    }

     /**
     * Tests for the 'more than MAX elements' Equivalence Partition
     */
    @Nested
    @DisplayName("Stack Tests (exceeding maximum inital storage size")
    class ExceedsSizeStackTests {

        @Test
        @DisplayName("Push more times than the inital storage size and pop then should get the last pushed element")
        public void PushOverStorageAndPopThenGetLastPushed() {
            for (int i = 0; i < INITIAL_STACK_SIZE; i++) {
                stack.push(GetRandomInteger());
            }
            int last = GetRandomInteger();
            stack.push(last);
            assertEquals(last, stack.pop());
        }

        @DisplayName("Parameterized tests for stack")
        @ParameterizedTest(name = "Should return the correct elements when pushing {0} times and popping {1} times")
        @CsvSource({"6,5", "13,9", "1, 1", "513, 513"})
        public void ShouldReturnCorrectElementWithAnyNumberOfPushesAndPops(int numPushes, int numPops) {
            int[] values = new int[numPushes];
            int i = 0;
            for (; i < numPushes; i++) {
                values[i] = GetRandomInteger();
                stack.push(values[i]);
            }
            for (i--; i > numPops - 1; i--) {
                assertEquals(values[i], stack.pop());
            }
        }
    }
}