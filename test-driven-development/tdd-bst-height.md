# BinarySearchTree with Test-Driven Development

This document shows how to incrementally add tests for the `BinarySearchTree` to driven the implementation of the `HeightNodeVisitor`. The final code is also given.

NOTE: using the visitor pattern to compute the height of a tree is not an effective use, but it is a good example. Effective uses are computing the `contains` and `add` operations, shown elsewhere in this directory.

# TDD Process

Write a test that asserts the height of a tree with a single node as root should be one:
```java
  @Test
  @Tag("height")
  @DisplayName("height returns 1 when just root")
  void shouldReturnOne_whenHeightJustWithRoot() {
    BinaryTreeNode root2 = new BinaryTreeNode(1);
    BinarySearchTree tree = new BinarySearchTree(root2);
    Assertions.assertEquals(1, tree.height());
  }
```
Then, add as little implementation to the `HeightNodeVisitor` as possible, to just get the test to compile and fail:
```java
public class HeightNodeVisitor extends NodeVisitor {

  private int height = 0;
  
  public int getHeight() {
    return height;
  }
  
  public boolean visit(Node n) {
    return true;
  }

  public void endVisit(Node n) {
    // empty
  }
```
Now that the test fails, make the test pass by setting changing the intial value of height to be one: `private int height = 1`

Write a test that will force us to change the implementation (height being hardcoded to one), by testing the height of tree that has a root with a left child:
```java
  @Test
  @Tag("height")
  @DisplayName("height returns 2 when just root and left")
  void shouldReturnTwo_whenHeightWithRootAndLeft() {
    BinaryTreeNode left = new BinaryTreeNode(0);
    BinaryTreeNode root2 = new BinaryTreeNode(1, left, null);
    BinarySearchTree tree = new BinarySearchTree(root2);
    Assertions.assertEquals(2, tree.height());
  }
```
Since that test already fails, we just need to make it now pass. The easiest way with the least amount of code is to just increment `height` in the visit method:
```java
  public boolean visit(Node n) {
    return true;
  }
```
Now the second test passes and we need a new test. Take a pause and discuss with the students how to choose the next test.

E.g., does testing a tree with this structure force us to change our implementation? No.
```
   root
   /
  left
  /
 leftleft
``` 
 
What about this tree? Yes!
```
      root
     /    \
  left    right
          /
         rightleft
```
 
Add this test to our suite:
```java
  @Test
  @Tag("height")
  @DisplayName("height returns 3 when just root with left and right and right with left")
  void shouldReturnThree_whenHeightWithRootAndLeftAndRightWithLeft() {
    BinaryTreeNode rightsLeft = new BinaryTreeNode(3);
    BinaryTreeNode left = new BinaryTreeNode(0);
    BinaryTreeNode right = new BinaryTreeNode(5, rightsLeft, null);
    BinaryTreeNode root2 = new BinaryTreeNode(1, left, right);
    BinarySearchTree tree = new BinarySearchTree(root2);
    Assertions.assertEquals(3, tree.height());
  }
```
It fails, so now we need to make it pass. We *finally* have to code the correct implementation: 1) decrement `height` in `endVisit` and 2) keep track of the maximum current height
```java
  private int currentHeight = 0;
  ...
  public void endVisit(Node n) {
    if (currentHeight > height) {
      height = currentHeight;
    }
    currentHeight--;
  }
```
NOTE: 2) can be done in either the `visit` method after the increment or in the `endVisit` method before the decrement

How do we know when we're done adding new tests? Can we think up another test that would make us have to modify the implementation? -- I cannot, so we're done.

The `HeightNodeVisitor` can be used as-is to implement the `length` method in the `LinkedList`. Just add tests to `LinkedListTest`.

# Final Code

## BinarySearchTree

```java
package edu.byu.cs329.visitorpattern;

import java.util.Objects;

public class BinarySearchTree {
    
  protected BinaryTreeNode root;

  public BinarySearchTree(BinaryTreeNode root) {
    Objects.requireNonNull(root);
    this.root = root;
  }

  public boolean contains(int value) {
    ContainsNodeVisitor cnv = new ContainsNodeVisitor(value);
    root.accept(cnv);
    return cnv.getIsFound();
  }
  
  public int height() {
    HeightNodeVisitor hnv = new HeightNodeVisitor();
    root.accept(hnv);
    return hnv.getHeight();
  }

  public boolean add(int value) {
    // TODO
    return false;
  }
}
```

## BinarySearchTreeTest

```java
package edu.byu.cs329.visitorpattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class BinarySearchTreeTest {

  public static BinarySearchTree bstForTest = null;
  public static BinaryTreeNode root = null;

  @BeforeAll
  static void createBST() {
    BinaryTreeNode left = new BinaryTreeNode(3);
    BinaryTreeNode right = new BinaryTreeNode(42);
    root = new BinaryTreeNode(13, left, right);
    bstForTest = new BinarySearchTree(root);
  }

  @Test
  @Tag("contains")
  @DisplayName("constains should return false when target is not in tree")
  void shouldReturnFase_whenContainsNotInTree() {
    Assertions.assertFalse(bstForTest.contains(0));
  }

  @Test
  @Tag("contains")
  @DisplayName("contains should return true when target is root's value")
  void shouldReturnTrue_whenContainsRoot() {
    Assertions.assertTrue(bstForTest.contains(root.value));
  }

  @Test
  @Tag("contains")
  @DisplayName("contains returns true when target is root's left node")
  void shouldReturnTrue_whenContainsRootsLeft() {
    Assertions.assertTrue(bstForTest.contains(root.left.value));
  }

  @Test
  @Tag("contains")
  @DisplayName("contains returns true when target is root's right node")
  void shouldReturnTrue_whenContainsRootsRight() {
    Assertions.assertTrue(bstForTest.contains(root.right.value));
  }

  @Test
  @Tag("height")
  @DisplayName("height returns 1 when just root")
  void shouldReturnOne_whenHeightJustWithRoot() {
    BinaryTreeNode root2 = new BinaryTreeNode(1);
    BinarySearchTree tree = new BinarySearchTree(root2);
    Assertions.assertEquals(1, tree.height());
  }

  @Test
  @Tag("height")
  @DisplayName("height returns 2 when just root and left")
  void shouldReturnTwo_whenHeightWithRootAndLeft() {
    BinaryTreeNode left = new BinaryTreeNode(0);
    BinaryTreeNode root2 = new BinaryTreeNode(1, left, null);
    BinarySearchTree tree = new BinarySearchTree(root2);
    Assertions.assertEquals(2, tree.height());
  }

  @Test
  @Tag("height")
  @DisplayName("height returns 3 when just root with left and right and right with left")
  void shouldReturnThree_whenHeightWithRootAndLeftAndRightWithLeft() {
    BinaryTreeNode rightsLeft = new BinaryTreeNode(3);
    BinaryTreeNode left = new BinaryTreeNode(0);
    BinaryTreeNode right = new BinaryTreeNode(5, rightsLeft, null);
    BinaryTreeNode root2 = new BinaryTreeNode(1, left, right);
    BinarySearchTree tree = new BinarySearchTree(root2);
    Assertions.assertEquals(3, tree.height());
  }
```

## HeightNodeVisitor

```java
package edu.byu.cs329.visitorpattern;

public class HeightNodeVisitor extends NodeVisitor {

  private int height = 0;
  private int currentHeight = 0;

  public int getHeight() {
    return height;
  }

  public boolean visit(Node n) {
    currentHeight++;
    // if statement in `endVisit` could alternatively be here
    return true;
  }

  public void endVisit(Node n) {
    if (currentHeight > height) {
      height = currentHeight;
    }
    currentHeight--;
  }
}
```