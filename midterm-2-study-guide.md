# Overview

Midterm 2 covers white-box testing, property-based testing, program dependence graphs, and static type checking.   The target is 1.5 to 2 hours of student time to complete. The format is short answer/free response. One 8.5x11 page of notes with data on any side is allowed.

# Resources

This repository has entries for each topic on the test with suggested readings and learning activities.

# Comprehensive List of Topics

The below is a comprehensive list of topics on the exam. If a topic is not on the below list, then that is not being tested for this course. Also included is some indication to what each question asks to do in regards to the topic.

  * Write [white-box](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/whitebox/white-box.md) tests for object-level branch coverage, otherwise known as decision coverage, for a given piece of code (see HW3 and Lab3).
  * Write [white-box](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/whitebox/white-box.md) for Modified Condition/Decision Coverage (MC/DC). Compare and contrast that test set with on that only gives object-level branch coverage (e.g., decision coverage).
  * Write tests for [stateless property-based testing](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/pbt-stateless.md) for a given piece of code. Questions are similar to HW3.
  * Write tests for [stateful property-based testing](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/pbt-stateful.md) for a given object. Explain the role of the prefix and suffix for some of the tests. Questions are similar to HW3.
  * Compute the post-dominance tree for a piece of code and its augemented control flow graph (e.g., a `start` node is added that goes to entry on true and exit on false) See HW4.
  * Compute the [program dependence graph](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/pdg.md) from a given piece of code. Be sure to show the the data-dependence graph and the control-dependence graph needed to create the program dependence graph. See HW4. 
  * Explain the meaning of a given [program dependence graph](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/pdg.md) and how it might be used in practice. See HW4.
  * Write a [type-proof](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/type-checking.md) ([compiler notes](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/compilers/09-type-checking.ppt)) for a given piece of code. See HW4 and Lab3.
