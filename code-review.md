# Intro

These lecture notes are extracted from http://extras.springer.com/2002/978-3-642-59413-7/4/rom/pdf/Fagan_hist.pdf, which seems to be the original document describing the process of code inspection.

# Team members

1) Moderator

    Drives the code review. Acts as a coach and must pay attention to personal needs. Training is advantageous. It is usually best to choose a moderator from a different team.

2) Designer

    The person who designed the program.

3) Implementor

    The person who wrote the code.

4) Tester

    The programmer responsible for testing.

If the designer and the implementor are the same person, this is fine. If this person is also the tester (IRL), another programmer should step in.

# Steps

1) Overview

    The designer describes the problem being addressed and the specific things being designed or implemented. Documentation is distributed to all participants. This step is particularly important for a design review.

2) Preparation (individual)

    Participants individually use the documentation to attempt to understand the design or code, its intent, and its logic. Focusing on common errors (presumably, errors can be ranked by prevalence based on studies of relevant corpora) can help focus code reviews on the components most likely to have errors. Apparently, there are also checklists to review.

3) Inspection

    The moderator chooses a reader from among the participants. Usually, this is the implementor. For a design review, the implementor describes how he or she will implement the design. The review must cover every piece of logic and every branch at least once. Higher-level documentation, designs, etc. must be available during the inspection.

    Once the design is understood, the inspection focuses on finding errors. When an error is identified, the moderator records it, including its type and severity. The inspection continues. If the solution is obvious, it is also noted.

    The moderator produces a written report of the inspection within one day of its occurrence.

4) Rework

    Fix the problems identified during step 3.

5) Follow-Up

    The moderator must ensure that errors are fixed. If more than 5% of the material has been reworked, a complete re-inspection should follow. If less than 5% of the material has been reworked, the moderator determines whether he or she can verify the changes, whether the changes should be re-inspected, or whether a complete re-inspection is necessary.
