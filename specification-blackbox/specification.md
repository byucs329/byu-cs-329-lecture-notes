# Reading:

  * Chalmers [Course Introduction](https://docs.google.com/presentation/d/19HIyDfVKHF4ypJTvPIWrJtWG4Es9BSusfkzF1T7nWiE/edit) has a great section on specification. 
  * [specification.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/specification-blackbox/specification.md) and [specification.pptx](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/specification-blackbox/specification.pptx)

# Objectives

  * Write pre and post conditions for methods with `@requires` and `@ensures` annotations
  * Recognize good and bad specifications
  * Explain the implicit properties

# Terminology

  * **Supplier**: the callee and the implementer of the method
  * **Client**: the caller using the method

A **contract** has two parts:

  * **Requires**: what the **client** must ensure, sometimes called a pre-condition
  * **Ensures**: what the **supplier** must ensure, sometimes called a post-condition

The contract should be written in terms of the input (requires), output (ensures), and internal state (requires and ensure). When reasoning about an object, it is helpful to define notation for the state of the object. The state should generally be *model based* meaning it is not the actual state for how the object is implemented, but the state of the idealized object regardless of implementation. Use the *old* keyword to refer to any previous state of the object for example to compare the new state to the old state in any ensures clause.

A few implicit properties that do not need to be stated:

  * Input/Output of correct type
  * No abnormal execution
  * No exceptions unless its a declared non-runtime exception
  * No modification of inputs or globals unless stated otherwise

It is still necessary to state clearly when the internal state does not change (observer methods), and when the internal state does change, exactly how it changes.

Work the *sort.java* example in class. Show how it is important to specify relations between input and output as well as restrictions on the input itself. The specification should make clear what is valid output for a given input and what is not valid (see *specification.pptx*)

A short note on exceptions: runtime exceptions are not part of the specification in this course. That is an arbitrary choice. So a specification only indicates exceptions when there is a throw-clause with a non-runtime exception.

As a note, the `@ensures` and `@requires` are tags defined in the `pom.xml` file. JavaDoc recognizes and generates those tags. Add a tag for each requirement. JavaDoc will separate them with commas.

# Sort Example

The goal here is to show just how specific the specification needs to be. The goal of the contract is to be able to sort the good output from the bad output. Start with the basic requirement that the set be sorted using the default input set.

```java
static Stream<Vector<Integer>> arrayProvider() {
    Integer[] arrayIn1 = { 3, 2, 5 };
    Vector<Integer> in1 = new Vector<Integer>(Arrays.asList(arrayIn1));

    Integer[] arrayIn2 = { 17 };
    Vector<Integer> in2 = new Vector<Integer>(Arrays.asList(arrayIn2));

    Integer[] arrayIn3 = {};
    Vector<Integer> in3 = new Vector<Integer>(Arrays.asList(arrayIn3));

    return Stream.of(in1, in2, in3);
  }

  static boolean isSorted(Vector<Integer> array) {
    Integer prev = null;
    for(Integer i : array) {
      if (prev != null && i > prev) {
        return false;
      }
    }
    return true;
  }
  
  @ParameterizedTest
  @MethodSource("arrayProvider")
  void Should_returnSortedArray_When_givinAnyArray(Vector<Integer> array) {
    Vector<Integer> out = sort(array);
    Assertions.assertTrue(isSorted(out));
  }

  /**
   * Sorts the array from least to greatest.
   * 
   * @ensures forall i in indices(out), i < out.size()-1 ==> out[i] < out[i+1] 
   * 
   * @param array array to be sorted
   * @return (out : Vector<Integer>) a sorted version of array  
   */
  static Vector<Integer> sort(Vector<Integer> array) {
    return Sort.sort(array);
  }
```

On the surface, everything seems OK. Let's look at the actual output though that is in the console from the logger. Need a stronger specification: output is sorted and contains only elements from array.

```java
static boolean hasOnlyElementsFromArray(Vector<Integer> out, Vector<Integer> array) {
    for (Integer i : out) {
      if (array.indexOf(i) == -1) {
        return false;
      }
    }
    return true;
  }

/**
   * Sorts the array from least to greatest.
   * 
   * @ensures forall i in indices(out), i < out.size()-1 ==> out[i] < out[i+1] 
   * @ensures forall i, i \in out <==> i \in array
   * 
   * @param array array to be sorted
   * @return (out : Vector<Integer>) a sorted version of array  
   */
  static Vector<Integer> sort(Vector<Integer> array) {
    return Sort.sort(array);
  }
```

Add in a new test input for the array `{1,2}`. Run the tests again. The contract is still not strong enough. Strengthen to be a permutation of the original array.

```java 

static int count(Integer target, Vector<Integer> array) {
    int count = 0;
    for (Integer i : array) {
      if (i == target) {
        ++count;
      }
    }
    return count;
  }
  
  static boolean sameCounts(Vector<Integer> out, Vector<Integer> array) {
    for (Integer i : out) {
      int countOut = count(i, out);
      int countArray = count(i, array);
      if (countOut != countArray) {
        return false;
      }
    }
    return true;
  }
  
@ParameterizedTest
  @MethodSource("arrayProvider")
  void Should_returnSortedArray_When_givinAnyArray(Vector<Integer> array) {
    Vector<Integer> out = sort(array);
    log.info(array.toString() + " ==> " + out.toString());
    
    Assertions.assertTrue(isSorted(out));
    Assertions.assertTrue(hasOnlyElementsFromArray(out,array)); 
    Assertions.assertTrue(sameCounts(out, array));
  }

  /**
   * Sorts the array from least to greatest.
   * 
   * @ensures forall i in indices(out), i < out.size()-1 ==> out[i] < out[i+1] 
   * @ensures forall i in out, (exists j in array, j == i)
   * @ensures out \in Permutations(array)
   * 
   * @param array array to be sorted
   * @return (out : Vector<Integer>) a sorted version of array  
   */
  static Vector<Integer> sort(Vector<Integer> array) {
    return Sort.sort(array);
  }
```

Add in a test for null. Add the requires class that the input is not null.

```java
 @Test 
  void Should_throwNullException_When_arrayIsNull() {
    Assertions.assertThrows(NullPointerException.class, () -> sort(null));
  }

  /**
   * Sorts the array from least to greatest.
   *
   * @requires array != null
   * 
   * @ensures forall i in indices(out), i < out.size()-1 ==> out[i] < out[i+1] 
   * @ensures forall i in out, (exists j in array, j == i)
   * @ensures out \in Permutations(array)
   * 
   * @param array array to be sorted
   * @return (out : Vector<Integer>) a sorted version of array  
   */
  static Vector<Integer> sort(Vector<Integer> array) {
    return Sort.sort(array);
  }
```

# IntegerSet

Class Activity: write the specification for the `IntegerSet` class in the `Set` repository.

```java
package edu.byu.cs329.blackbox;

import java.util.ArrayList;
import java.util.List;

public class IntegerSet {
  protected List<Integer> elements;

  private IntegerSet(List<Integer> elems) {
    elements = new ArrayList<>(elems);
  }

  /**
   * Creates an empty set.
   * 
   * @ensures this == {}
   */
  public IntegerSet() {
    elements = new ArrayList<>();
  }

  /**
   * Returns the size of this set (e.g., its rank).
   * 
   * @ensures size = |this|
   * 
   * @return (size : int) the number of unique elements in the set
   */
  public int size() {
    return elements.size();
  }

  /**
   * Adds an element to the set.
   * 
   * @requires x != null
   * @ensures this == {x} \cup old(this)
   * 
   * @param x is the element to add
   */
  public void addElement(Integer x) {
    for (int i = 0; i < elements.size(); ++i) {
      if (x >= elements.get(i)) {
        elements.add(i, x);
        return;
      }
    }
    elements.add(x);
  }

  /**
   * Removes an element from the set.
   * 
   * @requires x != null
   * @ensures this = old(this) \ {x}
   * 
   * @param x the element to remove from the set
   */
  public void removeElement(Integer x) {
    elements.remove(x);
  }

  /**
   * Determines if an element is in the set.
   * 
   * @requires x != null
   * @ensures true iff x in this and false otherwise
   * 
   * @param x the element to remove from the set.
   * @return true iff x in this
   */
  public boolean contains(Integer x) {
    return elements.contains(x);
  }

  /**
   * Returns the intersection of two sets.
   * 
   * @requires s != null
   * @ensures out == this \cap s
   * 
   * @param s the set to intersect with this
   * @return (out : IntegerSet) out is the intersection with the input set s and this
   */
  public IntegerSet intersect(IntegerSet s) {
    List<Integer> elems = new ArrayList<>(s.elements);
    elems.removeIf((Integer i) -> !s.contains(i));
    return new IntegerSet(elems);
  }

  /**
   * Returns the union of two sets.
   * 
   * @requires s != null
   * @ensures out == this \cup s
   * 
   * @param s the set to union with
   * @return (out : IntegerSet) out is the union with the input set
   */
  public IntegerSet union(IntegerSet s) {
    List<Integer> elems = new ArrayList<>(s.elements);
    elems.addAll(s.elements);
    return new IntegerSet(elems);
  }
}
``` 