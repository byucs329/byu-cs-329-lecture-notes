# Reading

The Art of Software Testing, pp 49-61 or any test book's section on black-box testing. Google has many resources too such as [this article](https://www.guru99.com/equivalence-partitioning-boundary-value-analysis.html).

# Objectives
  
  * Apply input partitioning to a specification to create a set of tests
  * Use boundary value analysis to augment a set of tests

# Forward

Define the method or object state *before* doing black-box test and write the specification for each method or function to be tested. Be sure the notation is simple to describe the effects of each method on the state of the object. The specification is critical to defining any black-box tests.

# Equivalence Partitioning

For each input, based on the specification, divide the input space into valid and invalid partitions:

 * If input in a range, then need at least one valid in the range and two invalid outside the low end and high end of range
 * If input represents an allowed number of things, then one in the range, and one outside such as zero or more than the max
 * If input is a set of values and each is handled differently, then one valid for each one and one invalid
 * If input is a *must-be* condition, then the valid is easy and have one invalid.

See list of page 52 for details.

Key: must define invalid partitions as well as valid for each input.

## Identifying tests

  1. Assign unique ID to each equivalence class
  2. Repeat until all **valid** classes are covered by tests
       * Write a test to cover as many valid classes as possible
  3. Repeat until all invalid classes are covered by tests
       * Write a test to cover one, and only one, of the uncovered invalid classes

Notice that valid classes are *not* interesting

# Boundary Value Analysis

Boundaries on the edges of equivalence classes often reveal defects:

  * Select tests so that each edge of each partition, or boundary, is subject to test
  * Rather than only focus on input, create tests that cover output equivalence classes

No easy formula or recipe to follow.

# Class Standards:

* The specification should make clear the requires and ensures for correct output
* The ensures clauses must be checked in any test
* Invalid input that violates any requires clause should be tested and checked that it throws a Java Runtime exception: use `Assertions.assertThrows`.
* Ensures only includes something about exceptions if there is a throws-clause with a non-runtime exception; otherwise any exception is implicit, runtime, and checked as part of the invalid input from the equivalence partitioning.  
 * Pick a naming convention for the tests and be consistent with it!

Write tests for the `IntegerSet`. Each code-shop will set it's own standard for testing invalid inputs that violate the specification. 

# Cause-effect Graphing or Decision Tables (not covered)

Required whenever the output depends on different combinations of the input. Each of those combinations should be identified with the appropriate expected output. There should be a test for each input combination in addition to any other tests from equivalence partitioning and boundary value analysis.
