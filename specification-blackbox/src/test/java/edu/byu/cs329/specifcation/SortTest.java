package edu.byu.cs329.specifcation;

import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.byu.cs329.specification.Sort;

@DisplayName("Tests for Sort")
public class SortTest {

  final Logger log = LoggerFactory.getLogger(SortTest.class);
  
  static Stream<Vector<Integer>> arrayProvider() {
    Integer[] arrayIn1 = { 3, 2, 5 };
    Vector<Integer> in1 = new Vector<Integer>(Arrays.asList(arrayIn1));

    Integer[] arrayIn2 = { 17 };
    Vector<Integer> in2 = new Vector<Integer>(Arrays.asList(arrayIn2));

    Integer[] arrayIn3 = {};
    Vector<Integer> in3 = new Vector<Integer>(Arrays.asList(arrayIn3));

    Integer[] arrayIn4 = {1, 2};
    Vector<Integer> in4 = new Vector<Integer>(Arrays.asList(arrayIn4));

    return Stream.of(in1, in2, in3, in4);
  }
  
  @DisplayName("Tests for SortTest.sort")
  @ParameterizedTest(name = "Should return sorted array when given {0}")
  @MethodSource("arrayProvider")
  void testsForSort(Vector<Integer> array) {
    Vector<Integer> out = sort(array);
    log.info("Sort.sort(" + array + ") == " + out);
  }
  
  /**
   * Sorts the array from least to greatest.
   * 
   * @param array array to be sorted
   * @return (out : Vector<Integer>) a sorted version of array  
   */
  static Vector<Integer> sort(Vector<Integer> array) {
    return Sort.sort(array);
  }

}
