package edu.byu.cs329.blackbox;

import java.util.ArrayList;
import java.util.List;

public class IntegerSet {
  protected List<Integer> elements;

  private IntegerSet(List<Integer> elems) {
    elements = new ArrayList<>(elems);
  }

  /**
   * Creates an empty set.
   * 
   */
  public IntegerSet() {
    elements = new ArrayList<>();
  }

  /**
   * Returns the size of this set (e.g., its cardinality).
   * 
   * @return (size : int) the number of unique elements in the set
   */
  public int size() {
    return elements.size();
  }

  /**
   * Adds an element to the set.
   * 
   * @param x is the element to add
   */
  public void addElement(Integer x) {
    for (int i = 0; i < elements.size(); ++i) {
      if (x >= elements.get(i)) {
        elements.add(i, x);
        return;
      }
    }
    elements.add(x);
  }

  /**
   * Removes an element from the set.
   * 
   * @param x the element to remove from the set
   */
  public void removeElement(Integer x) {
    elements.remove(x);
  }

  /**
   * Determines if an element is in the set.
   * 
   * @param x the element to remove from the set.
   * @return true iff x in this
   */
  public boolean contains(Integer x) {
    return elements.contains(x);
  }

  /**
   * Returns the intersection of two sets.
   * 
   * @param s the set to intersect with this
   * @return (out : IntegerSet) out is the intersection with the input set s and this
   */
  public IntegerSet intersect(IntegerSet s) {
    List<Integer> elems = new ArrayList<>(s.elements);
    elems.removeIf((Integer i) -> !s.contains(i));
    return new IntegerSet(elems);
  }

  /**
   * Returns the union of two sets.
   * 
   * @param s the set to union with
   * @return (out : IntegerSet) out is the union with the input set
   */
  public IntegerSet union(IntegerSet s) {
    List<Integer> elems = new ArrayList<>(s.elements);
    elems.addAll(s.elements);
    return new IntegerSet(elems);
  }
}
