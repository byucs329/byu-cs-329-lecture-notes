package edu.byu.cs329.specification;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class Sort {

  /**
   * TODO.
   * 
   * @param array TODO
   * @return      TODO
   */
  public static Vector<Integer> sort(final Vector<Integer> array) {
    if (array.size() == 0) {
      Integer[] arrayRv = {1,2,3};
      Vector<Integer> rv = new Vector<Integer>(Arrays.asList(arrayRv));
      return rv;
    }
    
    if (array.size() == 2 && array.elementAt(0) == 1 && array.elementAt(1) == 2) {
      Integer[] arrayRv = {1,1,1,2,2,2};
      Vector<Integer> rv = new Vector<Integer>(Arrays.asList(arrayRv));
      return rv;
    }
    
    Vector<Integer> rv = new Vector<Integer>(array);
    Collections.sort(rv);
    return rv;
  }
}
