# Objectives

  * Explain the internal model and how git stores revisions
  * Effectively use the index to stage and un-stage commits (including adding and removing files from the index and restoring deleted files)
  * Manage branches including merging, re-basing, and deleting
  * Compare changes between revisions with `git diff`
  * Work with remote repositories
  * Create feature branches and pull requests

# Reading

  * [Git](https://git-scm.com/)
  * [Git for Dummies](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/maven-lint-javadoc-git/Git-for-dummies.pptx), and [feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)

# What is Git?

Git is a *distributed revision control* system. Revision control is a way to track changes to files by storing snapshots of files. Snapshots are created by the user. Revision control lets the user retrieve those snapshots at any time. *Distributed* means that there is no **central repository** to hold the snapshots. everyone has a local copy of the entire repository, creates local revisions, and shares those with others from time to time.

In simple terms, it means that it is no longer necessary to copy a file before making changes!

# How Git Works

Draw two repositories (cylinders). Connect them with `push` and `pull`. Under one of the repositories, draw the index (triangle) and working directory (cloud). Connect the index to the working directory with `add` and `reset`. Connect the index to the repository with `commit`. Connect the repository to the working directory with `checkout`. The commands are how to interact with each entity. 

# Cloning and Configuring repository 

A `clone` makes an copy of an existing repository and does a `checkout` of the master branch by default. This workshop uses [Github](github.com) that is the most common way to use git where a main repository is hosted in the cloud and developers `clone` that repository to a local machine to develop. A developer then uses `push` to send changes to the central repository to share with other developers. 

Create the github repository by following the classroom link. Clone the repository.

As a note, it's possible to just create a new local repository with `git init` in a directory.

Configure `user.name` and `user.email` with `git configure`: `git configure --global user.name "Eric Mercer"` 

# Managing the Index and Local Changes

The role of the index is to stage changes for a commit. It is a convenient way to group changes in sensible ways when things get complex.

`git status` is a best friend. It shows what is in the index (**Changes to be committed**), what is changed but not in the index (**Changes not staged for commit**), and what is not part of the repository (**Untracked files**). The command includes helpful hints to moving files between the index.

 `.gitignore` quiets `git status` so that it only shows things that matter relative to untracked files. Only files that are needed to build a project should be in the repository. Build artifacts are typically not included. Also not included are the various dot-files created by IDEs and project environments.
 
 `git add <file>` puts something in the index. `git reset HEAD <file>` removes something from the index. `HEAD` is a reference to the most recent commit (e.g., snapshot). Only things in the index are added to the repository on a commit. `git reset --hard HEAD <file>` removes a file from the index **and** discards all the changes.

 `git checkout -f <file>` discards any changes in the local directory and makes the file match the most recent commit. It's possible to omit the file in reset or checkout to apply it to all files in the index (reset) or all files that are modified.

 Changing a file *after* it is added to the index **does not** stage those changes for the commit. `git status` will show it in the index **and** that it is modified. Try it and see. The `git add` snapshots the file and adds the snapshot to the index. Underneath the `git add` is creating a blob which is the file named by its SHA1 hash key. Same hash key means same file. Changing the file in the working directory does not affect that snapshot. Simply add the file again to update the index with the new snapshot if desired.

# Committing Changes

`git commit` creates a snapshot. It requires a message. Should be in present tense: *Add patch for missing icon**, *Fix failure on input x*, etc. In general, the first line of the commit should not be more than 72 characters (50 is better). More explanation can be given on the next line(s). `git commit -m "Demonstrate commit message"` skips the editor --- it is possible to configure the editor but that is out of scope: `core.editor`.

`git history` shows the history of commits. Each commit is identified by a SHA1 hash key. The hash key comes from the internal tree representation working directory with the staged changes in the index.

# Creating and Merging Branches

`git checkout` is how different versions of files are retrieved from the repository into the working directory. These are typically organized as branches. A branch is a named commit: `git branch <name>` creates a new branch at `HEAD` but does not change the working directory while `git checkout -b <name>` creates the branch and does the checkout to change the working directory. `git branch` shows the local branches, and `git checkout <branch>` changes the working directory between branches. Not that `<branch>` can actually be any commit in the history. Also not that only the first few numbers (enough to uniquely identify it) need to be typed for git to figure out what is wanted.

Merging changes from other branches is handled with `git merge`. Git tries to automate as much of the merge as possible, but sometimes there are *conflicts*. A conflict is when two commits make changes to the same code. Git reports the conflict, and `git status` has a new category that is **Both modified** meaning that files in this category are modified by both sides of the commit. The conflicts are annotated in each file, and the programer must resolve the conflict by chose how to weave the conflicting edits.

A conflict is resolved when the developer weaves the two commits and removes the annotations (show example). Once that is done, use `git add` to stage the resolved files and finish then finish the merge with a commit. Notice that git has already staged all the changed files in the index with the exception of files with conflicts that it leaves to the developer to resolve and add. 

A normal merge results in a parent having two commits. This type of merge, with or without `--no-commit`, results on a commit with two parents showing the merge path.

Sometimes it is not desired to have git commit changes on a merger. Especially when merging changes into the mainline branch as the development branch may have a bunch of noise in its commit history (it's a development branch after all). In such cases, `git --no-commit --squash <branch>` has git bring in and stage all changes in the index but not do the commit. This pause let's the developer re-group the files in the index and fully detail the merge as one commit rather than every commit that took place on the development branch. This type of commit only has a single parent, and its relation to the branch merged is not part of the commit history. Rather, it looks like a new commit on the master branch.

Both types of commits can be useful!

# Rebase (skip for time)

Rebase is useful when working on local branches. For example, if the master branch moves with changes, and the changes are need on the branch, then `git rebase master` removes all the commits on the branch, updates master, and then reapplies the commits on the branch. Reapplying may or may not cause conflicts which will need to be resolved as they occur in each commit. Conflict resolution is the same as in merging.

**Warning**: a rebase changes history making it so anyone tracking the branch can not do a fast-forward update. That is bad and generally discouraged. In general, only use rebase on branches that are not shared.

# Comparing Versions

Without an arguments `a` is the current commit and staged files (on the left) and the `b` is anything modified in the working directory. Can specify two commits if so desired. `git diff HEAD..mybranch` is the same as `git diff HEAD mybranch`. These are different than `git diff mybranch` as this later form includes anything staged in the diff and the former form does not. 

# Push to and Pull from Remotes

Not much no say on this topic. Keep it simple.

# Pull Requests 

The *GitHub* and *BitBucket* development model is called *feature branch*. A feature branch is a local branch that adds some new feature to the thing being worked on. Once that feature is done, and it is current with the mainline branch to which it belongs (e.g., it has merged in or rebased any new changes from the mainline), then it is ready to be reviewed for being added to the mainline branch. This process is done by pushing the branch to a new branch on the remote repository and then submitting a pull request to merge in the feature branch: `git push -u <remote-name> <branch>`. The pull request is created on `GitHub`. 

The clone command creates a named remote, `origin` that is the repository that was used for the clone. To push branch `new-button` to the repository used for the clone: `git push -u origin new-button`. 

Similarly, it may be desired to work on a remote branch. That branch needs to be checkout locally and configured to track the remote branch so that `push` and `pull` work as expected. `git branch -r` lists the remote branches. Checking out the branch of interest is easy: `git checkout -b <local-name-for-branch> --track <remote-name-for-branch>`. The `<remote-name-for-branch>` is usually `origin/somename` where `origin` is the repository used for the clone. 

It is possible to add other named remote repositories. `git remote` lists the known repositories---usually just `origin`. `git remote add` adds new remotes as in `git remote add <local-name> <url>`. Once a remote is added, it is possible to communicate with the remote by naming it in the commands. When no name is given, the default is `origin`. So `push` and `pull` default to `origin`, but it is equally possible to `git push <remote-name>` to interact with other remotes.