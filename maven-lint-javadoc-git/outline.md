# Objectives

Demonstrate the core tools for revision control, build management, documentation, lint-ing, and logging in the class. 

# Reading

  * [Git](https://git-scm.com/), [Git for Dummies](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/maven-lint-javadoc-git/Git-for-dummies.pptx), and [feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
  * [Git - Basic Branching and Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
  * [Git - Rebasing](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)
  * [Maven](https://maven.apache.org/)
  * [JavaDoc](https://en.wikipedia.org/wiki/Javadoc) and [Maven JavaDoc Plugin](https://maven.apache.org/plugins/maven-javadoc-plugin/)
  * [CheckStyle](https://checkstyle.sourceforge.io/) and [Maven CheckStyle Plugin](https://maven.apache.org/plugins/maven-checkstyle-plugin/)
  * [SLF4J](https://www.slf4j.org/), [Maven SLF4J repository](https://mvnrepository.com/artifact/org.slf4j), [log4j2](https://logging.apache.org/log4j/2.x/), and [Maven log4j2 repository](https://mvnrepository.com/artifact/org.apache.logging.log4j)---[log4j-slf4w-impl](https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-slf4j-impl) is what we really care about.

# Outline

## Source Control (Git)

Clone [hw0-tooling](https://github.com/byu-cs329/hw0-tooling). Create a branch. Draw on the board the commit tree where the branch is created and eventually merged. Show on the board synching back with master using both merge and rebase. Make clear how rebase changes history so an upstream repository should never use rebase on branches that are tracked downstream.

### Commands ###
  * `$> git clone https://github.com/byu-cs329/hw0-tooling` to clone repository
  * `$> git checkout -b new_feature` to create a new branch named `new_feature`
  * `$> git commit -a -m "...."` to add and commit changes to current branch with message `"...."`
  * `$> git checkout master` to switch back to `master` branch
  * `$> git merge new_feature` to merge `new_feature` into current branch
  * `$> git rebase master` (must be in other branch) to rebase current branch after master (then you need to switch back to `master` and merge)

## Build Life-cycle (Maven)

Generate a project using the [java8-minimal-quickstart](https://github.com/spilth/java8-minimal-quickstart):  `mvn archetype:generate -B -DarchetypeGroupId=org.spilth -DarchetypeArtifactId=java8-minimal-quickstart -DgroupId=edu.byu.cs329 -DartifactId=hw0-tooling-solution -Dversion=1.0.0`. The artifact ID is the directory in which the project is created. Open and discuss the `pom.xml` file. Show what is in the directory in general. Make the compiler plugin version 3.8.1 (will be needed later for Java Doc), and change the source to be 1.8.

```xml
        <properties>
                <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
                <maven.compiler.source>1.8</maven.compiler.source>
                <maven.compiler.target>1.8</maven.compiler.target>
        </properties>
```

Copy in the Dijkstra code for computing the shortest path. Use `mvn compile` to compile the code. Fix any errors and warnings (most likely the package declarations will be wrong). Show how to import the project into Eclipse as an existing Maven project. 

The hard way to run the code is by invoking java directly on the jar file after the `mvn package` command builds the jar file: `java -cp target/hw0-tooling-1.0-SNAPSHOT.jar edu.byu.cs329.hw0.DijkstraMain`.  What makes this hard is that the class path needs to be defined at the command line or in an environment variable:`export CLASSPATH=./target/hw0-tooling-1.0-SNAPSHOT.jar`. Managing the class path is a pain and best left to Maven.

Here are [three ways to run Java main from Maven](http://www.vineetmanohar.com/2009/11/3-ways-to-run-java-main-from-maven/). The easiest by far is to use `mvn exec:java` specifying the main method and arguments on the command line: `mvn exec:java -Dexec.mainClass="edu.byu.cs329.hw0.DijkstraMain"`. Now Maven handles the class path, and sets it to account for any dependencies (which matters a lot down the road). Add command line arguments with `-Dexec.args="arg0 arg1 arg2"`. Add the property to `pom.xml` for convenience: `<exec.mainClass>edu.byu.cs329.hw0.DijkstraMain</exec.mainClass>`.

## Docker Maven Containers

See [docker.md](../docker.md) for using a container for development either via the command line or connected with Visual Studio Code.

## Documentation (JavaDoc)

Add in the necessary hooks to `pom.xml` from a quick search on `Maven JavaDoc Plugin`. The usage section almost always has what is needed, and if not, then look for the dependency information section. Be sure to put the plugin outside the plugin management section in the build section and also remove the style sheet. Run `mvn site` and open the site in a browser. Navigate to the JavaDocs. Write the JavaDocs for a few classes after reviewing the [style guide](https://www.oracle.com/technetwork/java/javase/documentation/index-137868.html) and noting the minimum tags needed. 

As a note, the [Maven Site Plugin](https://maven.apache.org/plugins/maven-site-plugin/) needs to be in build section of `pom.xml` and the [Maven Project Info Reports Plugin](https://maven.apache.org/plugins/maven-project-info-reports-plugin/) needs to be in reporting section of the `pom.xml` to build the site. Missing the first will cause the `mvn site` command to fail and missing the second will case the `mvn site` command to warn about not setting the version. The [Maven Quickstart Archetype](https://maven.apache.org/archetypes/maven-archetype-quickstart/) includes these be default but the one used in this tutorial, [java8-minimal-quickstart](https://github.com/spilth/java8-minimal-quickstart), omits these plugins. Both of these need to be in the `plugins` with the report plugin optionally going instead in the reporting section.

JavaDoc will error, depending on the code, about not supporting modules. The issue is most often resolved by configuring JavaDoc for the correct version of Java being used.

```xml
<configuration>
  <source>8</source>
</configuration>
```

The documentation expectation for the course is as follows.

```java
/**
 * Returns an Image object that can then be painted on the screen. 
 * The url argument must specify an absolute {@link URL}. The name
 * argument is a specifier that is relative to the url argument. 
 * <p>
 * This method always returns immediately, whether or not the 
 * image exists. When this applet attempts to draw the image on
 * the screen, the data will be loaded. The graphics primitives 
 * that draw the image will incrementally paint on the screen. 
 *
 * @param  url  an absolute URL giving the base location of the image
 * @param  name the location of the image, relative to the url argument
 * @return      the image at the specified URL
 * @see         Image
 */
```

## Lint-ing (Checkstyle)

Add in the hooks to `pom.xml` for CheckStyle from a quick search on `Maven CheckStyle Plugin` and click on `Usage`. Configure it so that it checks for violations as part of the build. Change the checks to be `google_checks.xml`. Run `mvn clean compile` to show the warnings. Google has a two space indentation. Reformat the file accordingly. These must be outside of the plugin management section or the hook will not fire in the build. 

I use Eclipse to reformat. The preferences are in Java and then Code Style. Give it a new profile name such as `Google`. Make necessary changes until all the CheckStyle warnings are gone.

Note that Eclipse will flag an error in the `pom.xml` about the life cycle. This error has to do with how Eclipse integrates with Maven and does the life-cycle mapping for its build system. Re-import the project from scratch and Eclipse will install a CheckStyle plugin for its Maven connector that does the correct mapping.

## Logging (SLF4J)

Debugging with console output is an important tool. Logging makes it so the output can be turned on and off with ease. Learning to use an industry standard logging utility does matter.

Add in the SLF4J (Apache's logging facade) dependencies as per the [manual](https://www.slf4j.org/manual.html). Search for Maven. I am using the `log4j2` option that only requires one dependency entry in the `pom.xml` file. Eclipse will need to update the project to make it work in the Maven submenu. Use the `slf4` imports for the `Logger` and `LoggerFactory` in the code when adding in the logging to the Java files.

```xml
       <dependencies>
         <dependency> 
             <groupId>org.apache.logging.log4j</groupId> 
             <artifactId>log4j-slf4j-impl</artifactId> 
             <version>2.11.1</version> 
         </dependency>
     </dependencies>
```


Getting logging up quickly can be confusing, and the internet resources are not super to be honest. The manual is the best place to start. Add in the dependencies as described in the [manual](https://www.slf4j.org/manual.html). Create a [configuration file](https://www.mkyong.com/logging/log4j-xml-example/) for `log4j` and put it in the `src/main/resources` directory. Running the Java program should now include the logging either when invoked in Eclipse or from the command line with the `mvn exec:java` command (see above Maven section).

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final Logger logger = LoggerFactory.getLogger(Dijkstra.class);
```

Gets the logger for the particular class. From there use the `warn`, `info`, etc. methods.

Here is a configuration file that needs to be in the Java class path. I put it in the `src/main/resources`. The file should be named `log4j2.xml`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
   <Appenders>
     <Console name="Console" target="SYSTEM_OUT">
       <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
     </Console>
   </Appenders>
   <Loggers>
     <Root level="warn">
       <AppenderRef ref="Console"/>
     </Root>
   </Loggers>
</Configuration>
```

## Pull Request

Push the local branch to the remote: `git push -u origin feature_branch_name`

[Creating an issue or pull request](https://docs.github.com/en/desktop/contributing-and-collaborating-using-github-desktop/creating-an-issue-or-pull-request)