# Objectives

Implement constant propagation using constant folding and reaching definitions.

# Example Code

```java
  int a = -3;
  int b = 9 + (a + -3);
  int c;

  c = b + 9;
  if (c > 10) {
     c = c + (-10);
  }
  return c + (7 + a);
```

# Constant Propagation

For each method in the `CompilationUnit`:

  0. Apply constant folding
  1. Build the CFG
  2. Compute the reaching definitions
  3. For every variable use that has a single `NumberLiteral` definition or the same `NumberLiteral` definition from multiple places, replace the use with the `NumberLiteral`
  4. Repeat until no change in method

Work the example code on the board.
