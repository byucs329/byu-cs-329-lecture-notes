# Objectives

At the end of the lecture students should have a basic understanding of the visitor pattern and how it is used for computation. A student should be able to

  * Explain the role of the `accept` method
  * Explain the role of the `visit` method
  * Show the ways in which the `visit` method is able to change the traversal either through return value or calling accept directly on children.
  * Create visitors and visit methods that are able to do useful computation

# Reading

Wikipedia is a good starting point for learning the [visitor pattern](https://en.wikipedia.org/wiki/Visitor_pattern). This lecture ignores complex tree structures with different types for nodes. The key is understanding `accept` and `visit`. Polymorphism uses the same principles only with dynamic dispatch to resolve the type to the correct `visit` method.

# Outline

Pattern the visitor on the DOM visitor so the DOM is familiar to the students. Create visitors to compute

  1. `contains`: find a node in the tree (return true if present and false otherwise)
  2. `height`: compute the height of the tree
  3. `preorder`: compute a pre-order array
  4. `postorder`: compute a post-order array
  5. `inorder`: compute an in-order array
  6. `add`: Add a node (return true if not there already otherwise false)

The tests already exist. 

## Visitor Pattern

The `accept` method defines the traversal order. The `visit` method defines the computation. Typically the node defines the traversal order (the `accept` method) and the visitor is defined in a separate visitor class. As such the `accept` method takes as input the visitor class that will operate on each node as it is visited.

Define the NodeVisitor default implementation:

  * `visit(Node)` called when entering the node and before any children are visited. If it returns false, then the traversal does not visit children. The default implementation returns `true`.
  * `endVisit(Node)` called after the children are visited, and it is called immediately if `visit` returns false. The default implementation does nothing.

```java
package edu.byu.cs329.visitorpattern;

public class NodeVisitor {
  public boolean visit(Node n) {
    return true;
  }
  
  public void endVisit(Node n) {
  }
}
```

Create the `accept` method (in Node) to call the visit method as appropriate.

```java
 public void accept(NodeVisitor v) {
    boolean doTraverseChildren = v.visit(this);
    
    if (doTraverseChildren) {
      if (left != null) {
        left.accept(v);
      }
      if (right != null) {
        right.accept(v);
      }
    }
    
    v.endVisit(this);
  }
```

## Contains

Note that this implementation does *not* take advantage of the property that the structure is a binary tree as it checks every node.

```java 
public class ContainsVisitor extends NodeVisitor {
  boolean isFound = false;
  int target;
  
  public ContainsVisitor(int target) {
    this.target = target;
  }

  @Override
  public boolean visit(Node n) {
    if (n.value == target) { 
      isFound = true;
      return false;
    }    
    return true;
  }
}
```

## Height

```java
public class HeightVisitor extends NodeVisitor {

  int height = 0;
  int currentLevel = 0;
  
  @Override
  public boolean visit(Node n) {
    currentLevel += 1;
    return true;
  }
  
  @Override
  public void endVisit(Node n) {
    if (currentLevel > height) {
      height = currentLevel;
    }
    currentLevel -= 1;
  }
   
}
```

## Pre-order Traversal

```java
public class PreorderVisitor extends NodeVisitor {
  List<Integer> orderedList = null;

  public PreorderVisitor() {
    orderedList = new ArrayList<Integer>();
  }

  @Override
  public boolean visit(Node n) {
    orderedList.add(n.value);
    return true;
  }
}
```

## Post-order Traversal

```java
public class PostorderVisitor extends NodeVisitor {
  List<Integer> orderedList = null;

  public PostorderVisitor() {
    orderedList = new ArrayList<Integer>();
  }

  @Override
  public void endVisit(Node n) {
    orderedList.add(n.value);
  }
}

```

## In-order Traversal
```java
public class InorderVisitor extends NodeVisitor {
  List<Integer> orderedList = null;
  
  public InorderVisitor() {
    orderedList = new ArrayList<Integer>();
  }

  @Override
  public boolean visit(Node n) {
    if (n.left != null) {
      n.left.accept(this);
    }
    
    orderedList.add(n.value);
    
    if (n.right != null) {
      n.right.accept(this);
    }
    
    return false;
  }
```

## Add

The idea of the visitor is to find the node to which the added value should happen, and then add it correctly in the `add` method when the visitor finishes.

```java
public class ImmediateRootVisitor extends NodeVisitor {
  Node immediateRoot = null;
  
  protected int target = 0;
  
  public ImmediateRootVisitor(int target) {
    this.target = target;
  }

  @Override
  public boolean visit(Node n) {
    if (immediateRoot != null) {
      return false;
    }
 
    if (n.value == target) {
      immediateRoot = n;
    } else if (target < n.value && n.left != null) {
      n.left.accept(this);
    } else if (target > n.value && n.right != null) {
      n.right.accept(this);
    }
    
    return false;
  }

  @Override
  public void endVisit(Node n) {
    if (immediateRoot != null) {
      return;
    }
    
    immediateRoot = n;
  }
}
```

# Old Notes (READ BELOW AT YOUR OWN RISK)

The lecture on visitors takes two class periods.

# Lecture 1: Demos

This class focused on a demo using the [BST implementation](https://bitbucket.org/byucs329/binarysearchtree/src/for-class) and another using the NoAllCapsVisitor class in the [Visitor Skeleton](https://bitbucket.org/byucs329/visitor-skeleton/src/master/).

## First demo: BST

The first demo begins with a description of what the visitor pattern is intended to do. Draw the tree generated in `BinarySearchTreeDriver` on the board. After giving the intuition behind the goal, live code an `accept` method for the tree that passes through to an `accept()` method invoked on its root. The Node's `accept` method is the workhorse. A simple `visit()` method is already implemented. Be sure to call the tree's `accept` method from the `BinarySearchTreeDriver` `main()` function.

Take a moment to discuss the fact that this is a pre-order traversal. By ignoring the `visit()` method's return value, it is possible to perform in-order and post-order traversals, as well. Demonstrate these traversals on the board and in code.

Additionally, demonstrate what happens when `visit()` returns false; for example, change the literal `true` to `n.v != 7`. This causes `accept()` to skip 6 and 8.

## Second demo: Linter property

The second demo uses the visitor-skeleton code. It starts with tests already written and with a visitor plugged into the main function (the repository has an implementation; if you want to live code it, strip out the implementation from `NoAllCapsVisitor` before starting class). The visitor is intended to check for identifiers that are in all caps.

Demonstrate that the tests fail and walk through what they do.

Add the boiler plate, such as a constructor and a private data member to track the identifiers it finds that are capitalized. Add a `visit()` method that takes a `SimpleName`, gets its `toString()`, and compares that string against its own `str.toUpperCase()`. Demonstrate that it works. Point out that this is not brilliant code; rather you didn't have to do anything brilliant because the visitor pattern made it easy.

## Wrap-up

Lastly, introduce the linter assignment.

# Lecture 2: Participatory exploration

Implement some of the below properties in the *visitor-skeleton* project. The **bolded** ones are a good place to start.

## Code Smells
  1. Zeros should not be a possible denominator
  2. **Wildcard imports should not be used**
  3. **Variables should not be self-assigned**
  4. Values should not be uselessly incremented (assignment in a post-increment to self or returning a post-inc only)
  5. Utility classes should not have public constructors (any static class should have no public constructor)
  6. **Unary prefix operators should not be repeated (!, ~, -, and +)**
  7. **Thread.run() should not be called directly**
  8. The default unnamed package should not be used
  9. The Object.finalize() method should not be overridden
  10. The Object.finalize() method should not be called
  11. **Ternary operators should not be nested**
