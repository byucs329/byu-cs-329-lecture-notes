package edu.byu.cs329.visitorpattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BinarySearchTreeTest {

  public static BinarySearchTree bstForTest = null;
  final Logger log = LoggerFactory.getLogger(BinarySearchTreeTest.class);
  
  @BeforeEach 
  public void beforeEach() {
    Node rootLeftChild = new Node(1);
    Node leaf4 = new Node(4);
    Node leaf7 = new Node(7);
    Node rootRightChild = new Node(6, leaf4, leaf7);
    Node root = new Node(3, rootLeftChild, rootRightChild);
    bstForTest = new BinarySearchTree(root);
  }

  @Test
  @Tag("DEMO")
  @DisplayName("Should return true when checking if tree contains 4")
  void should_returnTrue_when_contains_4_called() {
    Assertions.assertTrue(bstForTest.contains(4));
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return false when checking if tree contains 2")
  void should_returnFalse_when_contains_2_called() {
    Assertions.assertFalse(bstForTest.contains(2));
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return 3 when height is called")
  void should_return3_when_height_called() {
    Assertions.assertEquals(3, bstForTest.height());
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return {3, 1, 6, 4, 7} when preorder is called")
  void should_return3_1_6_4_7_when_preorder_called() {
    int expected[] = {3,1,6,4,7};
    int[] actual = bstForTest.preorder().stream().mapToInt(Integer::intValue).toArray();
    Assertions.assertArrayEquals(expected, actual);
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return {1, 4, 7, 6, 3} when postorder is called")
  void should_return1_4_7_6_3_when_postorder_called() {
    int expected[] = {1,4,7,6,3};
    int[] actual = bstForTest.postorder().stream().mapToInt(Integer::intValue).toArray();
    Assertions.assertArrayEquals(expected, actual);
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return {1, 3, 4, 6, 7} when inorder is called")
  void should_return1_3_4_6_7_when_inorder_called() {
    int[] expected = {1,3,4,6,7};
    int[] actual = bstForTest.inorder().stream().mapToInt(Integer::intValue).toArray();
    Assertions.assertArrayEquals(expected, actual);
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return true when adding 2")
  void should_returnTrue_when_adding_2() {
    Assertions.assertTrue(bstForTest.add(2));
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return true when adding -1")
  void should_returnTrue_when_adding_Neg1() {
    Assertions.assertTrue(bstForTest.add(-1));
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return true when adding 8")
  void should_returnTrue_when_adding_8() {
    Assertions.assertTrue(bstForTest.add(8));
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return False when adding 3")
  void should_returnFalse_when_adding_3() {
    Assertions.assertFalse(bstForTest.add(3));
  }
  
  @Test
  @Tag("DEMO")
  @DisplayName("Should return False when adding 4")
  void should_returnFalse_when_adding_4() {
    Assertions.assertFalse(bstForTest.add(4));
  }
}
