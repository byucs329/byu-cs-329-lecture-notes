package edu.byu.cs329.visitorpattern;

import java.util.List;
import java.util.Objects;

public class BinarySearchTree {

  protected Node root;

  public BinarySearchTree(Node root) {
    Objects.requireNonNull(root);
    this.root = root;
  }

  public boolean contains(int value) {
    // TODO
    return false;
  }
  
  public int height() {
    // TODO
    return 0;
  }
  
  public List<Integer> inorder() {
    // TODO
    return null;
  }
  
  public List<Integer> preorder() {
    // TODO
    return null;
  }
  
  public List<Integer> postorder() {
    // TODO
    return null;
  }
  
  public boolean add(int value) {
    // TODO
    return false;
  }
}
