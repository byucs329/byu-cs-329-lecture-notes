package edu.byu.cs329.visitorpattern;

class Node {
  
  public final int value;
  Node left;
  Node right;
  
  public Node(int value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }

  public Node(int value, Node left, Node right) {
    this.value = value;
    this.left = left;
    this.right = right;
  }

}