Two articles that motivate much of the content for this course:

  * [Scrutinizing SpaceX, NASA Overlooked Some Boeing Software Problems](https://www.nytimes.com/2020/07/07/science/boeing-starliner-nasa.html)
  * [NASA and Boeing Complete Orbital Flight Test Reviews](https://www.nasa.gov/feature/nasa-and-boeing-complete-orbital-flight-test-reviews)

The general sequential flow of lectures and notes for the semester.
 
  1. Introduction - [Intro.pptx](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/Intro.pptx)
  2. Tools - [maven-lint-javadoc-git/outline.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/maven-lint-javadoc-git/outline.md)
  3. JUnit Jupiter and coverage - [junit5-lecture.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/junit5-lecture.md) 
  4. Specification - [specification-blackbox/specification.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/specification-blackbox/specification.md)
  55 Black-box Test - [specification/blackbox/blackbox.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/specification-blackbox/blackbox.md)
  4. Visitor Pattern - [visitor-pattern/README.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/visitor-pattern/README.md)
  5. DOM Visitor and Constant Folding - [DOM-Visitor/README.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/DOM-Visitor/README.md)
  6. Control Flow Graphs - [cfg-rd-lecture.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/cfg-rd-lecture.md) and [compilers/11-static-analysis-cfg.ppt](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/compilers/)
  7. Reaching Definitions - [cfg-rd-lecture.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/cfg-rd-lecture.md) and [compilers/12-static-analysis-reaching-definition.pptx](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/compilers/), [compilers/13-static-analysis-classic-problems.ppt](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/compilers/)
  8. Constant propagation - [constant-propagation.md](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/constant-propagation.md)
  10. Mocking with Mockito - [max-blocks-mockito/mockito.md](max-blocks-mockito/mockito.md)
  11. White box testing - [whitebox.md](whitebox.md)
  12. Property based testing - stateless [property-based-testing/pbt-stateless.md](property-based-testing/pbt-stateless.md)
  13. Property based testing - stateful [property-based-testing/pbt-stateful.md](property-based-testing/pbt-stateful.md)
  14. Static Type Checking - [type-checking.md](type-checking.md) (see the lab3 write up too with reference to my local ropository)
  15. Program Dependence graph - [pdg.md](pdg.md) and [program-dependence-graph.ppt](program-dependence-graph.ppt)
  16. Git Bisect - [git-bisect.md](git-bisect.md)
  17. Mutation Analysis - [pit-mutation-analysis/mutation-analysis.md](pit-mutation-analysis/mutation-analysis.md)
  18. Git Hooks - [git-hooks-CI/git-hooks.md](git-hooks.md)
  19. Continuous Integration - [git-hooks-CI/continuous-integration.md](git-hooks-CI/continuous-integration.md)
  20. Liskhov principle of substitution [liskov.md](liskov.md)
  21. Dafny - [dafny/dafny.md](dafny/dafny-intro.md)
  22. Weakest Precondition Calculus - [weakest-precondition.md](weakest-precondition.md) and [wp-examples.pdf](wp-examples.pdf)
  23. Weakest Precondition Calculus for Loops and Termination - [wp-for-loops.md](wp-for-loops.md) and [wp-loops-examples.pdf](wp-loops-examples.pdf)
  24. SPIN Model Checking - [spin/spin.md](spin/spin.md)
  