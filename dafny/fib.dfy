function fib(n : nat) : nat
{ if n <= 1 then n else fib(n-1) + fib(n - 2) }

method fibFast(n : nat) returns (c : nat)
requires n >= 1
ensures c == fib(n)
{ 
  var p := 0; // 1
  c  := 1; // 2
  var i := 1; // 3
  while i < n 
  invariant 1 <= i <= n
  invariant p == fib(i - 1) && c == fib(i)
  decreases (n - i)
  { var t := p + c; // 5
    p := c; // 6
    c := t; // 7
    i := i + 1; // 8
  } // 4
}
