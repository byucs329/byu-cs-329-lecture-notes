datatype Student = Student(firstName : string, lastname : string, number : int, graduated : bool)

function hashCode(a : Student) : int {
    a.number % 5 + if a.graduated then 5 else 15
}

function equals(a : Student, b : Student) : bool {
    a.number == b.number && a.firstName == b.firstName && a.graduated == b.graduated
}

method Main() {
    assert forall x, y : Student :: equals(x,y) ==> hashCode(x) == hashCode(y);
    assert forall x, y : Student :: hashCode(x) == hashCode(y) ==> equals(x,y); 
}