class HashTable {
    
      // Open addressing Hashtable with linear probing as collision resolution.
    
      var hashtable:array?<int>;
      var capacity:int;
      var size:int;	
	
      predicate Valid()
      {
       hashtable != null && 
       0 <= size <= capacity && 
       capacity == hashtable.Length &&
       capacity > 0  
      }

      method Init(c:int)
      {
        hashtable := new int[c]; 
        capacity := c;
        size := 0;
        
        forall (i | 0 <= i < hashtable.Length) {
           hashtable[i] := 0;
        }
      }
      	
     function method hash_function(key:int):(int)
     {
       var result:int := 0;
        
       if (key >= 0)
       then key % capacity
       else (key * -1) % capacity
      }

      method Add(val:int, key:int)
      {
        var i:int := hash_function(key);
           if (hashtable[i] != 0) {
           var j:int := 0;
  
           while (hashtable[i] != 0 && j < capacity)
           {
             if (i == capacity-1) 
                {i := 0;}
             else {i := i + 1;}
             
             j := j + 1;
 	       }
        }
        hashtable[i] := val;
        size := size + 1;   
      }
}