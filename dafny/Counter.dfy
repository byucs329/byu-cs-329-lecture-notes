// Specify the counter so that the assertion is proved
class Counter {

  /*
   * Should always be greater than or equal to zero
   */
  var value : int ;
  
  constructor init()
  {
    value := 0 ;
  }
  
  /* 
   * the value of the counter
   */
  method getValue() returns (x:int)
  {
    x := this.value ;
  }
  
  /*
   * increment the counter
   */
  method inc()
  {
    value := value + 1;
  }
  
  /* 
   * decrement the counter
   */
  method dec()
  { 
    if (value > 0) {
      value := value - 1;
    }
  }
}

/*
* Test method
*/
method Main ()
{
  var count := new Counter.init();
  count.dec();
  count.inc();
  count.inc();
  count.dec();
  count.inc();
  var aux : int := count.getValue();
  assert (aux == 2);
}