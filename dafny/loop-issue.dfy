method magic() returns()
requires true;
ensures  1 == 0; {
  while (1 != 0)   
  invariant true;
  { }
}
