method simpleInvariant(n : int) returns (m : int)
requires n >= 0
ensures  n == m {
  m := 0; // 1
  while m < n 
  decreases n-m;
  invariant m <= n
  { 
    m := m + 1; // 3 
  } // 2
}
