/*
 * Sets a[2] to 42
 * 
 * @param a is an array of positive integers
 * @return nothing
 */
method example(a : array?<int>)
{
  a[2] := 42;
}
