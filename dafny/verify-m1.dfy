method M (x0 : int) returns (x : int)
ensures (x0 < 3 ==> x == 1) && (x0 >= 3 ==> x < x0);
{
  x := x0 - 3;
  if (x < 0) {
    x := 1;
  } else {
    if (true) {
      x := x + 1;
    } else {
      x := 10;
    }
  }
}