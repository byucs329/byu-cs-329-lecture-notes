/*
 * Sets a[2] to 42
 * 
 * @param a is an array of positive integers
 * @return nothing
 */
method example(a : array<int>)
modifies a
requires a.Length >= 3
ensures a[2] == 42
ensures forall i : int :: (0 <= i < a.Length && i != 2) ==> a[i] == old(a[i])
{
  a[2] := 42;
}

method testArray(a : array<int>)
modifies a
requires a.Length >= 3
{
  example(a);
  assert(a[2] == 42);
  assert(forall i : int :: (0 <= i < a.Length && i != 2) ==> (a[i] == old(a[i])));
}

function absFunc(x : int) : int {
  if (x < 0) then -x else x  // x<0? -x :
}

method abs(x : int) returns (r : int) 
  ensures r == absFunc(x)
{
  r := x;
  if (x < 0) {
    r := -x;
  }
} 

method testAbs(y : int) {
  var absOfy := abs(y);
  assert (absOfy >= 0);
  var z := abs(-3);
  assert (z == 3);
  z := abs(3);
  assert (z == 3);
}

function max(a: int, b: int) : int {
  if (a > b) then a else b
}

// method testingMax() {
//   assert (forall a,b : int :: a >= b ==> max(a, b) == a);
//   assert (forall a,b : int :: a <= b ==> max(a, b) == b);
// }

lemma maxProperty(a : int, b : int) 
ensures max(a, b) >= a && max(a, b) >= b
ensures max(a, b) == a || max(a, b) == b
{
// Add more steps to hepl the proof
}
