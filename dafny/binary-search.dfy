module Search {
  ghost predicate isSorted(sqnc: seq<int>) {
    (forall i, j | 0 <= i <= j < |sqnc| :: sqnc[i] <= sqnc[j])
  }

  ghost predicate isValid(
    value: int,
    sqnc: seq<int>,
    low: int,
    high: int
  ) {
    && isSorted(sqnc)
    && 0 <= low <= high < |sqnc|
    && value !in sqnc[..low]
    && (value !in sqnc [high..] || (forall i | high <= i < |sqnc| - 1 :: value <= sqnc[i]))
  }

  ghost opaque function binarySearchSpec(
    value: int,
    sqnc: seq<int>,
    low: int,
    high: int
  ) : (i: int)
    requires isValid(value, sqnc, low, high)
    decreases high - low
  {
    if (value < sqnc[low] || value > sqnc[high]) then
      |sqnc|
    else
      if (low == high) then
        if (sqnc[low] == value) then
          low
        else
          |sqnc|
      else
        var mid := (low + high) / 2;
        if (value > sqnc[mid]) then
          binarySearchSpec(value, sqnc, mid + 1, high)
        else
          binarySearchSpec(value, sqnc, low, mid)
  }

  lemma binarySearchSpec_indexOfValue_iff_valueInSqnc(
    value: int,
    sqnc: seq<int>,
    low: int,
    high: int,
    i : int
  )
    requires
      && isValid(value, sqnc, low, high)
      && i == binarySearchSpec(value, sqnc, low, high)
    ensures
      if (value !in sqnc) then
        i == |sqnc|
      else
        && low <= i <= high
        && sqnc[i] == value
        && (forall j | 0 <= j < i :: sqnc[j] != value)
    decreases high - low
  {
    reveal binarySearchSpec();
    if (value < sqnc[low] || value > sqnc[high]) {
    } else {
      if (low == high) {
        if (sqnc[low] == value) {
        } else {
        }
      } else {
        var mid := (low + high) / 2;
        if (value > sqnc[mid]) {
          binarySearchSpec_indexOfValue_iff_valueInSqnc(value, sqnc, mid + 1, high, i);
        } else {
          binarySearchSpec_indexOfValue_iff_valueInSqnc(value, sqnc, low, mid, i);
        }
      }
    }
  }

  method binarySearch(
    value: int,
    sqnc: seq<int>
  ) returns (i: int)
    requires isValid(value, sqnc, 0, |sqnc| - 1)
    ensures
      && var spec := binarySearchSpec(value, sqnc, 0, |sqnc| - 1);
      && i == spec
  {
    var low := 0;
    var high := |sqnc| - 1;

    ghost var spec := binarySearchSpec(value, sqnc, low, high);
    binarySearchSpec_indexOfValue_iff_valueInSqnc(value, sqnc, low, high, spec);

    while(low != high)
      invariant
        && isValid(value, sqnc, low, high)
    {
      if (value < sqnc[low] || value > sqnc[high]) {
        return |sqnc|;
      }

      var mid := (low + high) / 2;
      if (value > sqnc[mid]) {
        low := mid + 1;
      } else {
        high := mid;
      }
    }

    i := if (value == sqnc[low]) then low else |sqnc|;
  }

  method {:test} given_valueInSqnc_when_binarySearch_then_returnIndex() {
    // given
    reveal binarySearchSpec();
    var sqnc := [1, 2, 3, 4, 4, 5];
    var value := 4;

    // when
    var i := binarySearch(value, sqnc);

    // then
    reveal binarySearchSpec();
    assert(i == 3);
    expect (i == 3), "ERROR: expected i == 3";
  }

  method {:test} given_valueNotInSqnc_when_binarySearch_then_returnSizeSqnc() {
    // given
    reveal binarySearchSpec();
    var sqnc := [1, 2, 2, 4, 4, 5];
    var value := 7;

    // when
    var i := binarySearch(value, sqnc);

    // then
    assert(i == |sqnc|);
    expect (i == |sqnc|), "ERROR: expected i == |sqnc|";
  }
}