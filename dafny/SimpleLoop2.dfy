class SimpleLoop2 {

  var x:int;
    
  method loop() returns (ret:int)

  {
    var y:int := x;
    var z:int := 0;

    while (y > 0) 

    {
       z := z + x;
       y := y - 1;
    }
 
    return z;
  }
}