method M3(x : int, y : int) returns (q : int, r : int)
requires x >= 0 && y > 0;
ensures q * y + r == x && r >= 0 && r < y; 
{
  q := 0;
  r := x;
  while (r >= y) 
  invariant q * y + r == x && r >= 0;
  decreases r;
  {
    r := r - y;
    q := q + 1;
  }
}

/*
Proof: 
Let Q = x >= 0 && y > 0 
Let R = q * y + r == x && r >= 0 && r < y && y > 0
Folowing the 5 step cheklist for proving loops correct from the lecture notes.

(1) Show that the invariant holds prior to entering the loop. This amount to showing

Q ==> wp(q:=0; r:=x, I)

By Seq rule followed by Assignment rule we get
x >= 0 && y > 0 ==> 0 * y + x == x && x >= 0 && y > 0

Simplify
x >= 0 && y > 0 ==> 0+x==x && x >= 0 && y > 0

True
(2) Show that the invariant is preserved by the loop. This amount to showing
I && B ==> wp(r := r - y; q := q + 1, I)

By Seq rule followed by Assignment rule
I && (r >=y) ==> q+1 * y + r-y == x && r-y >= 0 && y > 0

Simplify
q * y + r == x && r >= 0 && r >= 0 && y > 0 && r >=y ==>
       q+1 * y + r-y == x && r-y >= 0 && y > 0

Simplify
q * y + r == x && r >= 0 && r >= 0 && y > 0 && r >=y ==>
       (q*y + y) + r-y == x && r-y >= 0 && y > 0

First conjunct, (q*y + y) + r-y == x is identical to the first premise. The second conjunct, r-y >= 0, follows
from premises r >=0 && r >=y, and y > 0 follows trivially from the
corresponding premise. 
(3) Show that the postcondition hold after the loop exits. This amount to showing
 
I && !(r >=y) ==> R

q * y + r == x && r >= 0 && y > 0 && r < y ==>
q * y + r == x && r >= 0 && y > 0 && r < y

Which follows directly from the invariant and negated loop guard.
(4) Show that the variant is bounded from below by 0, as long as the loop has not terminated.
 
I && (r >=y) ==> r > 0

x >= 0 && y > 0 && q * y + r == x && r >= 0 && y > 0 && r >= y ==> r > 0

Which follows directly from the conjunct y > 0 and loop guard r >= y.
(5) Show that the variant decrease on each loop iteration. This amounts to proving that:
 
I && (r >=y) ==> wp(V1:= V; r := r - y; q := q + 1, V < V1)

By Sequential and Assignment rules, we obtain

I && (r >=y) ==> wp(V1 := r, r-y < V1)

By Assignment rule
I && (r >=y) ==> (r-y < r)

This follows from the loop guard: r >= y and conjunct y > 0.
This concludes the proof.
*/