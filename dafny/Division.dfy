/*
The following method Division divides x by y. q contains the quotient afterwards and r the remainder. Find a loop invariant and a specification which verify the previous implementation. 
*/
method Division(x : int, y : int) returns (q : int, r : int)

{
  q := 0;
  r := x;
  while (r >= y) 
  {
    r := r - y;
    q := q + 1;
  }
}