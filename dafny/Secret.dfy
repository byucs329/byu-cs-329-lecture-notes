/*
 * Your task is to complete the class Secret.dfy, and write specifications for
 * its methods. Implement and write formal specifications for the methods Init
 * and Guess. Check that your implementation and specification agree.
 *
 *  (A) The Init method initialises a new object of type Secret, by setting the
 *       secret to the value x. The value of x is required to be between 1 and 10.
 *       After initialisation, the field secret must hold the secret number, the
 *       number of guesses is 0 and the secret is not known.
 *
 *       method Init(x: int)
 *
 *  (B) The Guess method takes in a guess and returns two values, a boolean result
 *      which is true if the guess was correct and false otherwise, and an integer
 *      guesses telling how many guesses has been made so far. The precondiditon of
 *      the method is that the secret is still unknown. Each time the user make a
 *      guess, the count should be incremented. If the user guesses correctly, the
 *      secret becomes known, otherwise it remains unknown.
 *
 *      method guess(g : int) returns (result : bool, guesses : int)
 *
 *  (C) Complete the Main method with some tests: Create an object, make some guesses
 *      and assert some properties about the expected results. Compile and check
 *      your code.
 */
class Secret{
  var secret : int; // A secret number between 1...10
  var known : bool; // Has the secret been guessed yet?
  var count : int;  // How many guesses have been made?

  constructor Init(x: int)

  method Guess(g : int) returns (result : bool, guesses : int)
}

method {:test} test() {
  var s := new Secret.Init(5);

  var result : bool := false;
  var guesses : int := 0;

  result, guesses := s.Guess(6);
  assert
    && result == false
    && guesses == 1;
  expect
    && result == false
    && guesses == 1,
    "ERROR: expected result == false && guesses == 1";

  result, guesses := s.Guess(5);
  assert
    && result == true
    && guesses == 2;
  expect
    && result == true
    && guesses == 2,
    "ERROR: expected result == true && guesses == 2";
}