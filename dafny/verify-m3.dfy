// Prove partial correctness
method M3(x : int, y : int) returns (q : int, r : int)
requires x >= 0 && y > 0;
ensures q * y + r == x && r >= 0 && r < y; 
{
  q := 0; // 1
  r := x; // 2
  while (r >= y) 
  decreases r
  invariant q * y + r == x && r >= y
  {
    r := r - y; // 4
    q := q + 1; // 5
  } // 3
}
