# Objective

# Reading

 * [FormalSpecification1.pptx](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/chalmers-slides/FormalSpecification1.pptx)
 * [FormalSpecification2.pdf](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/chalmers-slides/FormalSpecification2.pdf)
 * [FormalSpecification3.pdf](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/chalmers-slides/FormalSpecification3.pdf)
 * [Dafny Guide](https://rise4fun.com/dafny/tutorialcontent/guide)
 * [Dafny Reference Manual](https://github.com/Microsoft/dafny/blob/master/Docs/DafnyRef/out/DafnyRef.pdf)
 * [Dafny Tutorial](https://rise4fun.com/Dafny/tutorial)
 * [Another Dafny Reference](https://homepage.cs.uiowa.edu/~tinelli/classes/181/Papers/dafny-reference.pdf)
 * [Parser](http://homepage.cs.uiowa.edu/~tinelli/classes/181/Papers/dafny-reference.pdf)

# Overview (quick)

Make clear the distinction between test and verification with a few examples.

The code is in `StudentHashCode.dfy`.

```csharp
datatype Student = Student(firstName : string, lastName : string, number : int, graduated : bool)

function hashCode(a : Student) : int
{
  a.number % 5 + if a.graduated then 15 else 31
}

function equals(a : Student, b : Student) : bool
{
  a.number == b.number && a.firstName == b.firstName
}

method Main() {
  assert forall x, y : Student :: equals(x,y) ==> hashCode(x) == hashCode(y);
}
```

Testing would need to generate inputs to see if the assertion fails. Verification is able to prove no such input exist. Run the above in [Dafny](https://rise4fun.com/Dafny/). Remove the addition on the `graduated` field.

What can Dafny do?

   * Prove that formal specifications are never violated --- that the require clauses with the implementation imply the guarantee (i.e, the code implements the contract)
   * Prove the absence of runtime errors:

      * Non-termination
      * Array index out-of-bounds
      * Null dereferences

Another example is the Zune leap-year bug (December 31, 2008, all Zunes froze). The issue related to how it computed the number of days since 1980. The code looped at the turn of the new year locking up all the devices. The official fix was to drain the device battery and then recharge after midday GMT on 1 January 2009.

The code is in `Zune.dfy`. The code calculates a year from the base year of 1980 given some number of days. The issue is that it fails to decrease `days` on certain conditions causing an infinite loop.

```csharp
predicate isLeapYear(y: int) {
  y % 4 == 0 && (y % 100 != 0 || y % 400 == 0)
}

// Does this method terminate?
method WhichYear_InfiniteLoop(d: int) returns (year: int) {
  var days := d;
  year := 1980;
  while days > 365 {
    if isLeapYear(year) {
      if days > 366 {
        days := days - 366;
        year := year + 1;
      }
    } else {
      days := days - 365;
      year := year + 1;
    }
  }
}
```

Dafny is not able to prove termination because it is possible with `d = 1256` to have `days = 366` and `year = 1984`. With this input, the `days` does not decrease leading to an infinite loop.

Write code that does not fail. Add the contract on `computeDaysInYear` and the `decreases` incrementally with `decreases` first. Look at the counter example that has `daysInYear` be unbounded. Then write the contract for `computeDaysInYear`. The incremental process is perfect for seeing how Dafny knows nothing about method calls without a contract. In the absence of a contract, the method can return **anything**.

```csharp
method computeDaysInYear(y: int) returns (days: int)
requires y >= 1980
ensures days >= 365 && days <= 366 {
  if (isLeapYear(y)) {
    days := 366;
  } else {
    days := 365;
  }
}

// Does this method terminate?
method WhichYear_good(d: int) returns (year: int) {
  var days := d;
  year := 1980;
  var daysInYear := computeDaysInYear(year);
  while (days > daysInYear)
  decreases days - daysInYear {
    days := days - daysInYear;
    year := year + 1;
    daysInYear := computeDaysInYear(year);
  }
}
```

Note the addition of the contract on `computeDaysInYear`. That is needed for the `decreases` clause for termination.

# Basic Dafny

Dafny is a hierarchical tool. It uses contracts to prove assertions. What that means is that a method gets a contract that is a declarative statement of what the method is intended to do. Dafny then proves that the implementation of the method, the definition of how the computation is done, satisfies the contract. If there is a method call in that implementation, then Dafny uses the contract for the call to prove the implementation.

Effectively there are two, somewhat independent, statements of the intended computation. The first is the declarative contract: what is being computed. The second is the implementation of the contract: how it is  being computed. Dafny uses these two things to prove assertions in the program. The key is that method calls are replaced with contracts.

## Methods

I have put the next several examples in `DafnyExercises.dfy`.

Start with the method and its body and let Dafny fail. Then add the *requires* and *ensures* for the specification. Have Dafny prove the specification. Note that the `?` indicates `a` may be `null`. If the `?` is removed, then `a` is guaranteed to be non-null.

```csharp
/*
 * Sets a[2] to 42
 *
 * @param a is an array of positive integers
 * @return nothing
 */
method example(a : array?<int>)
modifies a
requires a != null && a.Length >= 3
requires forall i : int :: (i >= 0 && i < a.Length) ==> a[i] > 0
ensures  forall i : int :: (0 <= i < a.Length && i != 2) ==> a[i] == old(a[i])
ensures  a[2] == 42
{
  a[2] := 42;
}
```

Write a method to return the absolute value of an integer. And prove it works with Dafny.

```csharp
/*
 * absolute value of x
 *
 * @param x is an integer
 * @return abs(x)
 */
method abs(x : int) returns (r : int)
ensures r >= 0;
{
  if (x < 0) {
    r := -x;
  } else {
    r := x;
  }
}
```

Add another method that uses *Abs* and asserts that the return is positive.

```csharp
method test(x : int) {
  var absOfx := abs(x);
  assert (absOfx >= 0);
}
```

**Note**: assertions are proved at compile time and not runtime. It uses the **requires** and **ensures** statements for the proof (modular).

Change the test method to prove the return value is 3 and try to verify. What happened?

```csharp
method test(x : int) {
  var absOfx := abs(x);
  assert (absOfx >= 0);
  var y := abs(-3);
  assert (y == 3);
}
```

Dafny only uses the **requires** and **ensures** clauses for the proof, and these say nothing about the actual value of the return. They only claim it is positive. Strengthen the specification so that Dafny is able to prove the value of the return and not just that it is positive.

```csharp
ensures x < 0 ==> r == -x;
ensures x >= 0 ==> r == x;
```

Note: add one ensures at a time each precipitated by an assertion that fails. Here, `abs(-3)` works but not `abs(3)` because nothing is said about positive numbers. Every case must be accounted for, and in this sense, Dafny is not all that different than testing.

  * Write the code
  * Write a declarative version of the same code for the contract

If it is not stated in the contract, then in is can be anything. And it turns out that writing declarative contracts is no easier than writing the code. So why write everything twice? Because each uses a different language, and there is greater assurance when you state the same thing in two different languages and Dafny proves that they are equivalent. That is the key, Dafny proves that both say the same thing, and so you have greater assurance that it reflects what you want.

## Functions

Cannot modify anything, consist of only one expression of the correct type, and can only be used in a specification. Functions are not part of the compiled program, they exist to help verify the code, so they can only appear in assertions (these are not runtime checks and only exist at compile time for the proofs).

Replace the specification with a function for *abs*.

```csharp
function abs(x : int) : int {
  if (x < 0) then -x else x
}

/*
 * absolute value of x
 *
 * @param x is an integer
 * @return abs(x)
 */
method Abs(x : int) returns (r : int)
ensures r >= 0;
ensures r == abs(x)
{
  if (x < 0) {
    r := -x;
  } else {
    r := x;
  }
}

method Test() {
  var v := Abs(3);
  assert v == 3;
}
```

Write a **max** function with a test method that proves it works for all input *a* and *b*.

```csharp
function max(a: int, b: int): int {
   if (a > b) then a else b
}

method Testing() {
  assert (forall a,b : int :: a >= b ==> max(a,b) == a);
  assert (forall a,b : int :: a <= b ==> max(a,b) == b);
}
```

Another way to state the same property is with a lemma:

```csharp
lemma maxProperty(a : nat, b : nat)
ensures max(a, b) >= a && max(a, b) >= b
ensures max(a, b) == a || max(a, b) == b
{

}
```

In general, lemmas are useful for proving properties of functions. They are especially useful with inductive arguments.
The body of the lemma is empty because Dafny is able to complete the proof without any additional information.

## Predicates

These are just functions that return Boolean values (**bool**).

## Classes

The same **class** keyword but without any scope detail.

# Exercises

   * Counter.dfy: start with nothing and iteratively add in the specification. Change an *inc* to a *dec* to violate a precondition to see what happens.
   * List.dfy
   * Quantifiers.dfy
   * Secret.dfy
   * Tictactoe.dfy

# Dafny Notes:

   * Be sure that any time `new` is used, that an appropriate `fresh` property is in a post-condition
   * Be sure to use `old` in the ensures clauses to define how something changes (or does not change)
   * To track down issues, use `assert` at the first place you believe something is true and then propagate it forward to the point where Dafny is unable to prove a property---the `assert` tells you what Dafny knows, where in the proof it knows it, and where it no longer knows it (usually after some method call that does not have strong enough post-conditions to preserve the assertion)
   * Tick operator indicates that a specific field is to be read or modified. Consider the object `head`. The `reads ``head` notation means that the `head` field is to be read as in `(head != null)`. It does not mean that any fields in `head` are to be accessed. To access fields in the object `head`, use `reads head` alone in the reads/modifies clause. With that `head.value > 0` is ok to do.

     * [Dafny Power User: old and unchanged](http://leino.science/papers/krml273.html) see Section 4.0 *More precise frame expressions*.
     * [More on the tick operator](http://www.cse.chalmers.se/edu/year/2018/course/TDA567/Exercises/FormalSpecification2/Dafny4JavaProgrammers.pdf).

   * Be as specific as possible in `modifies` and `reads` clauses meaning that you only list fields or values that you actually read or modify
   * Be careful with `old`

      * `old(t.primitive)` where the method changes `t.primitive` and `primitive` is a primitive type then `old` does exactly what is expected grabbing the old value of `t.primitive`
      * `old(t)` where `t` is some object is looking at the reference stored for `t`---if that reference has not changed (e.g., the object has not been assigned to reference something new), then all the fields in the object will be the same

   * Example:

        function f(t : Object) : bool
        reads t`x; {
           t.x == true
        }

        method g(t : Object) returns ()
        modifies t`x;
        requires t`x == false;
        ensures t`x == f(old(t)); {
           t.x := !f(t);
        }

    The above code will fail because the reference for `t` has not changed; rather, the value of its field has changed. The field has an old value; the reference for `t` does not.

  * Counter-examples are easier (only possible) to understand by assigning each part of the expression to a local variable. So capture all the values in local variables, and then write the assertion over those variables. In general, the counter-examples print the local variables. Objects are still heap references, but it is possible to pull out primitive fields into local variables to see the values.

## Advanced Example

See [LinkedListWithSeq.dfy](LinkedListWithSeq.dfy) and [LinkedListWithoutSeq.dfy](LinkedListWithoutSeq.dfy) for two ways to prove properties of a linked list. These include a recursive definition for `Valid()` that maintains an invariant on the list using `ghost` variables. A clean version with only required contract clauses is in [LinkedList.dfy](LinkedList.dfy).