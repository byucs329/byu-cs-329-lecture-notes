/*
 * Consider the following partially implemented class List which represents 
 * fix size lists. On this class, method head returns the initial element of
 * the list (i.e. the head of the list), method shift removes the head of the 
 * list, method snoc adds element at the end of the list, the variable size 
 * represents the total amount of values on the list and the constructor init 
 * is used to initialize List objects. On a list, values are stored on the 
 * array a. Method Main is there only for testing purposes.
 *
 * Your task is to add specifications for all the methods in the class. When 
 * doing this, take into account that:
 *    -- size is greater or equal than zero and can be increased up to a.Length.
 *    -- The array a cannot be null.
 *    -- After calling constructor init there should be space for at least one 
 *       element in the list.
 *    -- If the size is strictly greater than zero, then all of the following must hold:
 *          1) head returns the initial element of the list (i.e. the head of the list).
 *          2) shift shifts all the elements in a one position to the left.
 *    -- In snoc, size represents the index where the new element should be stored.
 * You can define any predicate or function which you consider convenient .
 *
 * After there errors after writing the specification? If so, was it on an assertion? Was it on a postcondition? Reflect about this.
 */

class List {

  // container for the list's elements
  var a: array?<int>;
  // size of the list
  var size: nat;

  predicate isValid() 
    reads `a, `size
  {
    && a != null
    && a.Length > 0
    && 0 <= size <= a.Length
  }

  constructor init(len:int)
    requires len > 0
    ensures
      && isValid()
      && fresh(a)
      && a.Length == len
      && size == 0
  {
    a := new int[len];
    size := 0;
  }

  method snoc(e: int)
    modifies a, `size
    requires
      && isValid()
      && size < a.Length
    ensures
      && size == old(size) + 1
      && a[old(size)] == e
      && forall i | 0 <= i < old(size) :: a[i] == old(a[i])
  {
    a[size] := e;    
    size := size + 1;    
  }

  method shift()
    modifies a, `size
    requires 
      && isValid()
      && size > 0
    ensures
      && size == old(size) - 1
      && forall i | 0 <= i < size :: a[i] == old(a[i+1])
  {
    forall (i | 0 <= i < size-1 ) {
      a[i] := a[i + 1];  
    }
    size := size - 1;
  }
  
  method head() returns (h:int)
    requires 
      && isValid()
      && size > 0
    ensures
      h == a[0]
  {
    h := a[0] ;
  }
  
}

method {:test} test() 
{
  var list := new List.init(4);
  var aux : int;  
  
  list.snoc(2);
  aux := list.head();
  assert (aux == 2) ;    
  
  list.snoc(3);    
  list.snoc(4);
  aux := list.head();    
  assert (aux == 2) ;
  
  list.shift();
  aux := list.head();
  assert aux == 3 ;
}