class ListNode {

    var next: ListNode?;

    var value: int;

    var size: int;

    ghost var nodes: set<ListNode>
    ghost var values: seq<int>

    predicate Valid() 
        reads this, nodes
        decreases {this} + nodes
    {
        size == |nodes| == |values| &&
        size > 0 &&
        (size > 1 ==> next != null) &&
        this in nodes &&
        value in values &&
        (next == null ==> values == [value]) &&
        (next != null ==> next in nodes && next.nodes == nodes - {this} &&
                          values == [value] + next.values && next.Valid() &&
                          next.size == |next.nodes| == |next.values| &&
                          next.size == size - 1)
    }

    constructor (val : int)
        ensures next == null
        ensures value == val
        ensures values == [value]
        ensures nodes == {this}
        ensures size == 1
        ensures Valid()
    {
        next := null;
        value := val;
        values := [value];
        nodes := {this};
        size := 1;
    }

    method Insert(val: int) returns (node: ListNode)
        requires Valid()
        ensures Valid()
        ensures node.Valid()
        ensures fresh(node)
        ensures node.size == this.size + 1
        ensures node.next == this
        ensures node.value == val
        ensures node.values[0] == val
        ensures forall i: int :: 0 <= i < old(size) - 1 ==> node.values[i+1] == old(values[i])
    {
        node := new ListNode(val);
        node.next := this;
        node.values := [val] + this.values;
        node.nodes := this.nodes + {node};
        node.size := this.size + 1;
    }

    method Size() returns (listSize: int)
        requires Valid()
        ensures listSize == this.size
        ensures Valid()
    {
        listSize := this.size;
    }

    method Search(val: int) returns (result: bool) 
        requires Valid()
        ensures Valid()
        ensures val in values ==> result
        ensures val !in values ==> !result
    {
        var len := size;
        var current := this;
        
        result := false;
        while (len > 0 && current != null)
            decreases len - 0
            invariant current != null ==> current.Valid()
            invariant current != null ==> |current.values| == len
            invariant val !in values && current != null ==> val !in current.values
            invariant val !in values ==> !result
            invariant val in values ==> (current != null && val in current.values) || result
        {
            if (current.value == val) {
                result := true;
            }
            current := current.next;
            len := len - 1;
        }
    }
}
method Main()
{
    var node1 := new ListNode(3);
    var size := node1.Size();
    assert (size == 1);
    var node2 := node1.Insert(2);
    size := node2.Size();
    assert (size == 2);
    assert (node2.next == node1);
    var node3 := node2.Insert(1);
    size := node3.Size();
    assert (size == 3);
    assert (node3.next == node2);
    assert (node3.next.next == node1);
    var result := node3.Search(3);
    assert (result == true);
    result := node3.Search(2);
    assert (result == true);
    result := node3.Search(1);
    assert (result == true);
    var node4 := node3.Insert(4);
    result := node3.Search(4);
    assert (result == false);
    result := node4.Search(4);
    assert (result == true);
}