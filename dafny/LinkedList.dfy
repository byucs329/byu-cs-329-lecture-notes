class ListNode {

    var next: ListNode?;

    var value: int;

    var size: int;

    ghost var nodes: set<ListNode>
    ghost var values: seq<int>

    predicate Valid() 
        reads this, nodes
        decreases nodes + {this}
    {
        size == |nodes| &&
        this in nodes &&
        (next == null ==> values == [value]) &&
        (next != null ==> next in nodes && next.nodes <= nodes &&
          this !in next.nodes && value in values && next.values <= values && values == next.values + [value] && next.Valid())
    }

    constructor (val : int)
        ensures next == null
        ensures value == val
        ensures values == [value]
        ensures nodes == {this}
        ensures size == 1
        ensures Valid()
    {
        next := null;
        value := val;
        values := [value];
        nodes := {this};
        size := 1;
    }

    method insert(value: int) returns (node: ListNode)
        requires Valid()
        ensures node.Valid()
        ensures Valid()
        ensures |values| == old(|values|)
        ensures |nodes| == old(|nodes|)
        ensures size == old(size)
        ensures fresh(node)
        ensures node.size == old(size) + 1
        ensures node.next == old(this)
        ensures node.value == value
        ensures value in node.values
        ensures node.values == values + [value]
    {
        node := new ListNode(value);
        node.next := this;
        node.values := this.values + [value];
        node.nodes := this.nodes + {node};
        node.size := this.size + 1;
    }
    
    method listSize() returns (size: int)
        requires Valid()
        ensures size == this.size
        ensures Valid()
    {
        size := this.size;
    }

    method {:tailrecursion true} search(value: int) returns (result: bool) 
        requires Valid()
        ensures value in values ==> result == true
        ensures value !in values ==> result == false 
        ensures Valid()
        decreases nodes + {this}
    {
      if (this.value == value) {
        result := true;
        return;
      } 
      
      if (this.next == null) {
        result := false;
        return;
      } 
  
      result := this.next.search(value);
    }
}

method Main()
{
    var node1 := new ListNode(3);
    var size := node1.listSize();
    assert (size == 1);
    var node2 := node1.insert(2);
    size := node2.listSize();
    assert (size == 2);
    var node3 := node2.insert(1);
    size := node3.listSize();
    assert (size == 3);
    assert (node3.next == node2);
    assert (node3.next.next == node1);
    var search_2 := node3.search(2);
    assert (search_2 == true);
}