/*
Consider the following class Hashtable. This class represents an open addressing hash table with linear probing as collision resolution. Within a hash table, values (of type int) are stored into a fixed array hashtable. Besides, in order to have an easy way of checking whether or not the capacity of hashtable is reached (i.e. the array hashtable is full), a field size keeps track of the number of stored objects and a field capacity represents the total amount of objects that can be added to the hash table.
The method add, which is used to add values to the hash table, first tries to put the corresponding value at the position of the computed hash code. However, if that index is occupied, then add searches upwards (modulo the array length) for the nearest following index which is free. A position is considered free if and only if it contains a 0.

Add specifications and loop invariants (if necessary) for all the methods and predicates in the class taking into account that:

The size field is never negative, and always less or equal than capacity.
The capacity should be the same value as hashtable.Length.
The array hashtable cannot be null.
There should be space for at least one element in the hash table.
The number of elements stored in array hashtable (i.e., the number of array cells whose content is not 0) is size.
The values added in hashtable are always positive (i.e. bigger than 0).
If the size is strictly smaller than capacity, then all of the following must hold:
add increases size by one.
After add(val,key), val is stored in hashtable at some valid index.
*/

class HashTable {
    
      // Open addressing Hashtable with linear probing as collision resolution.
    
      var hashtable:array?<int>;
      var capacity:int;
      var size:int;	
	
      predicate Valid()
       reads this, hashtable;
      {
       hashtable != null && 
       0 <= size <= capacity && 
       capacity == hashtable.Length &&
       capacity > 0  
      }

      method Init(c:int)
       requires c > 0;
       ensures Valid();
       ensures capacity == c && size == 0 && hashtable != null;
       ensures forall i :: 0 <= i < hashtable.Length ==> hashtable[i] == 0;
       ensures fresh(hashtable);
       modifies this;
      {
        hashtable := new int[c]; 
        capacity := c;
        size := 0;
        
        forall (i | 0 <= i < hashtable.Length) {
           hashtable[i] := 0;
        }
      }
      	
     function method hash_function(key:int):(int)
      requires Valid();
      reads this, hashtable;
     {
       var result:int := 0;
        
       if (key >= 0)
       then key % capacity
       else (key * -1) % capacity
      }

      method Add(val:int, key:int)
       requires Valid();
       requires val > 0;
       requires size < capacity;
       ensures Valid();
       ensures size == old(size) + 1;
       ensures exists j :: 0 <= j < capacity && hashtable[j] == val;
       modifies hashtable, `size;
      {
        var i:int := hash_function(key);
        assert 0 <= i < capacity;
        if (hashtable[i] != 0) {
           var j:int := 0;
  
           while (hashtable[i] != 0 && j < capacity)
             invariant 0 <= j <= capacity;
             invariant 0 <= i < capacity;
             decreases capacity - j;
           {
             if (i == capacity-1) 
                {i := 0;}
             else {i := i + 1;}
             
             j := j + 1;
 	       }
        }
        hashtable[i] := val;
        size := size + 1;   
        assert hashtable[i] == val;
        assert exists j :: 0 <= j < capacity && hashtable[j] == val;     
      }
}
