/*
 * Consider the partially implemented class Tictactoe which represents a Tic 
 * tac toe game. This class represents the grid by using the array board. 
 * However, when fillid in by using method PutValue, its coordinates are given 
 * in the usual two-dimentional way, i.e. by giving its coordinates x and y on
 * the grid. Method BoardPosition translates (x,y) coordinates into valid 
 * indexes on the board.
 * 
 * Your task consists of adding specifications for all the methods in the 
 * class. When doing this, take into account that:
 * 
 *   - The array board cannot be null and its length is always 9.
 *   - Constructor Init is used to initialize Tictactoe objects.
 *   - For any index i, board[i] == 0 means empty entry, board[i] == 1 means O, 
 *     board[i] == 2 means X.
 */
class Tictactoe {
  // game board
  // board[i] == 0 means empty entry, board[i] == 1 means O, board[i] == 2 means X
  var board : array<int> ;  

  constructor Init()
  {
      // Initialize const fields, if any, in the first division.
      new;
      board := new int[9];
      var i := 0;
      while (i < 9) {
        board[i] := 0;
        i := i + 1;
      }
  }
  
  method BoardPosition (x:int, y:int) returns (index:int)
  {
    index := (y * 3) + x ;
  }
  
  method PutValue(x:int, y:int, val:int) 
  {
      var index := BoardPosition(x,y) ;
      board[index] := val ;
  }

  method Main () 
  {
     var game := new Tictactoe.Init();     
     assert (game.board[0] == 0);     
   
     var aux : int ;
     aux := game.BoardPosition(1,1) ;
     assert aux == 4 ;
     
     game.PutValue(1,1,1) ;               
     assert game.board[4] == 1;
      
     aux := game.BoardPosition(2,1) ;
     assert aux == 5 ;     
     
     game.PutValue(2,1,2) ;
     assert game.board[5] == 2;     
  }
}
