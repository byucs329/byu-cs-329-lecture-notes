// Specify the counter so that the assertion is proved.
class Counter {

  var value : int ;
  
  predicate valid()
  reads `value;
  {
    this.value >= 0
  }
  
  constructor init() 
  ensures valid();
  ensures value == 0;
  {
    value := 0 ;
  }
  
  /* 
   * the value of the counter
   */
  method getValue() returns (x:int)
  ensures x == value;
  {
    x := value ;
  }
  
  /*
   * increment the counter
   */
  method inc()
  modifies this`value;
  ensures value == old(value) + 1
  {
    value := value + 1;
  }
  
  /* 
   * decrement the counter
   */
  method dec()
  modifies `value;
  requires value > 0
  ensures valid();
  ensures value == old(value) - 1;
  { 
    value := value - 1 ;
  }
  
  /*
   * Test method
   */
  method Main ()
  {
   var count := new Counter.init() ;
   count.inc();
   count.dec();
   count.inc();
   count.inc();
   var aux : int := count.getValue();
   assert (aux == 2) ;
  }
}
