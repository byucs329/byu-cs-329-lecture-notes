predicate method isLeapYear(y: int) {
  y % 4 == 0 && (y % 100 != 0 || y % 400 == 0)
}

// Does this method terminate?
method WhichYear_InfiniteLoop(d: int) returns (year: int) {
  var days := d;
  year := 1980;
  while (days > 365) 
  decreases days - 365 {
    if isLeapYear(year) {
      if days > 366 {
        days := days - 366;
        year := year + 1;
      }
    } else {
      days := days - 365;
      year := year + 1;
    }
  }
}

method computeDaysInYear(y: int) returns (days : int) 
ensures 365 == days  || days == 366 {
  if (isLeapYear(y)) {
    days := 366;
  } else {
    days := 365;
  }
}

method WhichYear_good(d : int) returns (year: int) {
  var days := d;
  year := 1980;
  var daysInYear := computeDaysInYear(year);
  while(days > daysInYear) 
  decreases days - daysInYear {
    days := days - daysInYear;
    year := year + 1;
    daysInYear := computeDaysInYear(year);
  }
}
