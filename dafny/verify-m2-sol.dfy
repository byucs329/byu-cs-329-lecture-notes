method M2(a : int, b : int, c : int) returns (m : int)
ensures (m == a || m == b || m == c);
ensures (m <= a && m <= b && m <= c) ;
{
  m := a;
  if (b < m) 
  { m := b; }
  if (c < m)
  { m := c; } 
}

/*
Proof: 
Let R = (m == a || m == b || m == c) && (m <= a && m <= b && m <= c)
By Seq. rule (twice)
wp(m:=a, wp(if (b < m)..., wp(if (c < m)..., R))))

By Conditional rule
wp(m:=a,
  wp(if (b < m)...,
   (c < m ==> wp(m:= c, R)
    &&
   !(c < m) ==> R)

By Assignment rule
wp(m:=a,
  wp(if (b < m)...,
   (c < m ==> (c==a || c==b ||c==c) && (c <= a && c <= b && c <= c)
    &&
   !(c < m) ==> R))

By Conditional rule, and simplification:
wp(m:=a,
  (b < m ==> wp(m:=b, (c < m ==> c <= a && c <=b) &&
                      (m <= c ==> R)))
    &&
   !(b < m) ==> (c < m ==> c <= a && c <=b) &&
                (m <= c ==> R)

By Assignment rule
wp(m:=a,
  (b < m ==> (c < b ==> c <= a && c <= b) &&
             (b >= c ==> (b == a || b == b || b == c) && (b <= a && b <= b && b <= c)) 
   &&
   !(b < m) ==> (c < m ==> c <= a && c <=b) &&
                (m <= c ==> R)

Simplify
wp(m:=a,
  (b < m ==> (c < b ==> c <= a && c <= b) &&
             (b <= c ==> (b <= a && b <= c)) 
   &&
   !(b < m) ==> (c < m ==> c <= a && c <=b) &&
                (m <= c ==> R)

Apply Assignment
(b < a ==> (c < b ==> c <= a && c <= b) &&
             (b <= c ==> (b <= a && b <= c)) 
   &&
!(b < a) ==> (c < a ==> c <= a && c <=b) &&
                (a <= c ==> R[m -> a])

First conjunct simplifies to true, simplify second conjunct to get
true
&&
!(b < a) ==> true &&
             (a <= c ==> (a == a || a == b || a == c)
              && (a <= a && a<= b && a <= c)

Simplify
true
*/