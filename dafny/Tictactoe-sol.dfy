/*
 * Consider the partially implemented class Tictactoe which represents a Tic 
 * tac toe game. This class represents the grid by using the array board. 
 * However, when fillid in by using method PutValue, its coordinates are given 
 * in the usual two-dimentional way, i.e. by giving its coordinates x and y on
 * the grid. Method BoardPosition translates (x,y) coordinates into valid 
 * indexes on the board.
 * 
 * Your task consists of adding specifications for all the methods in the 
 * class. When doing this, take into account that:
 * 
 *   - The array board cannot be null and its length is always 9.
 *   - Constructor Init is used to initialize Tictactoe objects.
 *   - For any index i, board[i] == 0 means empty entry, board[i] == 1 means O, 
 *     board[i] == 2 means X.
 */
class Tictactoe {
  // game board
  // board[i] == 0 means empty entry, board[i] == 1 means O, board[i] == 2 means X
  var board : array?<int> ;  

predicate valid() 
reads this, board; {
  board != null && board.Length == 9
}

  constructor Init()
  ensures fresh(board);
  ensures valid();
  ensures forall i :: 0 <= i < board.Length ==> board[i] == 0;
  {
      // Initialize const fields, if any, in the first division.
      new;
      board := new int[9];
      var x : int := 0;
      while (x < 9) 
      modifies board;
      decreases 9-x;
      invariant forall i :: 0 <= i < x ==> board[i] == 0; {
        board[x] := 0;
        x := x + 1;
      }
  }
  
  predicate validCoord(x:int, y:int) {
    0 <= x < 3 && 0 <= y < 3   
  }

  predicate validIndex(x : int) 
  reads this, this.board {
    valid() && 0 <= x < board.Length
  }
  function method boardPosition (x:int, y:int) : int
  //requires validCoord(x, y); 
  {
    (y * 3) + x
  }
  
  predicate validVal(x : int) {
    x == 1 || x == 2
  }

  method PutValue(x:int, y:int, val:int) 
  requires valid();
  requires validCoord(x,y);
  requires validVal(val);
  ensures valid();
  ensures board[boardPosition(x,y)] == val;
  modifies board;
  {
      var index := boardPosition(x,y);
      board[index] := val ;
  }

  method Main () 
  {
     var game := new Tictactoe.Init();     
     assert (game.board[0] == 0);     
   
     var aux : int ;
     aux := game.boardPosition(1,1) ;
     assert aux == 4 ;
     
     game.PutValue(1,1,1) ;               
     assert game.board[4] == 1;
      
     aux := game.boardPosition(2,1) ;
     assert aux == 5 ;     
     
     game.PutValue(2,1,2) ;
     assert game.board[5] == 2;     
  }
}
