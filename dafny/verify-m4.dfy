// Find the missing invariant which allows Dafny to prove correctness of the following program. If you have time, also prove the partial correctness on paper:

method M4 (n : int) returns (sq1 : int, sq2 : int)
requires n >= 0 && n * n >= n;
ensures  (sq1 == sq2 && sq1 * sq1 == n) ||
         (sq1 + 1 == sq2 && sq1 * sq1 < n && sq2 * sq2 >= n);
{
  sq1 := 0;
  sq2 := n;
  var mid := 0;
  while (sq2 - sq1 > 1) 
  {
    mid := (sq1 + sq2) / 2;
    if (mid * mid == n) {
      sq1 := mid;
      sq2 := mid;
    } else {
      if (mid * mid < n) {
        sq1 := mid;
      } else {
        sq2 := mid;
      }
    }
  }
  
}

// The program uses monotonicity to calculate the integer approximation of the square root of n.