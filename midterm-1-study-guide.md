# Overview

Midterm 1 covers the visitor-pattern, specification, black-box testing, control flow graphs, data-flow analysis, and mocking. You are allowed one page of notes on the exam. The paper size is 8.5x11 with no other restrictions.  The target is 1.5 to 2 hours of student time to complete. The format is short answer/free response.

# Comprehensive List of Topics

The below is a comprehensive list of topics on the exam. If a topic is not on the below list, then that is not being tested for this course.

  * Indicate input for black-box tests from a specification stating what a method *requires* and *ensures*. Use both input space partitioning and boundary value analysis to determine necessary input (HW 2, [specification-blackbox/specification.md](specification-blackbox/specification.md), and [specification-blackbox/blackbox.md](specification-blackbox/blackbox.md)).
  * Create a control flow graph from a given piece of code using the algorithm studied in class and as implemented by the `ControlFlowGraphBuilder` object. Identify when edges and nodes are added to the graph and by what visit method they are added (lab 1 and [cfg-rd-lecture.md](cfg-rd-lecture.md)).
  * Perform a reaching definition analysis on a given control flow graph and code. Clearly indicate *kill/gen* sets as well as *entry/exit* sets for each node (lab 1 and [cfg-rd-lecture.md](cfg-rd-lecture.md)).
  * Argue whether are not Mockito, with its verify interface, is a good tool to use to verify interactions on an interface and test correctness for a given system (lab1 and [max-blocks-mockito/mockito.md](max-blocks-mockito/mockito.md)).

<!-- Not tested this semester: -->

  <!-- Write a specification for a group of methods from a colloquial English description. Be sure to clearly indicate what the method *requires* and *ensures* based on the description (HW 2, [specification-blackbox/specification.md](specification-blackbox/specification.md), and [specification-blackbox/blackbox.md](specification-blackbox/blackbox.md)).  -->

  <!-- Indicate input for black-box tests for a Java program analysis tool using black-box input space partitioning and boundary value analysis (Lab 0, Lab 1, and Lab 2). -->

  <!-- Identify from a given visiter on the Eclipse DOM what it computes ([visitor-patter/README.md](visitor-pattern/README.md)).
   -->
  <!-- * Demonstrate knowledge of the Eclipse DOM visitor including the various methods available to overwrite and how return values from `visit` affect traversal behavior. Be able to use the visitor pattern to do useful computation including the ability to change the AST by adding, removing, or moving code (Lab 0, Lab 1, Lab 2, and [visitor-patter/README.md](visitor-pattern/README.md)). -->
