# Objectives

Write a paper-pencil proof of type safety for simple programs.

# Readings

See [compilers/09-type-checking.ppt](compilers/09-type-checking.ppt).

# Intro

Many languages take advantage of a type system to make guarantees about what the program will and won't do. For example, a type system can guarantee that the bytes of an integer value are never accidentally reinterpreted as a floating point value. They can also guarantee that an object is never reinterpreted as an object of an unrelated type.

For example,

```java
int x = true;
```

is clearly not type safe. The same is true of

```java
MyClass o = "A string value";
```

Because it is impossible to prove type-safe behaviors precisely (just as it is impossible to model any interesting program property precisely), most type systems use rules to preserve *static type safety*, which proves type safety in all cases but may disallow acceptable programs (for example, programs whose type-unsafe behaviors all occur in dead code). Static type safety is proven using *type judgments*, which are general rules that apply to types of statements, such as assignments.

Today's goal is to familiarize you with the basics of type safety for simple languages. One of the lab assignments is to build a simple type checker by generating dynamic tests that if all pass represent a proof certificate of the type correctness of the system. What makes it tricky is that it is not enough to only check the explicit uses of a type, the type of every expression must be checked as well.

Throughout this lecture, references to the [slides](compilers/09-type-checking.ppt) refer to the type checking slides by Robby.

# Examples

Present early on as the goal towards which we're working.

Work in class, adding general typing rules for +, <, declarations, and functions. Here rather than use type judgements, argue logical reasoning on each statement following the natural flow of the program. Be sure to treat each statement as a whole. For example, the if-statement needs to consider the type safety of its expression, then-statement, and else-statement for it to be type correct.

```java
void f(int x) {
    int y = 2;
    if (x < y) {
        y = y + 1;
    } else {
        y = 0;
    }
}
```

Then, have them work through the following:

```java
void f(int x, int y) {
    int z;
    if (x + y < z) {
        y = y + 1;
    } else {
        y = z + x;
    }
}
```

Then, have students take a crack at a type rule for `while` statements.

What happens with casting? Use the standard `<=` or `>=` for *type compatible*.

## Illustrating the Type Check

Create a recursion tree to illustrate the proof process. For that tree, the edge is annotated with a function call named `Type(E, s)` that should return the type of `s`. The `E` is the environment that looks up types of variables, method names, etc. The return for the function is the *type* of `s` as determined by the type checker. In this way, the edges in the recursion tree going down in recursion are labeled with calls to the function `Type(E.s)`. The edges coming up out of recursion are labeled with the actual types computed. A node in the tree represents the obligations that must be met to prove the type of `s` in the call given the environment `E`. For simplicity, `Type` is left off the edge annotations and rather just it's arguments are shows as `(E,s)`.

Recursion bottoms up when `s` is something defined in the environment. For example, if `s` is the local variable `x`, then `Type(E, x)` is the lookup `E(x)`. That type is returned up on the edge. 

What happens at a node depends on `s`. Every language construct has a rule for determining it's type. Consider an assignment to a literal as `x = 10`. The rule for this statement is as follows:

```
  E |- x : t        E |- 3 : s      t := s
  ----------------------------------------
          E |- x = 3:void
```

What the rule means is that in order for `E` to prove that `x = 3` has type `void` which means it is *type-safe* (here `void` is used for all statements that have no type) it must show that `x` is type `int` and that `3` has type `int`. If the environment can show both these obligations, then it can conclude the type-safety of the assignment. In the recursion tree, the node shows the two obligations, and there is a call to type for each as: `Type(E, x)` and `Type(E, 3)`. So the node has two outgoing edges one for each obligation. Those each return a type, and if both are the correct type, then the return for this node is `void`. 

# ASTNode Type Judgements

These only apply to a restricted set of inputs that is described in the Lab 3 writeup. In general, there are no nested classes, fields have no initializers, no constructors, types are `int`, `boolean`, object, etc. 

## CompilationUnit

The environment is up
```
E \forall TypeDeclaration \in CompilationUnit, E 
```

# General Type Judgements

Type judgments are a mathematical presentation of reasoning about types. They correspond to type checking code, which we will come to later in the lecture. Intuitively, the type judgements organize the type-proof to track all the obligations in the proof. Think of it recursively (and maybe even it draw it down if needed)

## An assignment

To reason about the following example, we need to determine the type of `3` and determine if it is compatible with the type of `x`, which is `int`. We will assume that the type environment `E` knows about `x`. In other words, `E` can be queried to discover the type of `x`: `E : Names -> Types`

```java
x = 3;
```

Slide 28 shows a type proofing rule for an assignment. This rule is general enough to reason about every assignment. (Note that the slides use Extended Static Java, which disallows initializers on declarations and forces declarations to occur in a block at the beginning of each scope, as does C90.) Applied to the assignment in question, the resulting proof is as follows:

```
    t = E(x)         s = E(3)
    ----------   -------------------
    E |- x : t        E |- 3 : s      t := s
    ----------------------------------------
                 E |- x = 3
```

The notation is important where `t := s` means the type `t` can be assigned to something of type `s`.

## Assigning with a variable

It gets slightly more complicated when using variables on the right-hand side, as we need to reason about the type of `y`:

```java
x = y;
```

In this context, we need an *environment* (in this case, E) that knows about symbols defined in the current scope. More specifically, this environment is a *type environment*, which stores a symbol's type and not its value. In the slides (e.g., slide 17), the environment consists of the current library, class, method, and variables. Slide 27 shows a type judgment for evaluating the type of a variable. `:=` indicates assignment compatibility (e.g., `t := s` means that something of type `s` may be assigned to something of type `t`), a relation that is not necessarily symmetric.

With the ability to reason about the types of variables, the corresponding type proof is now:

```
     E(x) = t          E(y) = s
    ----------        ----------
    E |- x : t        E |- e : s      t := s
    ----------------------------------------
                E |- x = e
```

## Control flow

A one-armed `if` statement must ensure that its condition is boolean and that its block type checks:

```
    E |- c : boolean    E |- S
    --------------------------
          E |- if (c) S
```

A two-armed `if` statement must ensure that its condition is boolean and that both of its blocks type check:

```
    E |- c : boolean    E |- S1    E |- S2
    --------------------------------------
            E |- if (c) S1 else S2
```

## Sequencing

A sequence of two statements is type checked by type checking each statement:

```
    E |- S1    E |- S2
    ------------------
       E |- S1; S2
```

An exception to this rule is when an assignment modifies the environment:

```
    E[x -> t] |- S2
    ---------------
     E |- t x; S2
```

The above rule is important because it decides when the judgements happen in parallel as in the first case---order does not matter, and when the judgements have to be sequential as in the second case---order does matter. In the second case, the change to the environment is preserved by passing up the judgement of S2 after in the new environment. Note that this rules may also require that an initializer e be judged as well if x is assigned to e as is `t x = e`.  

To generalize the environment, a block should start a new environment always, and leaving the block should remove that environment. Then apply the two different rules according the code. Note that this only works when the statements are nested with a sequencing statement, for example, S1 is a non-sequence statement and S2 is a sequence statement with everything that follows after S1. How do the rules generalize to a list of statements is in the representation in the DOM?

The easiest solution to a list of statements is to just chew through them sequentially where S1 is the first statement and S2 is everything else that follows. 

```
                     E[x -> int] |- S3 
                     --------------------
          E |- S2    E |- int x; S3
          --------------------------
E |- S1   E |- S2;int x;S3
--------------------------
E |- S1;S2;int x;S3
```

## Casting

The Java rule for casting is summarized in slide 38. Note that it allows for upcasts (from a subclass to a base class), which are safe, and downcasts (from a base class to a subclass), which are only safe if the object being cast is actually compatible with the subtype.

```
    E |- e : t    t <= C or C <= t
    ------------------------------
            E |- (C) e : C
```

If we want to create a version that is statically type safe, we simply drop the right hand side:

```
    E |- e : t    t <= C
    --------------------
       E |- (C) e : C
```

However, this disallows some safe casts.

## Return statements

Add to the environment a *return_fun* that can be used to look up the type of the method being checked. Now return statements can be checked for the correct type (see this [lecture](https://courses.cs.cornell.edu/cs412/2004sp/lectures/lec13.pdf)).

## Putting it together

Slide 36 shows a complete type proof for a shockingly small program.

# Symbol Table

Consider the following pair of classes:

```java
class C {
    public static int c;
    public static void f() {
        c = D.d;
    }
}
class D {
    public static int d;
    public static void f() {
        d = C.c;
    }
}
```

There is a circular dependency between the classes. Whether **C** or **D** is visited first, one will not know about the other's members. We resolve this with two passes. The first gathers information about classes' members: fields, methods, and enclosed types. The second pass uses the information from the first to check for type safety. We need to be able to look up a symbol and determine its type.

# Implementation Notes

**Avoid just copying the code from the lecture notes**. It seems like a short cut, but in the end, it is the longer and harder way. There is huge value in seeing it done once, and then recreating it on your own. Figuring out the easier concepts in the beginning gives you the understanding and intuition that you need to work out the hard concepts that come after the easy ones. **You have been warned.**

The best visualization of the implementation is as a stack where each visit method pushes an empty list on the stack that collects obligations (everything on the top of the type proof rule). Once all the obligations are in the list, then the list is popped from the stack on the end visit, bundled in a `DynamicContainer` with the statement of what is proved (the bottom part of the type proof rule), and that new container is added to the list that is now on the top of the stack as an obligation.

Do the proof for the following class

```java
class EmptyMethodTest {
  int x = 2;

  void m() {
    ;
  }
}
```

Show the class as a proof tree and as the JUnit report of the test making a clear correspondence between the containers and the tests. Once that is established, label all the tests and containers with a number. Now map it all onto the visitor pattern where the visit method pushes a list on the stack to capture obligations, and the end-visit pops off the obligations, bundles them into a container, and thens adds that container to the list on the top of the stack. With that, rework the same proof, only this time simulating the stack of obligations, identifying what is added with the number labels. Indicate which visitor is active for each call. A summary of the discussion is in [type-proof-EmptyMethodTest.jpg](type-proof-EmptyMethodTest.jpg).

The goal for the rest of the class is to implement the tests and proof for the `EmptyMethodTest` input file. The symbol table is mocked the whole way.  The full implementation is on the `LectureNotes` branch in Eric's local repo.

## Notation and Proof Organization

There are *many* ways to organize the proof. The example on the white board in [type-proof-EmptyMethodTest.jpg](type-proof-EmptyMethodTest.jpg) shows two ways: a tree growing down and sideways similar to the test output and a tree group up and sideways stacking up the type judgement rules. These are both equally fine.

A third way to show the proof is with a tree growing down. The down edges on the tree are a call to get a type judgement of a particular part of the input. The up edges are the returned types. The nodes in the tree are the judgement at that point that is being decided. There should be a down edge from a node for every obligation in the rule associated with that judgement. There should also be the final *ok* test of `void, ... = void` at the node to decide the type to put on the up edge.

This third way to illustrate the proof mimics the actual algorithm implemented by `TypeCheckBuilder`. It shows the recursive calls, the return types, and the tests that are gathered along the way. Each of the nodes ultimately become `DynamicContainer` instances (see `endVisit`). The up coming edges ultimately become the `ok` test (`void, ... = void`)---the test is added in the container. 

## Testing the Tests

The test code is able able to both check the structure of generated tests for the proof, and it is able to run the test as well. Something to keep in mind though is that the `DynamicContainer` stores the stream, and that stream can only be traversed once. The implication is that it is not possible to get the set of tests and traverse that set multiple times. If the tests are run, the streams are consumed and cannot be run again, even if the tests are stored in a collection or list. Why? The nested nature of the proof. The containers store streams, and as soon as those streams are traversed, the tests are consumed.

To be very specific, to inspect the structure, or run the tests, the children of each dynamic container must be traversed. The method to get the children, `DynamicContainer.getChildren()`, returns a stream. Anything that is done to that stream consumes it, and it cannot by reset (to my knowledge). Each test will need a new stream.

Testing the implementation is a huge part of the lab. The live code is only a starting point. This will do two types of test: the structure of the tree via counting and actually running the proof.

A correct implementation should create a specific number of containers and a specific total number of tests. The intuition is that it is not possible visually inspect every proof to be sure it has the correct structure, so the counting is a weak surrogate to detect that the generated proof correct (e.g., it checks all the right things). As the code changes with more language features supported, these tests may fail avoiding a tedious manual traversal of every proof for every input file to determine if anything changed.

The proof is a tree structure, so the count methods are recursive. In this example streams with lambdas are used. Any other implementation is fine too. 

```java
private long getContainerCount(final Stream<? extends DynamicNode> proof) {
    return proof.filter(t -> t instanceof DynamicContainer)
        .mapToLong(v -> 1 + getContainerCount(((DynamicContainer) v).getChildren())).sum();
  }

  private long getTestCount(final Stream<? extends DynamicNode> proof) {
    return proof.mapToLong(t -> {
      if (t instanceof DynamicTest) {
        return 1;
      }
      return getTestCount(((DynamicContainer) t).getChildren());
    }).sum();
  }
```

What else can be tested on the structure: max nesting level, number of items in each container so it must match a string of integers, etc. The stronger the surrogate for correctness the better. The key is to balance the precision with the coding effort.

The tests fall out simply once the counting methods are in place. The symbol table mock is trivial in this case since there is no interaction given the test input.

```java
  @Nested
  @DisplayName("Tests Empty Class")
  class EmptyClassTests {

    String fileName = "EmptyClass.java";
    ISymbolTable symbolTable = Mockito.mock(ISymbolTable.class);
    
    @Test
    @DisplayName("Should create one container when given an empty class")
    void Should_createOneContainer_When_EmptyClass() {
      List<DynamicNode> proof = generateProof(fileName, symbolTable);
      long containerCount = getContainerCount(proof.stream());
      Assertions.assertEquals(1, containerCount);
    }

    @Test
    @DisplayName("Should create one test when given an empty class")
    void Should_createOneTest_When_EmptyClass() {
      List<DynamicNode> proof = generateProof(fileName, symbolTable);
      long testCount = getTestCount(proof.stream());
      Assertions.assertEquals(1, testCount);
    }

    @TestFactory
    @DisplayName("Should prove type safe when given an empty class")
    Stream<DynamicNode> Should_proveTypeSafe_When_emptyClass() {
      return generateProof("EmptyClass.java", symbolTable).stream();
    }
  }
```

As another example of testing the tests, consider the below that counts the number of statements that appear in the proof. With this code, it is possible to test that the correct number of statements from the input file appear in the generated proof.

```java
public int countStatements(Stream<? extends DynamicNode> stream) {
    return stream.mapToInt(node -> {
      if (node instanceof DynamicTest) {
        return 0;
      }
      DynamicContainer container = (DynamicContainer)node;
      String name = container.getDisplayName();
      boolean isStatement = name.matches("S\\d+(.*)");
      int count = isStatement? 1 : 0;
      return count + countStatements(container.getChildren());
    }).sum();
  }

  @Test
  @DisplayName("Should prove type safe and have 5 statements  when given input with 5 statements")
  public void should_proveTypeSafeAndHave5Statements_when_givenInputWith5Statements() {
    String fileName = "typeChecker/should_proveTypeSafe_when_givenVariableDeclrationsWithCompatibleInits.java";
    List<DynamicNode> tests = new ArrayList<>();
    boolean isTypeSafe = getTypeChecker(fileName, tests);
    assertTrue(isTypeSafe);
    assertEquals(5, countStatements(tests.stream()));
  }
```

There are many other properties that can be tested to reduce, for example, a generated proof to a somewhat unique signature. To aid in these tests, feel free to change the naming of different elements in the proof (e.g., `generateBlockName`, `generateStatementName`, etc.).

# OLD NOTES (Same but with sketchy code)

The below code is OK by not great. I do not recommend copying it and using it. It is way better to write your own code, figuring things out along the way, and looking here for reference.

**You have been warned**

Consider the assignment rule. 

```
 E |- x : t        E |- e : s      t := s
    ----------------------------------------
                E |- x = e
```

The rule has three obligations on the top. The left two should be `DynamicContainers`  for the lookup in the environment. The `t := s` is a `DynamicTest`. That puts three obligations in the list. The list is popped from the stack, put in a `DynamicContainer` with `E |- x = e` as the display name, and then that new dynamic container is added to the list on the top of the stack since it becomes an obligation for whatever rule triggered the assignment.

This obligation stack architecture lends naturally to the visitor pattern. The `visit` methods push a list on the stack while the `endVisit` methods pop a list from the stack, create a container, and add that container to the list on the top of the stack.

There are no obligations for an empty class, an test `E |- true` is added to represent *no obligation*. The following code is part of the visitor.

```java

 private Deque<List<DynamicNode>> proofStack = null;

 public List<DynamicNode> popProof() {
    return proofStack.pop();
  }

  private void pushProof(List<DynamicNode> proof) {
    proofStack.push(proof);
  }

  private List<DynamicNode> peekProof() {
    return proofStack.peek();
  }

  @Override
  public boolean visit(TypeDeclaration node) {
    className.push(node.getName().getIdentifier());
    pushProof(new ArrayList<>());
    return super.visit(node);
  }

  @Override
  public void endVisit(TypeDeclaration node) {
    className.pop();
    String displayName = "E |- " + node;
    createProofAndAddToObligations(displayName);
    super.endVisit(node);
  }

  private void createProofAndAddToObligations(String displayName) {
    List<DynamicNode> proofs = popProof();
    addNoObligationIfEmpty(proofs);
    DynamicContainer proof = DynamicContainer.dynamicContainer(displayName, proofs.stream());
    List<DynamicNode> obligations = peekProof();
    obligations.add(proof);
  }

  private void addNoObligationIfEmpty(List<DynamicNode> proofs) {
    if (proofs.size() > 0) {
      return;
    }
    
    proofs.add(generateNoObligation());
  }
  
  private DynamicNode generateNoObligation() {
    return DynamicTest.dynamicTest("E |- true", () -> Assertions.assertTrue(true));
  }
```

Make clear the push/pop relation between the `visit` and `endVisit` methods. Trace the stack evolution on the board.

## One Method with One Empty Statement

A single method with an empty statement is a small step from the empty class. The test and implementation is below. Note how it follows the same structure.

### Testing

```java
@Nested
  @DisplayName("Tests class with one method with empty statement")
  class OneMethodClassTests {

    String fileName = "OneMethodWithEmptyStatement.java";
    ISymbolTable symbolTable = Mockito.mock(ISymbolTable.class);
    
    @Test
    @DisplayName("Should create three containers when given a class with one method with empty statement")
    void Should_createThreeContainers_When_oneMethodWithEmptyStatement() {   
      List<DynamicNode> proof = generateProof(fileName, symbolTable);
      long containerCount = getContainerCount(proof.stream());
      Assertions.assertEquals(3, containerCount);
    }

    @Test
    @DisplayName("Should create three containers when given a class with one method with empty statement")
    void Should_createOneTest_When_oneMethodWithEmptyStatement() {
      List<DynamicNode> proof = generateProof(fileName, symbolTable);
      long testCount = getTestCount(proof.stream());
      Assertions.assertEquals(1, testCount);
    }

    @TestFactory
    Stream<DynamicNode> Should_proveTypeSafe_When_oneMethodWithEmptyStatement() {
      return generateProof(fileName, symbolTable).stream();
    }
  }
```

### Implementation

```java

  private void setMethodName(final String name) {
    methodName = name;
  }
   
  @Override
  public boolean visit(MethodDeclaration node) {
    pushProof(new ArrayList<>());
    setMethodName(node.getName().getIdentifier());
    return super.visit(node);
  }

  @Override
  public boolean visit(EmptyStatement node) {
    pushProof(new ArrayList<>());
    return super.visit(node);
  }
  
  @Override
  public void endVisit(MethodDeclaration node) {
    String displayName = "E |- " + node;
    createProofAndAddToObligations(displayName);
    super.endVisit(node);
  }
  
  @Override
  public void endVisit(EmptyStatement node) {
    String displayName = "E |- " + node;
    createProofAndAddToObligations(displayName);
    super.endVisit(node);
  }
```

The structure has not changed.

## One Method with Several Empty Statements

The easiest and most natural way to handle sequences is by using the `Block` type's `visit` and `endVisit` method. Here is the general idea that gives a good indication of what needs to happen though all the details are missing.

```java
  @Override
  public boolean visit(Block node) {
    pushFrame(new ArrayList<>());
    return super.visit(node);
  }

  @Override
  public void endVisit(Block node) {
    List<String> frame = popFrame();
    symbolTable.removeLocals(frame);
  }
```

Here the method is creating a frame to track in the environment any local variable declarations that are created in the scope. When the scope closes, those same variables are removed from the environment. Local variables can then be managed with the `VariableDeclarationFragment`. Here is a general direction that might be considered to make it happen.

```java
 @Override
  public boolean visit(VariableDeclarationFragment node) {
    pushProof(new ArrayList<>());
    
    String name = node.getName().getIdentifier();
    if (node.getParent() instanceof VariableDeclarationStatement) {
      VariableDeclarationStatement parent = (VariableDeclarationStatement)(node.getParent());
      String type = parent.getType().toString();
      symbolTable.addLocal(name, type);
      List<String> frame = peekFrame();
      frame.add(name);
    }
    
    Expression e = node.getInitializer();
    if (e != null) {
      assignmentObligations(name, e);
    }
    
    return false;
  }
```

There is more code that can be *demonstrated* in class. See the **InitializerNumberLiteral** branch in Eric's local repository for **lab3-type-checking-p1** for such details.

# Symbol Table

The interface is rather straight forward. Not much to say. Maybe just review it.

# Following up (another lecture later on)

It was useful to revisit type checking in class with an example. Some students were struggling to perform type proofs, to devise proofing rules for constructs we had not specifically addressed, or to translate a type proof into code.

We practiced in class in small groups (2-3 people) on the following example:

```java
package pack;

class C {
    Integer i;

    void f(int x) {
        int y = x + 2;
        i = new Integer(y);
    }
}
```

Each group created the type proof code (with respect to the given program) as follows:

  1. Determine what the symbol table should contain,

    - Classes:
        - Integer (java.lang.Integer)
        - pack.C
    - Fields:
        - pack.C, i, Integer
    - Methods:
        - pack.C, f, void, params:
            - x, int

  2. Write a pencil-and-paper type proof for the program,

    Part of the point of this exercise was for each group to think through what type checking a class definition should do. All that the class definition needs to do is ensure that its children are type checked.

  3. Determine which subclasses of ASTNode should be visited, and
  4. Write pseudo-code for each visitor
