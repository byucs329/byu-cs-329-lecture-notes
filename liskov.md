# Objective

  * Explain the Liskov Principle of Substitution
  * Weaken requires
  * Strengthen ensures

# Reading

  * [Liskov Substitution Principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle)
  * [Definition of the Liskov Substitution Principle](https://stackify.com/solid-design-liskov-substitution-principle/#:~:text=The%20Liskov%20Substitution%20Principle%20in,the%20objects%20of%20your%20superclass.)

# Outline

Liskov substitution is also known as subcontracting and is very related by design by contracts (really the same principle). The specification is the contract. Any subcontractor is valid as long is it meets the contract. In object oriented land, that means that any type can be replaced with any one of its subtypes and the program is guaranteed to be correct. This property is also known as behavioral subtyping.

The principle relies on understanding what it means to strengthen or weaken a clause in a specification. Requirements can not be strengthened in any subtype. Similarly, ensures cannot be weakened in any subtype. Illustrate with a simple boolean formula: `A /\ B` and a Venn diagram. The universe is all possible values of the propositions `A`, `B`, and `C`. The Venn diagram is the set of valuations that satisfy `A /\ B`. Strengthening makes the set smaller, so `Strengthen(A /\ B) \subseteq (A /\ B)`. Weaken is just the opposite. Weaken makes the set larger, so `A /\ B \subseteq weaken(A /\ B)`.

Work several examples to make clear the notion of *strengthen* and *weaken* in regards to a specification: weaken allows more possible values and strengthen allows fewer possible values.

Summary: requirements cannot be strengthened, but may be weakened, and ensures cannot be weakened, but may be strengthened. If a program is correct with type `T`, then it is also correct with any subtype of `T` if the subtypes obey the specification of the `T`. 
