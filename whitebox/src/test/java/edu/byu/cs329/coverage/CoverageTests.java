package edu.byu.cs329.coverage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import edu.byu.cs329.coverage.Coverage;

@DisplayName("Tests to demonstrate whitebox coverage concepts")
public class CoverageTests {

  @Test
  @DisplayName("Should give object-level branch coverage")
  void testCoverageRussianMultiplication() {
    // Assertions.assertEquals(2, Coverage.russianMultiplication(1, 2));
    Assertions.assertEquals(4, Coverage.russianMultiplication(2, 2));
  }

  @Test
  @DisplayName("Should give condition decision coverage")
  void testCoverageConditionDecision() {
    Assertions.assertEquals(0, Coverage.conditionDecision(3, 0));
    Assertions.assertEquals(1, Coverage.conditionDecision(4, 1));
    Assertions.assertEquals(1, Coverage.conditionDecision(2, 0));
  }

  @Test
  @DisplayName("Should give object-level branch coverage")
  void testCoverageFun() {
    Assertions.assertTrue(Coverage.fun(6, 14));
    Assertions.assertFalse(Coverage.fun(5, 0));
      Assertions.assertFalse(Coverage.fun(6, 15));
  }

  @Test
  @DisplayName("Should give MC/DC coverage")
  void testCoverageObcVsmcdc() {

  }

}
