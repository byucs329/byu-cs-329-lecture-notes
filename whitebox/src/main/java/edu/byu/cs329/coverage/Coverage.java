package edu.byu.cs329.coverage;

/**
 * Code to teach whitebox concepts.
 */
public class Coverage {

  private Coverage() {
  }

  /**
   * Multiplies two numbers with the Russian algorithm.
   *
   * @param a   Left operand
   * @param b   Right oprand
   * @return  the value of left * right
   */
  public static int russianMultiplication(int a, int b) {
    int z = 0;
    while (a != 0) {
      if (a % 2 != 0) {
        z = z + b;
      }
      a = a / 2;
      b = b * 2;
    }
    return z;
  }

  /**
   * Coverage for complex condition.
   *
   * @param a   integer
   * @param x   integer
   * @return  one or zero
   */
  public static int conditionDecision(int a, int x) {
    if ((3 > a) || (x != 0)) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * Illustrates object branch coverage.
   *
   * @param a integer
   * @param b integer
   * @return true or false
   */
  public static boolean fun(int a, int b) {
    return (a > 5) && (b < 15);
  }
  
  /** 
   * Illustrates the difference between OBC and MC/DC.
   *
   * @param a   boolean
   * @param b   boolean
   * @param c   boolean
   * @return    boolean
   */
  public static boolean obcVersusmcdc(boolean a, boolean b, boolean c) {
    if ((a || b) && c) {
      return true;
    } 
    return false;
  }
}
