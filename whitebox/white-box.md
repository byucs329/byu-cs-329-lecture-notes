# Objectives

  * Use white-box coverage criteria to determine sufficiency in test generation
  * Create tests to achieve coverage white-box coverage criteria
    * Statement coverage
    * Condition coverage
    * Branch or Decision coverage
    * MC/DC coverage
    * Complexity coverage
    * Path coverage

# Reading

  * [JaCoCo -- Java Code Coverage](https://www.eclemma.org/jacoco/trunk/index.html) --- see the [documentation](https://www.eclemma.org/jacoco/trunk/doc/index.html) in particular
  * [Intro to JaCoCo](https://www.baeldung.com/jacoco)
  * [JaCoCo Maven Plugin](https://www.eclemma.org/jacoco/trunk/doc/maven.html)
  * [White-box Testing](https://en.wikipedia.org/wiki/White-box_testing)
  * [Automated Test Generation of MC/DC](https://formalmethods.github.io/atg/2017/11/20/automated-test-generation-of-mcdc-part1.html)
  * [Modified Condition/Decision Coverage](https://en.wikipedia.org/wiki/Modified_condition/decision_coverage)
  * [A Practical Tutorial on Modified Condition/Decision Coverage](https://shemesh.larc.nasa.gov/fm/papers/Hayhurst-2001-tm210876-MCDC.pdf) --- Good source.
  * [A Flexible and Non-intrusive Approach for Computing Complex Structural Coverage Metrics](https://dl.acm.org/citation.cfm?id=2818817)
  * [Cyclomatic (Complexity) Coverage](https://en.wikipedia.org/wiki/Cyclomatic_complexity)

# JaCoco and ECLEmma

Here is the configuration from [Intro to JaCoCo](https://www.baeldung.com/jacoco) that works well.

```xml
<plugin>
	<groupId>org.jacoco</groupId>
	<artifactId>jacoco-maven-plugin</artifactId>
	<version>0.8.7</version>
	<executions>
		<execution>
			<goals>
				<goal>prepare-agent</goal>
			</goals>
		</execution>
		<execution>
		  <id>report</id>
			<phase>prepare-package</phase>
			<goals>
				<goal>report</goal>
			</goals>
		</execution>
	</executions>
</plugin>
```

Running the tests (```mvn test```) triggers JaCoCo automatically to create a ```./target/jacoco.exec``` file that contains the coverage information. The ```mvn jacoco:report``` command generates a human readable version in ```target/site/jacoco/index.html```. In that same directory is a CSV and XML version of the same report but each with decidedly less information in regards to where the uncovered elements are located. In general, prefer the HTML report.

Eclipse integrates the reporting: ```Window/Show View/Java/Coverage``` and gives the same HTML report. It is generated with the ```Coverage As``` option rather than ```Run As``` or ```Debug As``` options.

Legend for the report:

  * Red diamond indicates no branches have been covered
  * Yellow diamond indicates partial coverage
  * Green diamond means all branches covered

What coverage does it track:

  * Lines in terms of bytecodes
  * Branches
  * Cyclomatic complexity (number of independent paths through the code)

## jacoco:check

It is possible to declare minimum coverage requirements that are enforced by JaCoCo.

```xml
<execution>
    <id>jacoco-check</id>
    <goals>
        <goal>check</goal>
    </goals>
    <configuration>
        <rules>
            <rule>
                <element>PACKAGE</element>
                <limits>
                    <limit>
                        <counter>LINE</counter>
                        <value>COVEREDRATIO</value>
                        <minimum>0.60</minimum>
                    </limit>
                </limits>
            </rule>
        </rules>
    </configuration>
</execution>
```

```jacoco:check``` is part of the ```verify``` phase. For all the options, ```mvn help:describe -Dplugin=org.jacoco:jacoco-maven-plugin -Ddetail``` The section for ```jacoco:check``` has several examples and documentation.

# Classical Structural Coverage

This section is going to follow Section 2.3 on page 7 of [A Practical Tutorial on Modified Condition/Decision Coverage](https://shemesh.larc.nasa.gov/fm/papers/Hayhurst-2001-tm210876-MCDC.pdf). It uses its definitions as well.

White-box testing looks at the structure of the source-code and derives coverage metrics that represent different levels of test effort. It is not based on a specification of program behavior; rather, it is based on how the program is written and structured. It some regards, it is a syntactic definition for test effort based on the program text itself. Define the metrics relative to the control-flow graph where possible.

Use the [Coverage.russianMultiplication(int a, int b)](https://bitbucket.org/byucs329/homework-support/src/master/) for live coding. Add tests along the way.

  * Show how to run a test to report coverage in Eclipse and Maven (```mvn clean test jacoco:report```)
  * Show the drop down menu (*down triangle* in the coverage view) to set the counters
  * Show how to exclude the source folders in the **Coverage Configurations...** window
  * Indicate that if **Run As...** does not show *JUnit* as an option after using the `@Test` tag, then be sure to check the JRE in the build-path that it is 1.8 or higher for Jupiter. If it is wrong, check the *POM* file and be sure it indicates the JRE, then right click to select **Maven** --> **Update Project...**.

Here is the list of common structural coverage metrics for weakest to strongest in terms of control flow

**Statement coverage**: each statement is executed at least once by some test (e.g., each node in the control flow graph is seen). Minimum test standard for white-box.

Add the `Coverage.russionMultiplication(1,0)` test and view coverage. Set the counters to be **Line Counters**. Contrast with the **Instruction Counters**. Connect with the CFG for the method and show how each node in covered by the one test. Turn on **Hide Unused Elements** to clear noise in the window.

#  Coverage: condition, decision, MC/DC, and MCC

**Condition**: a leaf-level Boolean expression containing no Boolean operators (it cannot be broken down into simpler Boolean expressions)---a proposition.

**Decision** or **Branch**: a Boolean expression composed of conditions and zero or more Boolean operators. A decision without a Boolean operator is a condition. If a condition appears more than once in a decision, each occurrence is a distinct condition.

**Condition coverage** or **Predicate Coverage**: every condition in a decision in the program has taken all possible outcomes at least once.

```java
   @Test
   void testConditionCoverageConditionDecision() {
      Coverage.conditionDecision(2, 0);
	  Coverage.conditionDecision(4, 1);
   }
```
For ```if (A && B)```, tests ```(A=false, B=true)``` and ```(A=true, B=false)``` meet condition coverage. Notice though that it does not cover both sides of a branch.

**Decision** or **Branch Coverage**: every point of entry and exit in the program has been invoked at least once, and every decision in the program has taken all possible outcomes at least once. Another way to look at it is that each edge in the control flow graph is traversed at least once by some test. Stronger than statement coverage. Must define at what level the coverage takes place. At the semantic level there is short circuit logic which creates more branches than what is seen in the source code. For example, JaCoCo reports coverage at the byte-code level, and it is the byte-code that implements the short-circuit logic, so for the following code JaCoCo reports four rather than two branches:

```java
if (A && B) {
   // true branch
} else {
   // false branch
}
```

For JaCoCo, this codes looks like:

```java
if (A) {
  if (B) {
   // true branch
   goto done;
  }
}
// False branch here

done:
// rest of code
```

Branch-coverage always needs to define at which level it is to be understood: "*although structural code metrics are defined at the source code level, they are measured during execution of the compiled code, e.g., Java bytecode.*" ([Section 1 Paragraph 2](https://en.wikipedia.org/wiki/Cyclomatic_complexity)).  If it is at the source-level, then traversing each edge is sufficient. If it is the level of the implementation, then there are more edges in the control flow graph because of the short-circuit logic than what is seen in the control flow graph.

This bytecode level that JaCoCo measures is called *object-level* branch coverage or OBC. OBC is not enough for stronger coverage metrics such as *modified condition decision coverage* (MC/DC) defined below.

Add the `Coverage.russionMultiplication(2,0)` test and view coverage. Set the counters to be **Branch Counters**. Note that **Branch coverage implies statement coverage**.

Here is another example to illustrate some of the challenges with OBC.

```java
   @Test
   void testDecisionCoverageConditionDecision() {
	  Coverage.conditionDecision(1, 0);
	  Coverage.conditionDecision(4, 0);
   }
```
The above does not give OBC.

What coverage criteria do each of the inputs meet when considered individually?

  * `Coverage.russionMultiplication(3,3)`: SC
  * `Coverage.russionMultiplication(0,2)` : None
  * `Coverage.russionMultiplication(4,1)` : BC => SC

Consider the below code to show how some presentations differentiate branch and decision coverage.

```java
   int fun(int a, int b){
      return (a > 5) && (b < 15);
   }
```

 There is no branch per-se so only one test would be required for coverage. Decision coverage would require two tests however to cover both outcomes of the expression. Note that in JaCoCo, since it is tracking OBC, branch and decision coverage are the same. This equivalence is somewhat common in many presentations; though, some will not treat ```return (a > 5) && (b < 15);``` as part of branch coverage.

**Condition/decision coverage**: every point of entry and exit in the program has been invoked at least once, every condition in a decision in the program has taken all possible outcomes at least once, and every decision in the program has taken all possible outcomes at least once.

Again, a stronger statement. What would it be for the two examples?

```java
   @Test
   void testConditionDecisionCoverageConditionDecision() {
      Coverage.conditionDecision(1, 0);
	  Coverage.conditionDecision(4, 0);
	  Coverage.conditionDecision(1, 1);
	  Coverage.conditionDecision(4, 1);
   }
```

Is it possible to reduce the test set and keep the same coverage? Remove `Coverage.conditionDecision(1, 1);`. What about the other method?

**Multiple Condition**: [see page 7 in section 2.3](A Practical Tutorial on Modified Condition/
Decision Coverage)

**Modified condition/decision coverage**: every point of entry and exit in the program has been invoked at least once, every condition in a decision in the program has taken all possible outcomes at least once, and each condition has been shown to affect that decision outcome independently. A condition is shown to affect a decision's outcome independently by varying just that condition while holding fixed all other possible conditions. Short-circuit logic must be considered, and only conditions that can affect the outcome of the expression must be defined. This definition is the *masking* form of MC/DC to determine the independence of conditions. In masking MC/DC, a basic condition is masked if changing its value cannot affect the outcome of a decision.

The condition/decision criterion does not guarantee the coverage of all conditions in the module because in many test cases, some conditions of a decision are masked by the other conditions. Using the modified condition/decision criterion, each condition must be shown to be able to act on the decision outcome by itself, everything else being held fixed. The MC/DC criterion is thus much stronger than the condition/decision coverage because it requires each condition to be the controlling value, and that is not always possible to do in the code.

## MC/DC Lecture
Start with `A && B`. Generate tests for decision coverage. Give condition/decision coverage. Note that condition/decision coverage implies decision coverage.  Slightly modify the expressions to be `A = B || C` and `E = A && D`. Write the tests again for full decision coverage for `E`. Should be the same as `A && B`. But also need tests for the decision in `A`, but easy to cover as `A` must be false once and true twice. Expand out to see the three needed tests (see below).

Define the MC/DC coverage:  A condition is shown to affect a decision's outcome independently by varying just that condition while holding fixed all other possible conditions. Short-circuit logic must be considered, and only conditions that can affect the outcome of the expression must be defined.

Are the tests strong enough for MC/DC? No. Add in the missing test.

Repeat for some made up examples.

See [MC/DC explanation](https://formalmethods.github.io/atg/2017/11/20/automated-test-generation-of-mcdc-part1.html).

## MC/DC Notes

**Unique-cause approach**: use a truth table work through fixing inputs and showing how changing each condition changes the decision.

Analyze `Coverage.conditionDecision` for MC/DC.

Work `((a < b) || D) && (m ≥ n ∗ o)` carefully. Create a table that lists all the tests.

For an example of why it is not always possible, consider `(A and B) or (A and C)` where *A*, *B*, and *C* have no Boolean operators. The definition gives four conditions, but it is not possible to fix *A* and toggle *A* to affect the decision. Must rewrite the expression.

Here is another example of why it is tricky.  Consider `A = B || C` and `E = A && D`. Write the MC/DC tests for ```E```.

```
A  D  E
=======
T  T  T  <== Covers A with D fixed and Covers D with A fixed
F  T  F  <== Covers A with D fixed
T  F  F  <== Covers D with A fixed
```

Does the above cover `E = (B || C) && D`? No because there are more conditions in this expression. How the expression is written affects the coverage.

```
B  C  D  E
==========
T  F  T  T  <== Covers B with C and D fixed
F  F  T  F  <== Covers B with C and D fixed and also covers C with B and D fixed
F  T  T  T  <== Covers C with B ad D fixed and also covers D with B and C fixed
F  T  F  F  <== Covers D with B and C fixed
```

It is important to distinguish that OBC (object-level branch coverage) is not equivalent to MC/DC coverage, but that MC/DC coverage does achieve OBC. As a general rules, if you have `n` conditions, then you need at least `n+1` tests for MC/DC whereas it is possible to get OBC with just `n` tests.

Consider the below code to illustrate that OBC does not imply MC/DC.

```java
if ((A || B) && C) {
  // something
}
```

Full OBC is achieved with (try it with ```obcVsmcdc```)

```
A  B  C  O
==========
T  F  T  T
F  T  F  F
F  F  F  F
```

But these are not enough tests for MC/DC as C does not independently affect the outcome because the prefix is not held constant. Either of these do work:

```
 A  B  C
  =======
  F. T. T. <== B
  F. F. *. <== A and B
  T. *. T. <== A and C
  T. *. F. <== C

OR

  A  B  C
  =======
  T. *. T.
  F. T. T.
  F. T. F.
  F. F. *.
```

If you look at the CFG for the compiled code, then it is easier to work out via paths. Draw the CFG as a decision tree with the `A < B < C` order defined in the short circuit logic treating `A`, `B`, and `C` as unique decisions. For a given prefix to a branch, both sides of that branch must be exercised with the **same prefix**. For the first branch on `A`, the `T. *. T.` and `F. *. *.` cover both. For the `B` branch, `F. T. T` and `F. F. *.` cover both. For the `C` branch, either the `F. T` prefix or `T. *.` prefix work, but two tests are needed for both values of condition `C`.

## Activity

What do the following tests cover for `(x < 1 || y > z)`

  * `[x=0, y=0, z=1]` and `[x=2, y=2, z=1]`
  * `[x=2, y=2, z=1]` and `[x=2, y=0, z=1]`
  * `[x=2, y=2, z=2]`, `[x=0, y=0, z=1]`, `[x=2, y=0, z=0]`, `[x=2, y=2, z=1]`

# Mutation Coverage

Need a definition of weak and strong mutation coverage.

# Complexity Coverage

More tractable than path coverage. It is based on the notion of *independent* paths. Consider a few examples:

  * Sequential code: one path
  * Single if-statement: two paths
  * Nested if-statement: depends on the number of branches so if single outcome outer-if and dual-inner-if, then 3 paths.

Cyclomatic complexity measures the number of independent paths: *M = E - N + 2* where

  * E: edges in the CFG
  * N: Nodes in the CFG

Write tests to cover each linearly independent path.

Apply to `Coverage.russionMultiplication(int, int)`:

```java
   @Test
   void testComplexityCoverage() {
      // MC/DC coverage
	  Coverage.russianMultiplication(2, 0);
	  Coverage.russianMultiplication(1, 0);
	  Coverage.russianMultiplication(0, 0);
   }
```

What other coverage does it give?
