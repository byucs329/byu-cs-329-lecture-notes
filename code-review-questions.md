
# Code Review

A pull request must have a review **before** being merged into the mainline branch.

The review should address these questions:

  1. Are the commits in the history logically grouped and appropriately sized?
  1. Does the description adequately convey the changes being made?
  1. Is the code doing the right thing in the right way?
  1. Is the code organized clearly?
  1. Does the code style match the code around it?
  1. Is the behavior and implementation clear?
  1. Is it backward compatible?
  1. Is there a testing plan, adequate tests, and documentation?

The reviewer should comment briefly on these questions calling out where the changes in the pull request excel and where they can be improved.
The comments become part of the pull-request history.

The developer merges a pull request after all the reviewer concerns are addressed either in the comments or with a sub-sequent pull request and review.