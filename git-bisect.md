# Debugging

We have several tools available to us to find and fix bugs.

# Reading

See [git bisect](https://git-scm.com/docs/git-bisect)

## git bisect

git provides bisect, which performs a binary search over commits. Its goal is to identify the last "good" commit and the first "bad" commit, thus enabling developers to isolate a defect to a single diff. It relies on a means of testing whether or not a commit is good or bad. (Note that it is possible to use "new" and "old" instead; it is not necessarily just for debugging.)

Use the [BST repository](https://bitbucket.org/byucs329/binarysearchtree) to perform a git bisect. Specifically, the [bisect branch](https://bitbucket.org/byucs329/binarysearchtree/branch/bisect) and the [bisect-origin branch](https://bitbucket.org/byucs329/binarysearchtree/branch/bisect-origin). The workflow is as follows:

```bash
git checkout bisect
git bisect start
git bisect bad
git checkout bisect-origin
git bisect good
(Until git prints information about a commit - the output is different:)
    (Refresh Eclipse and run JUnit tests)
    (Depending on whether or not the tests pass:)
        git bisect good
    or
        git bisect bad
```

The bisect branch is ahead of bisect-origin by 301 commits. 300 of them are junk; they add or modify files in the bogus directory. Git bisect will narrow it down to where it breaks by putting forward a new revision each time. For each revision, run the tests, and make *good* or *bad* depending on the tests. Continue until there are no revisions and no steps left. The intuition for the tool is that it is possible to find a *last working commit* history, and from that, use bisect to narrow down the commits to the one that introduced tho error.

```
Bisecting: 0 revisions left to test after this (roughly 0 steps)
[a34ed1adb8f953ab30a701a27fa396e7f390c1e2] converted from CRLF to LF
```

**NOTE**: the POM file is junk for the bisect. Copy in the one from the HEAD of **bisect** or **bisect-origin**. Bisect will not conflict with the POM in its search. When it stops, it is sitting at commit that introduced the issue.

The bad commit converts files from CRLF to LF encodings (so it looks every line in every file has changed) but it also introduces a typo in the `Node.size()` method:

```java
public int size() {
  int lSize = 0;
  if (l != null) {
    lSize = l.size();
  }
  int rSize = 0;
  if (r != null) {
    rSize = r.size();
  }
  return 2 + lSize + rSize;
}
```
 
 The `2` should be `1`: root plus the two children sizes. This defect would not be easy to spot with all the lines showing as changed, but such a *worst case* is the exception and not the rule. Here are a few useful commands to isolate things:

   * `git cat-file commit HEAD`:prints all the details on the offending commit
   * `git diff HEAD^`: gives the diff relative to the head's parent (use `~` for head's child--add an index to indicate which branch if there is more than one parent or child for the commit) 
   * `git bisect reset`: restore everything back to original branch

 The `size()` function was added after the original tests were created, and it is not covered by any of the **BSTSet** regression tests, so the error was never spotted until the Dijkstra's tests revealed it. Take a moment to discuss the fact that **it's easy to rely on existing regression tests and miss the testing of new functionality.**

## Unit tests

To run a specific class for tests: `mvn -Dtest=BSTSetTests test` and then `mvn jacoco:report` to create the actual report. The `./target/jacoco.exec` file may need to be deleted to see the different in coverage.

Also, running coverage on _all_ of the (regression) tests shows coverage of `size()`, even though none of the unit tests in the **BSTSetTests** class uses the function at all. Running just the tests in **BSTSetTests** shows that `size()` is not covered by any unit tests.

Properly developed unit tests help programmers to locate software defects by identifying small parts of the program that contain flaws.
