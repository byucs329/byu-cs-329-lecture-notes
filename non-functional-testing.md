# Logistics

The first time through, this lecture was split with wrap-up for mutation testing. I anticipate that this will not be the case the second time through.

# Materials

This was a freewheeling discussion of the topics listed [on Wikipedia](https://en.wikipedia.org/wiki/Non-functional_testing) under non-functional testing. Some of the students had taken or were taking CS 465 and knew a little about side channels. We discussed the fact that side channels make it possible for attackers to learn about encryption keys in the presence of an encryption implementation that is completely correct from a functional perspective (it always produces the correct ciphertext) that is not resistant to these side channels.

We discussed performance testing, as well. We talked about different metrics that might or might not concern us. We discussed the example of streaming video. In this instance, multiple seconds of latency aren't that big of a deal, but throughput changes video quality and is a big deal. We then talked through a possible performance test. After a discussion of what that test might look like (streaming a particular video), we talked about what we had tested and hadn't. This led to a discussion of what we did know and didn't know.

In our example, we hadn't considered the shape of the network, the load of the network, the server or servers on which this particular movie's data are stored, the particulars of the client, and so on. This means that we had one measure of performance but knew nothing about performance under different circumstances. I followed up with a brief, hand-wavy discussion of different distributions that could be used to model network traffic and such. If a full class period is available for this topic, it would be worthwhile to prepare more time to discuss this topic.

In any event, the goal of this discussion was not to teach everything that they might want to know about non-functional testing but to introduce some of the basic ideas and to point out that they can easily read up on any of these topics as the need arises.
