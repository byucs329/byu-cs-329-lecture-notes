/*
Consider the following mutual exclusion algorithm that uses the shared
variables y_1 and y_2 (initially both 0).

Process P1:
   while true do
      ... noncriticalsection ...
      y_1 := y_2 +1;
      wait until (y_2 =0) || (y_1 < y_2)
      ... critical section ...
      y_1 :=0;
   od
 
Process P2:
   while true do
      ... noncriticalsection ...
      y_2 := y_1 +1;
      wait until (y_1 = 0) || (y_2 < y_1)
      ... critical section ...
      y_2 :=0;
   od

Create a verifiation model in PROMELA and use SPIN to verify the correctness of the algorithm (deadlock and mutual exclusion).

 Make it so there is only one proctype with two instances. Use inline to help readability. 
*/

#define MAX_BYTE  255

byte y_1 = 0
byte y_2 = 0

byte number_in_critical_section = 0

active proctype mutex_unamed_y_1() {
  bit c1 = false
loop:
  if
  :: y_2 == MAX_BYTE ->
	 (y_2 == 0)
  :: else
  fi
  
  y_1 = y_2 + 1
  (y_2 == 0 || y_1 < y_2)

crit:
  c1 = true
  number_in_critical_section++
  /*assert(number_in_critical_section == 1)*/
  number_in_critical_section--
  c1 = false
  
  y_1 = 0
  goto loop
  
}

active proctype mutex_unamed_y_2() {
  bit c2 = false
loop:

  if
  :: y_1 == MAX_BYTE ->
	 (y_1 == 0)
  :: else
  fi
  
  y_2 = y_1 + 1
  (y_1 == 0 || y_2 < y_1)

crit:
  c2 = true
  number_in_critical_section++
  /*assert(number_in_critical_section == 1)*/
  number_in_critical_section--
  c2 = false
  
  y_2 = 0
  goto loop
}

never mutex0 {
  do
  :: number_in_critical_section == 2 ->
	 break
  :: else ->
	 skip
  od
}

never mutex1 {
  do
  :: mutex_unamed_y_1@crit && mutex_unamed_y_2@crit ->
	 break
  :: else ->
	 skip
  od
}

never mutex2 {
  do
  :: mutex_unamed_y_1:c1 && mutex_unamed_y_2:c2 ->
	 break
  :: else ->
	 skip
  od
}