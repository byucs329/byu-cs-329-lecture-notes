/*
Consider the following mutual exclusion algorithm that uses the shared
variables y_1 and y_2 (initially both 0).

Process P1:
   while true do
      ... noncriticalsection ...
      y_1 := y_2 +1;
      wait until (y_2 =0) || (y_1 < y_2)
      ... critical section ...
      y_1 :=0;
   od
 
Process P2:
   while true do
      ... noncriticalsection ...
      y_2 := y_1 +1;
      wait until (y_1 = 0) || (y_2 < y_1)
      ... critical section ...
      y_2 :=0;
   od

Create a verifiation model in PROMELA and use SPIN to verify the correctness of the algorithm (deadlock and mutual exclusion).

 Make it so there is only one proctype with two instances. Use inline to help readability. 
*/
