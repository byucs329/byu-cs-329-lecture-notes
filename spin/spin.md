# Objectives

  * Classify properties as either safety or liveness
  * Write simple PROMELA models

    * Communicate with shared variables
    * Communicate with message passing

  * Verify safety properties in SPIN using assertions

# Reading

  * [install.md](install.md) for a installation and a primer
  * [http://spinroot.com/spin/whatispin.html](http://spinroot.com/spin/whatispin.html)
  * Principles of the SPIN Model Checker (electronic copy through [BYU Library](https://lib.byu.edu/))
  * The SPIN Model Checker (hard copy through [BYU Library](https://lib.byu.edu/))

# Safety Versus Liveness

A *safety property* reasons about the history of execution and is characterized as having a finite counter-example. A finite counter-example is some history or execution that ends in a bad state. In other words, starting at the last state and looking back through the history of execution it is possible to determine that the property is violated. Colloquially, a safety property ensures that nothing bad happens.

A *liveness property reasons about the future of execution and is characterized as having an infinite counter-example. An infinite counter-example is some future execution that never includes a desired behavior. In other words, starting at the current state and looking forward into the future execution it is possible to determine that the property never holds. Colloquially, a liveness properties ensures that something good happens.

Characterize the following:

  * A use of a variable must be preceded by a definition -- Safety
  * When a file is opened it must subsequently be closed -- Liveness
  * You cannot shift from drive to reverse without passing through neutral  -- Safety
  * No pair of adjacent dining philosophers can be eating at the same time  -- Safety
  * The program will eventually terminate -- Liveness
  * The program is free of deadlock -- Safety

# Model Checking with SPIN

**Emphasis**: build verification models for concurrent systems and verify properties in the model. One the model does what it is intended to do, build an actual system based on the model. In general, the computation in the system is _not_ the part to verify; rather the goal is to verify the interaction between the concurrent components. Common issues are deadlock and data race, but model checking is much more expressive; it can verify complex temporal properties that sequence concurrent actions. The nice thing about model checking is that its results are based on the most permissive scheduler possible, so it makes no assumptions about when concurrent entities run.

**PROMELA**: **PRO**cess **ME**ta **LA**nguage.

**SPIN**: **S**imple **P**romela **IN**terpreter --> Both "simple" and "interpreter" are no longer true.

PROMELA is _not_ an implementation language. It is a modeling language for verification.


## PROMELA Introduction.

Introduce PROMELA semantics building from *hello world* by adding more processes and requiring more ordering between processes. At the end of the lecture, students should have seen the following constructs (be sure to show students where to find the [spin manual pages](http://spinroot.com/spin/Man/promela.html)):

  * `proctype`, `active`, and array notation for `proctype`
  * `printf`
  * `init` process
  * `_pid` and `_nr_pr`
  * `assert`
  * predicates how they brock a process
  * `atomic`
  * `skip`
  * if-statement, do-statement, `timeout`, `else`, and `break`
  * `byte` as a type

Here is an a rough example of where the lecture might end. The example evolves to this state from the simple *hello world* starting point. The evolution is guided by questions asking what the program does (semantically) and how it needs to change to do something different. Introduce the different semantics along the way.

```
byte n = 0
byte order = 0

#define TOTAL 5
proctype main(byte i) {
  n == i
  printf("hello(%d)\n", _pid)
    n = n + 1
}

init {

  atomic {
    printf("init(%d)\n", _pid)
    do
    :: order < TOTAL -> run main(order)
               order = order + 1
    :: else -> break
    od 
    /*assert(_nr_pr == 4);*/
  }
  
  skip

}
```

Recall that `spin <filename>` interprets the file on some random schedule, `spin -i <filename>` is interactive so the user can choose which process to run, `spin -uN <filename>` stops simulation after `N` steps, and `spin -run <filename>` verifies the file. These options, and a whole lot more, are shown with `spin --help`.

Cover the output of spin including the meaning if invalid end states (processes that did not terminate). Counter-examples are important in verification mode. Spin creates a file named after the verification model with a `trail` extension anytime there is a violation in verification mode. That trail file is the trace for the counterexample to the property. Spin reports the trace with `spin -p -t <file>.pml`. The `-p` shows the PROMELA for the model, the `-t` tails Spin to follow the trace in the associated trail file, and the last argument is the model file tho which the trace belongs.

## Never claims (and problems to work)

Work more advanced examples in class and show how to verify with assertions *and* never claims (see below). The properties that should hold for mutual exclusion are:

  1. Mutual exclusion on the critical section
  2. No deadlock
  3. It works if only one process is active (no need to strictly alternate)

Properties (1) and (2) are direct. Property (3) takes some thought. The easiest solution is to run the model with a single process. In should not deadlock if implemented correctly. Here are example to model.

  * `mutex-unnamed.pml`
  * `pneueli-mutex.pml` (This code is the Peterson algorithm for two processes in disguise)
  * `simplification-dijkstra.pml`

If there is time, then consider teaching how to detect safety properties using `remoterefs` and `never` claims. A `never` claim detects finite behavior if the claim ever reaches its final state (exits the claim). The claim specifies something that should `never` occur in the model. Be sure to make clear that the presence of a `never` claim means that Spin in no longer checking for invalid end states (e.g., deadlocks). Use `spin -run -noclaim <filename>` to check for invalid end states. 

The `never` claim must always be able to step as it acts as a monitor in lock-step with the model. The model produces a transition, and the `never` claim consumes the effects of the transition, marking it as preserving the safety property or violating the property. The violation is denoted by exiting the claim. Note that if the `never` claim is unable to accept a transition, but the transition does not lead to an error state, that Spin backtracks and does not consider any further steps along that path.

Here is an example `never` claim for mutual exclusion.

```
never {
  do
  :: (pnueli[0]@CS && pnueli[1]@CS) -> break
  :: else -> skip
  od
}
```

## Mutex Reference

[The Problem of Mutual Exclusion: A New Distributed Solution](https://scholarscompass.vcu.edu/cgi/viewcontent.cgi?article=5524&context=etd) lists all the classic algorithms with their evolution in time.


