byte flags[2] = false
byte turn = 0

active [1] proctype p()
{
  byte i = _pid
  byte j = 1 - i
loop:
  flags[i] = true
  turn = j
  (flags[j] == false || turn == i)
cs:
  // Access shared resource exclusively
  flags[i] = false
  goto loop
}

ltl p0 {always !(p[0]@cs && p[1]@cs)}
ltl p1 {always (eventually p[0]@cs)}
ltl p2 {always (eventually p[1]@cs)}