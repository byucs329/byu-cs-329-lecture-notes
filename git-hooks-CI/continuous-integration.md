# Objectives

* Run integration tests with the Maven fail-safe plugin
* Configure continuous integration to run integration tests on any push
* Add web-hooks to github for continuous integration
* Protect branches in github to block push that fail integration tests

# Reading

* [Maven Failsafe](https://maven.apache.org/surefire/maven-failsafe-plugin/index.html) and [Inclusions](https://maven.apache.org/surefire/maven-failsafe-plugin/examples/inclusion-exclusion.html)
* [Integration Testing with Maven](https://www.baeldung.com/maven-integration-test)
* [Github protected branches](https://help.github.com/en/github/administering-a-repository/about-protected-branches)
* [CircleCI GitHub Integration](https://www.hackingwithswift.com/articles/100/how-to-validate-code-changes-using-circleci)
* [Jenkins Github Integration](https://www.blazemeter.com/blog/how-to-integrate-your-github-repository-to-your-jenkins-project/)

# Outline

Maven has the `test` phase and an `integration-test` phase. If the `test` phase fails, then the `integration-test` and `verify` phase do not run. Integration tests typically take longer to run compared to unit tests, so it is natural to separate them out in the build life-cycle.

## Maven Failsafe (Integration Tests)

The Surefire plugin recognizes anything with the `Test` or `Tests` pattern beginning or ending class and method names. It integrates into JUnit5, and is very useful for unit tests. The Failsafe plugin is the analogue for the integration tests, only it has a different pattern to use for finding tests. Its [inclusion pattern](https://maven.apache.org/surefire/maven-failsafe-plugin/examples/inclusion-exclusion.html) looks for the `IT` prefix or suffix.

  * `"**/IT*.java"` - includes all of its subdirectories and all Java filenames that start with "IT".
  * `"**/*IT.java"` - includes all of its subdirectories and all Java filenames that end with "IT".
  * `"**/*ITCase.java"` - includes all of its subdirectories and all Java filenames that end with "ITCase".

Configuration is direct in the project area. The executions are in `integration-test` and `verify`. Be sure to specify the test engine provider as a dependency somewhere in the POM (in this case, `org.junit.jupiter` is already in the dependency for the project). **NOTE**: put the plugin outside the `<pluginManagement>` section (after JaCoco in this example) for `mvn` to pick it up. For some reason, when it went in `<pluginManagement>` Maven never activated the plugin (there went a few hours of productivity).

```xml
<project>
  <build>
    <plugins>
      <plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>3.0.0-M3</version>
				<executions>
					<execution>
						<goals>
							<goal>integration-test</goal>
							<goal>verify</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
    </plugins>
  </build>
</project>
```

A `maven verify` will activate the target that should produce two test reports: one from Surefire for the unit tests and the other from FailSafe for the integration tests. Create a class that matches the defined patterns above and test it.

## GitHub CI/CD (PREFERRED FOR CLASS)

[Github Continuous Integration](https://docs.github.com/en/free-pro-team@latest/actions/guides/about-continuous-integration) is easy to configure and already to go. Enable by adding to the master branch a `.github/workflows/maven.yml` file with the following (default for project as suggested by github):

```yml
# This workflow will build a Java project with Maven
# For more information see: https://help.github.com/actions/language-and-framework-guides/building-and-testing-java-with-maven

name: Java CI with Maven

on:
  push:
    branches: [ master ]
  pull_request:
    branches: [ master ]

jobs:
  build:

    runs-on: ubuntu-latest

    steps:
    - uses: actions/checkout@v2
    - name: Set up JDK 1.8
      uses: actions/setup-java@v1
      with:
        java-version: 1.8
    - name: Build with Maven
      run: mvn -B package --file pom.xml
```

The file is fairly simple and self-explanatory by and large. It triggers on any push or pull-request to master. 

## Circle CI (Deprecated Class Demo)

Circle CI is fully connected to Bitbucket and GitHub. Creating a CI pipeline in either product in relatively easy. Sign into CircleCI with one or the other credentials, and then add projects to the pipeline. 

Adding a project to the pipeline requires that the master directory contain a `.circleci/config.yml` file to define the build pipeline for CI. Here is the `Java 8 Maven` configuration for Linux. The `working_directory` is the name of the directory on the server where the POM is located. The `path` is where the repository will be cloned for the CI. In this example, the project is in a subdirectory, and so the configuration of the `working_directory` and `path` is more complicated.

```yml
# Java Maven CircleCI 2.0 configuration file
#
# Check https://circleci.com/docs/2.0/language-java/ for more details
#
version: 2
jobs:
  build:
   
    docker:
      # specify the version you desire here
      - image: circleci/openjdk:8-jdk

      # Specify service dependencies here if necessary
      # CircleCI maintains a library of pre-built images
      # documented at https://circleci.com/docs/2.0/circleci-images/
      # - image: circleci/postgres:9.4

    working_directory: ~/byu-cs-329-lecture-notes/git-hooks-CI

    environment:
      # Customize the JVM maximum heap limit
      MAVEN_OPTS: -Xmx3200m

    steps:
      - checkout:
          path: ~/byu-cs-329-lecture-notes

      # Download and cache dependencies
      - restore_cache:
          keys:
            - v1-dependencies-{{ checksum "pom.xml" }}
            # fallback to using the latest cache if no exact match is found
            - v1-dependencies-

      - run: mvn dependency:go-offline

      - save_cache:
          paths:
            - ~/.m2
          key: v1-dependencies-{{ checksum "pom.xml" }}

      # run tests!
      - run: mvn verify

```

CircleCI always builds on any push to the `default` branch. In the project settings (choose the project and then the gears icon in the right corner), it is possible to change tho behavior to only build on pull requests. Go to the `Advanced Settings` and turn on the options to limit to pull requests from forked projects and pull requests in general.  CircleCI no longer triggers the pipeline on any commit to any branch, it only triggers on commits to the default branch (`master`). CircleCI does trigger now on any pull request.

A pull request triggers the CircleCI pipeline. Bitbucket reports the status of the pipeline build on the pull request (look right under the `watch` icon on the right---may need to refresh the page), and the reviewer is able to decline a pull request if the build fails. The Github integration is even tighter and the merge button is disabled until the pipeline finishes. If the build fails, then it is not possible to merge.

Note that it is worthwhile to install the CircleCI CLI tool to validate the config: `brew install circleci`. The command is `circleci config validate`. The file is sensitive to tabs etc. (similar to Make files).

The demonstration is on the `CI-Demo` branch. First create it in Bitbucket and then show how to protect it branch in Bitbucket for the lecture. It is protected in the `Settings` menu under `Branch Permissions`. The `Write Access` is nobody. The `Merge via Pull Request` is `ericmercer`. Set that the `Merge Check` that the last commit builds (Premium is required to enforce the check). Github is able to do the exact same thing.

Create a `README.md` file, add, commit, and try to push. Should see the reject message. Push it into a new branch, and then open a pull request on the `CI-Demo` branch. It will fail.

Push a new branch to the remote and then create a pull request for the merge: `git push origin CI-Demo:new_README`. The pipeline should trigger on the request and start the build. Once the build finishes, refresh the pull request page to see the build results. 

## Bitbucket Pipeline

Bitbucket offers 50 minutes per month to use its Pipeline product, For $3/month it upgrades to 2500 minutes. Consider doing the demo with Bitbucket Pipeline since the lectures are hosted on Bitbucket. Setup and integration seem trivial.

## GitLab for CI (Another Option But A Pain)

[Using GitLab CI/CD with a GitHub repository](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html) is the most direct path. There is also [Github Project Integration](https://docs.gitlab.com/ee/user/project/integrations/github.html) explain how to use [gitlab.com](http://gitlab.com) for just CI/CD. These are both the some and yet slightly different.

In any case, if connected to gitlab with github credentials, then simply connecting to the target repo is sufficient.

There are several ways to use GitLab (and it is just one example of many). It can either connect to a hosted project on GitHub using only CI/CD or it can import in an existing projected to be hosted directly on GitLab (enabling server side git-hooks). 

If Gitlab connects to an external repository to only use the CI/CD part of Gitlab, then it effectively clones the repository into GitLab, and when there is a push on the external repository, it pulls in the changes and runs the pipeline. The other way it can work is that developers switch to the Gitlab hosted project, and then when a commit is accepted on Gitlab, it pushes that same commit to the external repository. The behavior can all be configured in myriad ways.

The more common, and better integrated model, is to have Gitlab host the repository. In that mode, it provides not just CI/CD but a host of other products to manage the project. It is definitely worth exploring.