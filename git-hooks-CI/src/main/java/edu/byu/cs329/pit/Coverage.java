package edu.byu.cs329.pit;

public class Coverage {

  private Coverage() {
  }

  /**
   * TODO.
   * 
   * @param a TODO
   * @param b TODO
   * @return TODO
   */
  public static int russianMultiplication(int a, int b) {
    int z = 0;
    while (a != 0) {
      if (a % 2 != 0) {
        z = z + b;
      }
      a = a / 2;
      b = b * 2;
    }
    return z;
  }

  /**
   * TODO.
   * 
   * @param a TODO
   * @param x TODO
   * @return TODO
   */
  public static int conditionDecision(int a, int x) {
    if ((3 > a) || (x != 0)) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * TODO.
   * 
   * @param a TODO
   * @param x TODO
   * @return TODO
   */
  public static int conditionDecisionMore(int a, int x) {
    boolean b = x != 0;
    if ((3 > a) || b) {
      return 1;
    } else {
      return 0;
    }

  }

}
