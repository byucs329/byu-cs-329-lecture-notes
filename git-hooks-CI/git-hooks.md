# Objectives

* Use client side git-hooks to gateway commits
* Configure git-hooks for other events
* Explain the difference between client side and server side hooks

# Reading

* Atlassian [git-hooks tutorial](https://www.atlassian.com/git/tutorials/git-hooks)
* [Maven Git-hook](https://gist.github.com/mallocator/34332f7a6a68d15a419c)
* [Git Hooks Manual](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)

# Outline

Git-hooks are scripts that trigger on git events. Any non-zero return aborts the process. Events are partitioned into client-side and server-sides buckets. Below are some of the client side hooks (see the manual for a complete list).

  * pre-commit: runs before the commit message is even entered and can be used to inspect the snapshot (e.g. lint for example). It can be bypassed with `git commit --no-verify` (something to keep in mind).
  * prepare-commit-msg: runs before the commit message editor starts (can generate a default message).
  * commit-msg: gives access to the actual commit message and can abort the process if needed. Useful to inspect the commit message for style etc.
  * post-commit: useful for notification after the commit finishes
  * post-checkout: useful for setting up a working directory 
  * pre-rebase
  * post-merge
  * pre-push: runs after remote refs are updated but before objects are sent to the server

These give broad control over the entire commit life-cycle (first 4) and room for extra safety actions on checkout and rebase.

Server-side hooks control things that come in and out of the central repository. These can be used for continuous integration with a team of developers.

  * pre-receive (first thing that runs when handling a push from a client)
  * update (run on each branch the pusher is trying to update)
  * post-receive (runs after the entire process)

  We are only looking at a few client side hooks.

## Scripting Language

Really any scripting language can be used. In general a return value of `0` means everything is OK. A non-zero return value indicates some type of error or failure. Depending on the hook, a non-zero value terminates the action (e.g., aborts a commit).

## Client Side Hooks are Local

Hooks are *not* part of the clone process. They are not copied from a central repository when the project is cloned. As such, client side hooks are not able to **force** all the team members to use them, and it is possible to change them locally.

All the hooks reside in the `.git/hooks` directory. Git populates that directory with examples in a clone are a init command. *Installing a hook* simply means removing the `.sample` suffix and making the file executable (`chmod a+x <filename>`).

There is one way to try to force all team members to use the hooks. That is to provide git with a template directory to use for any clone or init. That template directory can include all of the hooks that are wanted so that they are there by default. Be warned though, they are still local and can be *uninstalled* or changed to suit the desires of any individual developer.

If there are more than one scripts to run on any hook, it is possible to put those in a directory. For example, create a `.git/hooks/pre-commit.d` directory for any scripts to run in that phase and then change the top-level script to run each script in the directory, in the appropriate order, and check the return type of each on aborting when needed.

Create and install a post-commit message as a demonstration.

```sh
#!/bin/sh
echo "# Nice commit!"
```

Execute a commit and verify that it runs.

BTW, if the scripts need to be in version control, then it is easy to add the scripts up with the other files that are part of the repository and then use symbolic links to map those files into the `.git/hooks` directory.

## Require mvn clean test

An obvious requirement, aside from enforcing lint and commit message formats is to be sure unit tests pass. The idea is to guard commits from breaking the build process. As mentioned previously, these are more effective on the server side, but with a good team, can be effective on the client side. The script goes in `pre-commit` as in the comments below.

```bash
#!/bin/bash
 
# save the file as <git_directory>/.git/hooks/pre-commit
# Be sure it is executable (chmod a+x pre-commit)
 
echo "Running mvn clean test for errors"

# retrieving current working directory
CWD=`pwd`
MAIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# go to git-hook-CI directory in the top-level
cd $MAIN_DIR/../../git-hooks-CI
# running maven clean test
MVN_RESULT=$(mvn clean test 2>&1)
if [ $? -ne 0 ]; then
  echo
  echo "${MVN_RESULT}" | ((tee /dev/fd/5 | grep -A 10 -B 2 "Reactor Summary:" >/dev/fd/4) 5>&1 | sed -n -e '/^Failed tests:/,/Tests run:.*$/ p' ) 4>&1
  echo
  echo "Error while testing the code"
  # go back to current working dir
  cd $CWD
  exit 1
fi
# go back to current working dir
cd $CWD
```

Create a test that fails in the repository and then attempt the commit.

There is a lot that can be done. Use it to improve defect rates and enforce style.
