# Objectives

JUnit is the backbone for test automation and management for this course. At the end of this lecture, the student is able to:

  * Configure Maven to run JUnit tests from the command line with the [Maven Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/)
  * Write basic JUnit5 tests with proper documentation
  * Use parameterized and dynamic tests with containers for test organization and test generation
  * Explain how parameterized tests are different from dynamic tests

# Reading

  * [JUnit5 User Guide](https://junit.org/junit5/docs/current/user-guide/)
  * [JUnit5 Tutorial](https://howtodoinjava.com/junit-5-tutorial/)
  * [Maven Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/)

# Maven, JUnit5, and Running Tests

The in-class activity will start with a solution for the [hw0-tooling](https://github.com/byu-cs329/hw0-tooling) code. There are two items that need to be in the `pom.xml` file:

  1. Compilation requires the **junit-jupiter-engine**
  2. Running tests from `mvn` in the *test* phase requires the [Maven Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/) configured to use `DisplayName`

Adding **(1)** is found by first searching for `Maven` in the [JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/) and then following the link to the `junit5-jupiter-starter-maven`. The `pom.xml` has the needed dependency for the build. It also has the entry for the [Maven Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/), but for Surefire, be sure to look at [Surefire Extensions and Reports Configuration for @DisplayName](https://maven.apache.org/surefire/maven-surefire-plugin/examples/junit-platform.html) to turn on `@DisplayName` in the reporting.

Adding **(2)** is also found in the [Usage](https://maven.apache.org/surefire/maven-surefire-plugin/usage.html) section of the [Maven Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/). I seem to use the version that appears in the usage guide with little other thought. That may or may not be a good idea. The [Using JUnit 5 Platform](https://maven.apache.org/surefire/maven-surefire-plugin/examples/junit-platform.html) at the bottom shows how to turn on `@DisplayName` in the reporting.

A `maven clean test` should report that there are no tests to run. The next step is to write a few tests for the `getMinimumIndex` method. Show the reports generated in `target/surefire-reports`.

# Writing Basic Tests

Properties of a good test

  * it uses a consistent and descriptive [naming convention](https://dzone.com/articles/7-popular-unit-test-naming) -- pick one and stick with it
  * it only tests one thing
  * it labels the test in the output with the `@DisplayName` annotation
  * it uses tags for filtering with the `@tag` annotation
  * it groups tests in a sensible way with the `@Nested` annotation

Create the needed `src/test/java` directory. Create the `src/test/java/edu/byu/cs329/hw0` directory structure for the test code. A `Maven --> Update Project` is needed to show the new package in Eclipse. Alternatively, copy in the solution from [hw0-tooling](https://github.com/byu-cs329/hw0-tooling) into the [hw1-junit](https://github.com/byu-cs329/hw1-junit) tree and work from there. For class, create a patch from the solution and then apply that patch to the [hw1-junit](https://github.com/byu-cs329/hw1-junit) fresh clone. Remember to create the feature branch first.

Create a new class: `DijkstaTest` and add the `@DisplayName` tag with some descriptive name: `JUnit5 unit tests for the Dijkstra class`. Create a few tests for the `getMinimumIndexMethod`. Run these tests. Show what happens when one fails.

# JUnit Platform Console Standalone

The JUnit Platform Console Standalone in the JUnit 5 User Guide in another good way to run tests. What makes the standalone nice is that it shows the test results in a tree structure using the `DisplayName`. It differs from the Surefire in that the Surefire console report flattens the test hierarchy where the standalone console report does not. This flattening really matters with nested, dynamic, and parameterized tests.

**Note: be sure to check and confirm versions to be the latest/greatest for the dependencies and plugins.**

Add the dependency.

```xml
<dependency>
  <groupId>org.junit.platform</groupId>
  <artifactId>junit-platform-console-standalone</artifactId>
  <version>1.10.0</version>
</dependency>
```

Configure `exec-maven-plugin`.

```xml
<plugin>
  <groupId>org.codehaus.mojo</groupId>
  <artifactId>exec-maven-plugin</artifactId>
  <version>3.1.0</version>
  <executions>
      <execution>
          <goals>
              <goal>java</goal>
          </goals>
      </execution>
  </executions>
  <configuration>
      <mainClass>org.junit.platform.console.ConsoleLauncher</mainClass>
      <arguments>
          <argument>--disable-banner</argument>
          <argument>--class-path=./target/test-classes</argument>
          <argument>--scan-classpath</argument>
      </arguments>
  </configuration>
</plugin>
```

If you got an "unnamed module" exception, then be sure the failed tests are testing public methods.

# Test Life-cycle

Use the `@BeforeAll` tag to create an instance of the `Dijkstra` to use for testing shortest path. Copy the code from `DijkstraMain.main`. Need to add static member variable for the `testSubject`.  Note that `@BeforeAll` and `@AfterAll` are **required** to be static methods.
Write several tests for the shortest path method.

In the rest of the lecture, we are not taking out any tests; rather, we are creating the same tests in different ways, and we are grouping tests.

# Nested Tests

Create inner classes for each unit test. Add in the `@BeforeEach`and `@AfterEach` annotations on methods and explain where they appear in the test life-cycle using the logger. The code should be something like below

```java
@DisplayName("Dijksta shortest path tests")
public class DijkstraTest {
  static final Logger logger = LoggerFactory.getLogger(DijkstraTest.class);

  static int count = 0;
  static Dijkstra testSubject = null;

  @Nested
  @DisplayName("Tests for Dijkstras.getMinimumIndex")
  class GetMinimumIndexTest {
    @Test
    @DisplayName("Given all lengths M and an empty set "
        + "when getMinimumIndex "
        + "then return 0")
    void given_allLengthsMAndAnEmptySet_when_getMinimumIndex_then_return0() {
      Set<Integer> set = new HashSet<Integer>();
      final int[] length = { Dijkstra.M, Dijkstra.M };
      Assertions.assertEquals(0, Dijkstra.getMinimumIndex(length, set));
    }

    @Test
    @DisplayName("Given all lengths M and all indices in set "
        + "when getMinimumIndex "
        + "then return M")
    void given_allLengthsMAndAllIndicesInSet_when_getMinimumIndex_then_returnM() {
      Set<Integer> set = new HashSet<Integer>();
      set.add(0);
      set.add(1);
      final int[] length = { Dijkstra.M, Dijkstra.M };
      Assertions.assertEquals(Dijkstra.M, Dijkstra.getMinimumIndex(length, set));
    }
  }

  @Nested
  @DisplayName("Tests for Dijkstras.shortestPath")
  class ShortestPathTest {
    final Logger logger = LoggerFactory.getLogger(ShortestPathTest.class);

    @BeforeAll
    static void beforeAll() {
      final int[][] graph = { { 0, 2, 4, Dijkstra.M, Dijkstra.M, Dijkstra.M },
          { 2, 0, 1, 4, Dijkstra.M, Dijkstra.M }, { 4, 1, 0, 2, 6, Dijkstra.M },
          { Dijkstra.M, 4, 2, 0, 1, 3 }, { Dijkstra.M, Dijkstra.M, 6, 1, 0, 5 },
          { Dijkstra.M, Dijkstra.M, Dijkstra.M, 3, 5, 0 } };
      testSubject = new Dijkstra(graph);
    }

    @BeforeEach
    void beforeEach() {
      logger.info("Test " + count++);
    }

    @Test
    @DisplayName("Given node 0 and 4 in test graph "
        + "when shortestPath "
        + "then return 6")
    void given_nodes0And4_when_shortestPath_then_return6() {
      Assertions.assertEquals(6, testSubject.shortestPath(0, 4));
    }

    @Test
    @DisplayName("Given node 0 and 3 in test graph "
        + "when shortestPath "
        + "then return 5")
    void given_node0And3_when_GivenNode0AndNode3_then_return5() {
      Assertions.assertEquals(5, testSubject.shortestPath(0, 3));
    }
  }
}
```

# Parameterized Tests

Move all the shortest path tests into one parameterized test that gets the nodes and expected value from a list using the `@CsvSource`. Use the `name` value in the `@ParameterizedTest` to give each test a sensible name.

```java
  @DisplayName("Tests for Dijkstra.shortest path (ParameterizedTest)")
  @ParameterizedTest(name = "Given ({1},{2}) when shortestPath then return {0}")
  @MethodSource("generateExpectedAndFromAndTo")
  void shortestPathTestsParamterized(int expected, int from, int to) {
    int actual = testSubject.shortestPath(from, to);
    Assertions.assertEquals(expected, actual);
  }

  static Stream<Arguments> generateExpectedAndFromAndTo() {
    return Stream.of(
        Arguments.arguments(6, 0, 4),
        Arguments.arguments(5, 0, 3));
  }
```

Note that these tests follow the life cycle and `@BeforeEach` code.

# Dynamic Tests

These tests do not support the `@BeforeEach` and `@AfterEach` life-cycle hooks. Create the same tests as the parameterized tests only as dynamic tests this time.

```java
@DisplayName("Tests for Dijkstra.shortest path (TestFactory)")
  @TestFactory
  Stream<DynamicTest> shortestPathTestsFactory() {
    int[] expected = { 6, 5, 3 };
    int from = 0;
    int[] to = { 4, 3, 2 };
    Vector<DynamicTest> tests = new Vector<>();

    for (int i = 0; i < expected.length; ++i) {
      String displayName = MessageFormat.format(
          "Given ({1},{2}) when shortestPath then return {0}",
          expected[i], from, to[i]);
      final int index = i;
      DynamicTest test = DynamicTest.dynamicTest(displayName,
          () -> {
            int actual = testSubject.shortestPath(from, to[index]);
            Assertions.assertEquals(expected[index], actual);
          });
      tests.add(test);
    }
    return tests.stream();
  }
```
Or you can lazily build everything using `Stream.Map`.

```java
  @DisplayName("Tests for Dijkstra.shortest path (TestFactory -- stream based)")
  @TestFactory
  Stream<DynamicTest> shortestPathTestsFactoryStreamBased() {
    int[] expected = { 6, 5, 3 };
    int from = 0;
    int[] to = { 4, 3, 2 };

    Stream<Integer> indices = Stream.iterate(0, i -> i + 1);
    Stream<DynamicTest> tests = indices
        .limit(3)
        .map(i -> {
          String displayName = MessageFormat.format(
              "Given ({1},{2}) when shortestPath then return {0}",
              expected[i], from, to[i]);
          final int index = i;
          return DynamicTest.dynamicTest(displayName,
              () -> {
                int actual = testSubject.shortestPath(from, to[index]);
                Assertions.assertEquals(expected[index], actual);
              });
        });
    return tests;
  }
```

Really we have only scratched the surface for JUnit5. The expectation for the course is to use all the features to create, organize, and document tests.

# Assertions.assertAll

Collect multiple assertions in to a single ```Assertions.addAll```. It will run all the assertions in the list and then report any that fail.

```java
@Test
void mockitoTest() {
  Set<Statement> leaders = ComputeLeaders.getLeaders(cfg);
  Assertions.assertAll(() -> Assertions.assertEquals(5, leaders.size()),
    () -> Assertions.assertTrue(leaders.contains(es1)),
    () -> Assertions.assertTrue(leaders.contains(ws1)),
    () -> Assertions.assertTrue(leaders.contains(es3)),
    () -> Assertions.assertTrue(leaders.contains(es5)),
    () -> Assertions.assertTrue(leaders.contains(end)));
}
```

# Old outline (that is good too)

This lecture is designed to go early in the course and depends on no course material (although it assumes that students have Eclipse up and running).

It uses the BST implementation found [on bitbucket](https://bitbucket.org/byucs329/binarysearchtree/); specifically, it uses the no-tests branch. The master branch contains tests that cover 100% of the branches and instructions.

Structure
=========

First, have students partner up so that each pair has a laptop. Have them clone the bitbucket repository and check out the no-tests branch. Take a moment to discuss GUI vs. terminal git use. (5 min intended; probably will take 10 min.)

Show them the creation of a single JUnit Jupiter (what most of us mean when we say JUnit 5; JU5 contains Jupiter, Vintage, and Platform) test case. Run it without checking coverage. Have them create a similar test case with their partners. (5 min; total is 15 min.)

Take a few minutes to allow students to write a few more test cases. Encourage them to try to exercise all of the code in the implementation. (10 min; total is 25 min.)

Show them coverage (EclEmma is built in). Since this implementation is fairly straightforward, it should be fairly easy to determine what sort of test is needed to exercise a particular line of code or branch. (5 min; total is 30 min.)

Have them work with their partners to write tests, aiming for 100% coverage. Congratulate anyone who finds an actual bug. (This should easily take the remaining 20 min of class time.)
