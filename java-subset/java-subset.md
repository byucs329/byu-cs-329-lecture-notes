# Language Features

Only a subset of Java is considered in the course. That subset is defined as a list of allowed constructs. An additional list of specific omissions is added for clarity. First the allowed constructs with the correspondence to the [org.eclipse.jdt.core.dom](https://help.eclipse.org/2019-12/index.jsp?topic=%2Forg.eclipse.jdt.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fjdt%2Fcore%2Fdom%2Fpackage-summary.html) object.

  * Single file compilation (e.g., no imports) (`CompilationUnit`)
  * Type declarations including inner classes (`TypeDeclaration`)
  * Methods (`MethodDeclaration`)
  * Blocks/Scopes (e.g., `{...}`) (`Block`)
  * Variable Declarations with optional initializers but no declaration lists (`VariableDeclarationFragment`)
  * `Boolean`, `Integer`, `Object`, `Short`, and `String` from `java.lang`
  * `short`, `int`, and `boolean` primitives
  * return statements (`ReturnStatement`) 
  * if-statements (`IStatement`)
  * while-statements (`WhileStatement`)
  * do-statements (`DoStatement`)
  * Method invocations (`MethodInvocation`)
  * Empty statements (e.g., `;` alone) (`EmptyStatement`)
  * New expressions (`ClassInstanceCreation`)
  * `this` expressions (`ThisExpression` that contains `FieldAccess`)
  * dotted expressions (e.g., `a.b.c`) (`QualifiedName`)
  * Assignment expressions (`Assignment`)
  * Number literals (`NumberLiteral`)
  * String literals (`StringLiteral`)
  * Boolean literals (`BooleanLiteral`)
  * Numeric infix expressions with `+`, `-`, `*`, or `/` as operators including extended operands (e.g., `1 + a + 3`) (`InfixExpression`)
  * Boolean expressions with `||`, `&&`, `<`, `>`, `<=`, `>=`, `==`, or `!=` as operators (short-circuit evaluation must be considered) (`InfixExpression`)
  * Prefix expressions for the `!` operator only (`PrefixExpressions`)
  * Parenthesized expressions (`ParenthesizedExpression`)
  
### Not Allowed Stuff to Be Clear

  * No generics, lambda-expressions, or anonymous classes
  * No interfaces or inheritance (you do not need to track a type hierarchy in this part of the lab)
  * No reflection
  * No imports
  * No shift operators: `<<`, `>>`, and `>>>`
  * No binary operators: `^`, `&`, and `|`
  * No `switch`-statements
  * No `for`-statements
  * No arrays
  * No type casting
  * No exceptions
  * No conditional expressions
  * No type literals
  * No variable declaration lists (e.g. `int a,b,c`)
  * No variable shadowing

# Example Program

[BinarySearchTree.java](BinarySearchTree.java) fits neatly into the subset and is shown here for convenience.

```java
package edu.byu.cs329.javasubset;

public class BinarySearchTreeSubset {

  class Node {
    
    public final int value;
    Node left;
    Node right;
    
    
    public Node(int value) {
      this.value = value;
      this.left = null;
      this.right = null;
    }

    public Node(int value, Node left, Node right) {
      this.value = value;
      this.left = left;
      this.right = right;
    }

  }
  
  protected Node root;

  private boolean search(final Node node, final int value, final boolean doAddValue) {
    if (value == node.value) {
      return true;
    }
    
    if (value < node.value) {
      if (node.left != null) {
        return search(node.left, value, doAddValue);
      }
      if (doAddValue == true) {
        node.left = new Node(value);
      }
      return false;
    }
    
    if (node.right != null) {
      return search(node.right, value, doAddValue);
    }
    if (doAddValue == true) {
      node.right = new Node(value);
    }
    return false;
  }
  
  public BinarySearchTreeSubset(int value) {
    this.root = new Node(value);
  }

  public boolean contains(int value) {
    return search(this.root, value, false);
  }
  
  public boolean add(int value) {
    return !search(root, value, true);
  }
  
  /**
   * Main method.
   * 
   * @param args  none required.
   */
  public static void main(String[] args) {
    BinarySearchTreeSubset bst = new BinarySearchTreeSubset(10);
    bst.add(11);
    System.out.print(10);
  }
}
```