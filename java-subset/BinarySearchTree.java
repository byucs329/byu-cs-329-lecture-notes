package edu.byu.cs329.javasubset;

public class BinarySearchTreeSubset {

  class Node {
    
    public final int value;
    Node left;
    Node right;
    
    
    public Node(int value) {
      this.value = value;
      this.left = null;
      this.right = null;
    }

    public Node(int value, Node left, Node right) {
      this.value = value;
      this.left = left;
      this.right = right;
    }

  }
  
  protected Node root;

  private boolean search(final Node node, final int value, final boolean doAddValue) {
    if (value == node.value) {
      return true;
    }
    
    if (value < node.value) {
      if (node.left != null) {
        return search(node.left, value, doAddValue);
      }
      if (doAddValue == true) {
        node.left = new Node(value);
      }
      return false;
    }
    
    if (node.right != null) {
      return search(node.right, value, doAddValue);
    }
    if (doAddValue == true) {
      node.right = new Node(value);
    }
    return false;
  }
  
  public BinarySearchTreeSubset(int value) {
    this.root = new Node(value);
  }

  public boolean contains(int value) {
    return search(this.root, value, false);
  }
  
  public boolean add(int value) {
    return !search(root, value, true);
  }
  
  /**
   * Main method.
   * 
   * @param args  none required.
   */
  public static void main(String[] args) {
    BinarySearchTreeSubset bst = new BinarySearchTreeSubset(10);
    bst.add(11);
    System.out.print(10);
  }
}
