Containerization is critical for testing is it bridges the gab between deployment and test environments. It also provides a mechanism whereby the test and development environments are easily deployed to all developers. In the extreme, it is also the mechanism to deploy to the customer or production environments. It also worth noting that it is how most CI/CD tools deploy and test in the cloud.

Docker is fast becoming the de-factor way to specify *images* and build *containers* from images. An image is a specification for a container. A container is an instance of an image. A container ends up being a process, but that process effectively some form of Linux distribution, depending on the image, for Docker. Good practice includes with the code the image specifications for development, test, and deploy. The image specifications are stored in the code repository and as important as the code itself.

# Docker Desktop

[Docker Desktop](https://www.docker.com/products/docker-desktop) is all that is needed to get started with Docker. There is a cask is brew for it: `$ brew cask install docker`. A few useful commands: `docker run` and `docker container ls`. 

The windows desktop version will show the containers and give some basic ability to understand the state of the container. OSX only provides the `docker` command after the app is launched.

# Java Development

There are two readily available (and easy to use) paths in Docker for Java development and test. Each is details below.

## Maven Images for Test

The easiest by far is to use maven images that are publicly available. A good overview is in [Crafting the Perfect Java Docker Build Flow](https://codefresh.io/docker-tutorial/java_docker_pipeline/).

```
$ docker run --interactive --tty --rm --name hw2 --volume $(pwd):/usr/src/app --workdir /usr/src/app maven:3.8-jdk-11 mvn install
```

The `--interactive` and `--tty` make the command at the end be interactive using a console interface. The `--rm` deletes the container once the command exits. The `--volume` flag mounts the current working directory to the `/usr/src/app` mount point in the container. The host filesystem is now synced with the container at the mount point and below so it is possible to edit files and then re-run tests in the container. The `--workdir` specifies where to run the command. The image is `maven:3.6-jdk-8`. Docker automatically gets and builds a container from the image. The rest is the command to run.

Super helpful is the ability to get a shell in the container:

```
$ docker run --interactive --tty --rm --name hw2 --volume $(pwd):/usr/src/app --workdir /usr/src/app maven:3.8-jdk-11 bash
```

The shell enables the ability to run maven from the command line inside the shell running in the container. For example, it is possible to edit the files and then run `mvn test` from the shell interactively. In this interactive mode using the shell, the maven is build and reuse its cache in the container.

As the container is deleted each run, all the project dependencies are deleted as well, so running the maven command again will download all the dependencies again. This re-download to populate the maven cache can be avoided by mounting the local cache in the filesystem to the container: `--volume $(home)/.m2:/root/.m2`

For the windows folks, the environment variables and commands have to be treated differently. The command is as below in Powershell:

```
$ docker run --interactive --tty --rm --name hw2 --volume "${pwd}:/usr/src/app" --workdir "/usr/src/app" maven:3.8-jdk-11 mvn install
```

The paths have to be quoted as do the lists (e.g., the : joined things).

# Visual Studio Code Connected to Docker Container

Super cool is the ability to have Visual Studio Code open a directory on the host machine in a running docker container. [Developing inside a Container](https://code.visualstudio.com/docs/remote/containers) is a good starting point. For this class, the Java container is a fine starting point. The specification for that container is added to the local directory, so the image can be changed and rebuilt. Code automatically detects changes and prompts to rebuild.

Visual Studio Code is for Java development is easy to get up and going. The *Java Extension Pack*, *Maven for Java*, and *Java Test Runner* cover about everything needed for code to work. Open the directory with the `pom.xml` in code for all the extensions to wake up and go. Right click on the Maven entry in Explorer gives the `mvn` targets. Hovering over tests will give `Run Test|Debug Test`. As a note, when running code in a container, the *Java Test Runner* defaults to lightweight mode. Click the beaker bottle in code to get to *Test* and there should be a button to switch to standard mode. The `Run Test|Debug Test` are not available in lightweight mode.

Install the *Remote Development* extension. In the command prompt look *Remote-Containers*. Choose *Java* for the container. It takes some time to build the image so be patient. After that, use the terminal in code to run commands or use the `mvn` targets. Code prompts to re-open the directory in the container whenever the directory is opened in code. The container preserves the `mvn` cache.
