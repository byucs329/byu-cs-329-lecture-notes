# Reading

# Objectives

* Cast data flow analysis into a generaly data flow framework
* Explain the difference between a *must* and a *may* analysis
* Define kill/gen sets for
   * Reaching definitions
   * Available expressions
   * Very busy expressions
   * Live variables

# Reaching Definitions

*For each program point, which assignments **may** have been made and not overwritten along some path to that program point.*

* RDentry : Lab* → P(Var* \times D) 
* RDexit : Lab* → P(Var* \times D) 

**Notes:** 

* Lab* and Var* are the subsets of labels and variables occurring in the program under analysis, e.g., { 1, 2, …, 7 } and { n, result, i }
* D = { \cdot, ? } \cup Lab*
dot (\cdot) denotes definition from a formal parameter (or a field)
question mark (?) denotes unknown definition

The data flow is defined in terms of *kill* and *gen* sets.

A definition is **killed** in a block if its variable is defined in the block

    kill_{RD} : Lab* \rightarrow P(Var* \times D) 
  
Examples: kill_{RD}([x = e ;]^l) = { (x, d’) | d’ \in D } and killRD([…]^l) = \emptyset, where *l* is the label of the statement. Notice that the definition adds to the set *all* tho possible definitions of *x*.

A definition is **generated** in a block if the block assigns a value to a variable

    gen_{RD} : Lab* \rightarrow P(Var* \times D) 

Examples: genRD([x = e ;]^l) = { (x, d) } and gen_{RD}([…]^l) = \emptyset

Generate *kill* and *gen* sets for factorial. 

## Flow Equations

RD_{entry}(l) =
 * if l = b_init: (x, \cdot) for all parameters and class member variables and (x,?) for all local variables that are unassigned
 * otherwise: \cup{RD_{exit}(l^\prime) | l^\prime \in preds(l)}

RD_{exit}(l) = (RD_{entry}(l) \ kill_{RD}(l)) \cup gen_{RD}(l)

The direction is forwart with RD_{entry} defined for the first node.

Work out the flow for the `factorial(int n)` method. Repeat the exercise for the `fib(int n)` method.

**Question**: what about a maximal basic block?

```java
x = x + 2
y = x * 4
```

# Live Variable Analysis

A backward version of reaching definitions.


# Lecture Outline

Do everything on the whiteboard and follow *13-static-analysis-classic-problems.ppt* in this order:

  * Give the definition
  * Show the example
  * Draw the CFG
  * Define the Kill/Gen sets
  * State the flow (forward/backward, union/intersect)
  * Work the example

Start with RD, then show LV as the backward version of RD. Do AE and then show VBE as the backward version of AE.

