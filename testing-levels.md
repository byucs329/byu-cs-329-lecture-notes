# Intro

Testing levels are a structured way for thinking about tests for components and systems.

# Testing

## Testing levels: Unit, Integration, Validation

- A unit test is for an individual component, like a method or a class
- Integration testing ensures that the units interact correctly
- Validation testing checks against the user's expectations; usually, in terms of a specification

## Regression

Regression tests are useful for ensuring that changes don't break things. In contrast with testing levels, which describe _what_ is being tested, regression tests are about _how_ or _why_ the tests are written and performed.

Regression tests are applied per-commit or at regular time intervals. It is possible to configure version contol systems like git to apply tests before commits or after updates. It is also possible to run a cron (e.g., daily) to make sure things still work.

## PDGs

Build a PDG of a function with a defect. Use a debugger and the PDG to successively step backwards until the source of the defect is found.

(TODO: choose or write an appropriate function and identify or add a defect)

# Unit tests, revisited

Unit tests are more complicated to write when the components we test interact with other components in ways we don't want them to during testing. For example, we might not want our unit tests to actually send network traffic or to actually write to disk. The same is true of components with circular dependencies.

To address this, we "mock" components. From the perspective of a unit test, a mocked component behaves as expected; however, it does nothing on the other end. For example, a mocked object for sending information to the network returns normally (unless it is configured to throw an exception) but sends nothing to the network. By specifying behaviors, we can simulate one component doing the "right thing" so we can test whether or not the unit being tested works correctly.

(TODO: choose or write an appropriate example of Mockito)